




import std.string;


import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

//dlang struct vs class?
// structs are copied by value. Do we want this? Maybe for color, but not for bitmaps?
// but then what happens if you FREE a bitmap that's referenced multiple times?



// TODO ---> Whats the best way to wrap this stuff in a D, managed layer?

struct font_t
	{	
	ALLEGRO_FONT *data;
	//int t; even with aliasing, we can still use  font x; x.t = 2; and even forward method calls into a aliased structure;
	alias data this; //DO WE WANT THIS exposed?
	}
	
struct bitmap_t 
	{
	ALLEGRO_BITMAP *data;
	alias data this;
	}


struct display_t
	{
	ALLEGRO_DISPLAY *data;
	alias data this;
	}

struct color_t
	{
	ALLEGRO_COLOR data;
	alias data this;
	/*
	struct
		{
		ubyte r, g, b, a; //what about deep color? meh?
		}
	union
		{
		int packed_color;
		}*/
	}
	
	
class graphics_driver_t
	{
	@property bool has_initialized = false; //use properties? (then we can't use prefix _notation)	
	bool display_created = false;
	bool error_occured; //?
	
	display_t display;
	
	
	void initialize(){}
	
	
	void create_display(int x, int y)
		{
		display = al_create_display(x, y);
		}
	
	
	/*
	http://dlang.org/phobos/std_string.html#.toStringz
	
	Important Note: When passing a char* to a C function, and the C function
	 keeps it around for any reason, make sure that you keep a reference to it
	  in your D code. Otherwise, it may become invalid during a garbage 
	  collection cycle and cause a nasty bug when the C code tries to use it.
*/
	bitmap_t load_bitmap(string path)
		{
		bitmap_t x;
		x = al_load_bitmap(toStringz(path));
		return x;
		}
		
	font_t load_font(string path, int point_size, int flags)
		{
		font_t x;
		x = al_load_font(toStringz(path), point_size, flags);
		return x;
		}

	void select_bitmap(bitmap_t destination)
		{
		}

	void draw_bitmap(int x, int y, bitmap_t source, int flags)
		{
		al_draw_bitmap(source, x, y, flags);
		}
	
	//NOTE: Should we use make_rgb and make_rgba, OR, use make_rgba and use a default argument for a?
	color_t make_rgba(float r, float g, float b, float a = 1.0f) //make_rgb, grab_rgb, color_rgb?
		{
		color_t x;
		
		x = al_map_rgba_f(r, g, b, a);
		
		//x.data.r = r; //I hope ALLEGRO_COLOR is floating point internally!
		//x.data.g = g;
	//	x.data.b = b;
///		x.data.a = a;
		return x;
		}
				
	color_t make_hsl(float h, float s, float l)
		{
		color_t y;
		y = al_color_hsl(h, s, l);
		return y;
		}

	void clear_to_color(color_t x)
		{
		al_clear_to_color(x);
		}
		
	void flip_display()
		{
		al_flip_display();
		}
		
	void draw_text(font_t the_font, color_t the_color, int alignment, float x, float y, string text)
		{
		//	al_draw_text(font, ALLEGRO_COLOR(1, 1, 1, 1), x, y, ALLEGRO_ALIGN_CENTRE, "Hello!");
		al_draw_text(the_font, the_color, x, y,  ALLEGRO_ALIGN_CENTRE, toStringz(text) );
		}
	
	//SHOULD WE SUPPORT string.Format() from C# style?
	void draw_textf() //or printf? or writef etc...
		{
		//TODO
		}		
		
	void set_target_bitmap(bitmap_t bitmap)
		{
		al_set_target_bitmap(bitmap);
		}
		
	bitmap_t get_target_bitmap()
		{
		bitmap_t x;
		x = al_get_target_bitmap();
		return x;
		}
		
		
	void resize_display(int screen_id, int w, int h){}
	void resize_window(int x, int y, int w, int h){}
	void move_window(int screen_id){}
	
	void get_dpi(){}
	float get_physical_width(){return 0f;}
	float get_physical_height(){return 0f;}
	}



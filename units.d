
import std.stdio;
import std.conv;
import std.string;
import std.format; //String.Format like C#?! Nope. Damn, like printf.


// Fun unit testing ... test
//=============================================================================

// INTERNALLY, we can probably use SINGLE unit types (meters, kilograms, etc) and just convert
// to other types like FEET on the visual side at the VERY END of calculations.

// THIS WAY, we never have to implement converting between say, KILOGRAMS and LBS, inside the general code
// and template code.

// THE ONE PROBLEM might be if we have to convert between DEGREES and RADIANS. 
// BUT we could hardcode them ourselves either by manually converting or using
// ADDITIONAL conversion functions degToRad() / etc as you normally would.








// Most of our calculations will be restricted to
// position, velocity, acceleration.

// and MAYBE pv=mRT and heat equation for FEA
// Ugh, maybe more for electronics. Don't forget WATTS for power. Ugh. 


/// https://github.com/PhilippeSigaud/D-templates-tutorial/blob/master/D-templates-tutorial.html

// https://dlang.org/templates-revisited.html

// variable arguments  
// https://dlang.org/variadic-function-templates.html



// template TUPLE parameter A...
//http://dlang.org/spec/template.html#TemplateTupleParameter
// LOL the section is missing...  GO TO SECTION Template Sequence Parameters

class pixel_t{}
class __pixel_t{}

class meter_t{}
class meter__s_t{}
//class meter2_t{}		DON'T NEED WITH TEMPLATES, BWAAAA
//class meter2__s_t{}		On the otherhand, we haven't written ANY FACT CHECKING yet. This is SOLELY making new data types. 
// NO interaction between them is coded yet.

class second_t{}
class __second_t{}
//class second2_t{}
//class __second2_t{}

// ??? maybe?
class degree{} //or fahrenheit?
class __degree{} //??
class celsius{}
class __celsius{}
class kevlin{}
class __kevlin{}
	// ARE WE GOING TO automatically do UNIT CONVERSIONS? or are we SOLELY unit checking for plausibility?
	// otherwise, just stick to NORMAL units of {METER, SECOND, KELVIN}


class newton{}	//force
class __newton{}
class kilogram{}	// weight  (should we use GRAMS or KILOGRAMS?) I THINK if we use grams, we need to use centimeters. (CGS system vs MKS)
class __kilogram{}	
class joule{}	//energy
class __joule{} 
class watt{}	// power
class __watt{}	


class cycle{}   //dimensionless?
class __cycle{} //possible?

class radian{}	// dimensionless?
class __radian{}

// Do we need PRESSURE? or just psi/etc/whatever using pressure / area
// PASCELS

// WHAT about FEA? Electronics? etc.

// class ohm{}  and
// class __ohm{}
// voltage, amperage, charge?
// https://en.wikipedia.org/wiki/Centimetre%E2%80%93gram%E2%80%93second_system_of_units

// viscosity?

// WHAT ABOUT GAS/LIQUID UNITS?
// volume, pressure, viscosity?

//(involving units of length, mass, force, energy, pressure, and so on)

class template_class_t( T )
	{
		public:
		T data;
	}

/*
template TFoo(T)  //TFoo becomes the namespace
	{

		
	class foo
		{
		}
		
	void func2(T)
		{
		}
	
	T value;
	}

*/


// OLD STYLE
// ---------------------------------------------------------------------
class NIL{}
class meter{}
class __meter{}
class second{}
class __second{}

class unit( T, U = NIL ) //DEFAULT template arguments?! cool! Still kind of HACKISH if we have a max amount and just HARDCODE the rest to NIL. (see varadic template)
	{
	int x;
	}


// ---------------------------------------------------------------------

// Haven't done ANY TESTING with T = double vs float vs integer mixing.
// That is, 
// auto x =	third_unit(float, second)(3.22) * third_unit(double, second)(4.23);   

class third_unit( T, Args...)
	{
	T data;
	alias data this; // might work??? for downcasting (?) to float/double/int?	

	this()
		{
		data = 0;
		}
			
	this(T)(T x) //float, double, int, etc. (how to prevent COMPLEX types?)
		if(__traits(isArithmetic, x)) // <-- Maybe this?
		{
		writeln("[0a] - this(", typeid(x), " x=",x,")");//DEBUG
		data = x;
		}
		
	// for NON-arith types?
	this(T)(T x) 
		if(!__traits(isArithmetic, x)) // <-- NOTE negation.
		{
		writeln("[0b] - this(", typeid(x), " x=",x,")");//DEBUG
		data = x.data;
		}




	auto opAssign(T x) //works with x = 3
		//if( is(T == T) )			WHY ARE THESE BLOWING UP???? It works on other functions!!!
		//if(__traits(isArithmetic, T))
		{
		static if (is(T == float))
			{
			data = x;
	
			writeln("  [1-A] - opAssign(", typeid(x), " x=",x,")");//DEBUG   	HEY, I think because of alias data this; x.data -> x  when you ask for x here.
			writeln("  [1-A] - I am a (", typeid(this), ")");//DEBUG
	
			}
		else static if (is(T == this))	// CAN THIS case ever happen? We NEED IT don't we, since this is the only valid case?
			{
			data = x.data;

			writeln("  [1-B] - opAssign(", typeid(x), " x=",x,")");//DEBUG
			writeln("  [1-B] - I am a (", typeid(this), ")");//DEBUG
			}
		}
		
	//	void opAssign(	third_unit!(T, string) x)  HOW DO WE ACCEPT OURSELVES? (Woah.... deep....)
	//	{
	//	data = x.data;
	//	}

	T opBinary(string op)(T x) //I have a float, therefor I take a float
	//  if(__traits(isArithmetic, T))
	//	if(T !is V)
		{
		writeln("  [2] - opBinary(", typeid(x), " x=",x,") { data * x }"); //DEBUG
		return data * x; //works if float NOT if another T/unit class
		}

	auto opBinary(string op)(third_unit x) // For working with SAME TYPE AS ME (but what about different template instantiations of ME?) 
		{
		writeln("  [3] - opBinary(", typeid(x), ")"); //DEBUG
		data *= x.data;
		return x; 
		}

	// Is this even being called anymore?
	// <------------------------------------------
	/*
	V opBinary(string op, U, V)(U x) //For TWO different template types?!  U means ADD ANOTHER TEMPLATE, then we use it in (U x)
		{	
		writeln("[4] - opBinary(", typeid(this), " WITH ", typeid(x), ")"); //DEBUG
		//data *= x.data;
		return x;   // DON'T WE need to return a NEW TYPE, combined with the units???
		}
		// V doesn't seem to be helping. 
*/
	// cast(type ) e --->	e.opCast!(type)()		
	float opCast(T)() //BINGO FINALLY, we didn't need (T x) variable. We JUST NEED A TYPE (T).
//		if(is(T == float))
		{
		writeln("  [5] - opCast(", typeid(T), ") CALLED"); 
		return data;
		}

	void opCall(T)(T x)
		{
		writeln("  [6] - opCall(", typeid(T), "=", x,") CALLED"); 
		data = x;
		}


	

/*

if (MyClassInt subclass = cast(MyClassInt)value) {
	writeln(subclass.value);
}

If it doesn't cast to said type (it will be null) that branch won't execute.
*/
	}


//non-template tests
class normalclass
	{
	double data;
	
	float opCast()
		{
		return data;
		}
	}

class normalclass2
	{
	double data;
	alias data this;
	}



void display_values(T)(T x)
	{
	writeln(x, "     --     ", typeid(x));
	}


void display_values(T, V)(T x, V y)
	{
	writeln(x, " + ", y, "     --     ", typeid(x), " + ", typeid(y));
	}

void test_third()	
	{
	writeln();

	auto test1 = new normalclass;
	auto test2 = new normalclass2;
	float out1 = cast(float)test1; //using opCast() works
	float out2 = test2;  //using ALIAS'ing works!
		
		
		
	// NOTHING is using [4] (see template/class). Hmm.
		
	// SELF TESTS as well as BUILT-IN TYPES
	// ---------------------------------------------------------
	auto x = new third_unit!(float, meter, second);
	auto x2 = new third_unit!(float, meter, second)(5.1f); // I NEED TO WRITE CONSTRUCTOR.
	x(3); 								// calls opCall(3)
	x(3.5f); 							// ""
//	writeln(x.get_units());
	x = 3 ; 							// calls opAssign [1]
	writeln( x ); 						// [NEW] alias T this; MAKES IT WORK. (before, printed type)
	writeln( (x * 2) ); 				// calls opBinary, [2]
	x = x; 								// works... shallow copy??
	auto d = (x * x);					// [3]
	writeln("d is ", typeid(d));
	
	// TYPE CONVERSION TESTS
	// -------------------------------------------------------
	auto t1 = new third_unit!(float, meter);
	auto t2 = new third_unit!(float, __second);
	t1.data = 5;
	t2.data = 10;

	//r = results
	auto r1 = new third_unit!(float, meter, __second);
	float r2;
	float r3;
	float r4;
	
	// OBJECT -> OBJECT
	r1 = (t1 * t2); 			// Works... now?? (using alias T this; I think)  //NOTE this is a THIRD TYPE separeate from t1 and t2
    
    // OBJECT -> float
    r2 = (t1 * t2);				// Works... now?? (using alias T this; I think)
	r3 = cast(float)(t1); 		// FIXED. opCast(T)(){}  NOT opCast(T)(T x){}
	r4 = t1;  					// WORKS via alias T this;... I think?
	
	writeln();
	
	writeln("r1 is ", typeid(r1), " = ", r1.data);
	writeln("r2 is ", typeid(r2), " = ", r2);
	
	writeln();
	
	// TYPE CONVERSION TESTS to INCORRECT types
	// -------------------------------------------------------
	writeln("NEXT TEST");
	writeln("-----------------------------------");

	auto test10 = new third_unit!(float, meter, __second)(10);
	auto test11 = new third_unit!(float, meter, __second)(12);
	auto test12 = new third_unit!(float, __second)(5);
	float test13 = 5;
	auto test14 = new third_unit!(float, __second)(5);

//	test10 = test11; //no function printout??? Is this because it's a REFERENCE IDENTITY ASSIGNMENT so it's "automatic"? ObjectA dies. ObjectB has two references now.
//	test10 = test12; //involves casting, so it DOES call opAssign??


// WAIT, x += y is different than x = x + y   ????


	writeln("----------");
	display_values(test10, test11);
//		test10 += test11;
		test10 = test10 + test11;
	display_values(test10);	

	writeln("----------");
	display_values(test11, test12);
	//	test11 += test12;
		test11 = test11 + test12;	//blows up without alias data this;  	BUT NOTICES they're different types!
	display_values(test11);	

	writeln("----------");
	display_values(test14, test13); //note 14/13	
//		test14 += test13;
		test14 = test14 + test13;
	display_values(test14);	
	
	writeln("-----------------------------------");
	writeln("END");

	// V = A * time
//	auto vel = new third_unit!(float, "meter;__time");
//	auto accel = new third_unit!(float, "meter;__time;__time")(3);
//  auto time = new third_unit!(float, "time")(2);
//	vel = accel * time; //UGH this could get hard....
	}



// BASED ON THIS:
// https://dlang.org/variadic-function-templates.html
class varidic_unit() // TERMINUS for recursion as well as default for no arguments. (see url)
	{
	void run(){}
	}

class varidic_unit( T, A...)
//	if() //CAN HAVE IF HERE, also we can use ANY COMPILE TIME FUNCTION. like custom isValid()	
	{
		
	float data;
		
// TRAITS
// https://dlang.org/spec/traits.html
//	assert(  
//			__traits(isTemplate(this)) 
// 	)


//https://dlang.org/spec/operatoroverloading.html

//https://dlang.org/templates-revisited.html	


// TEMPLATE CONSTRAINTS
//https://dlang.org/concepts.html

	
	auto opUnary(string s)() 
		if (s == "-")
		{
        return -data;
		}
	
	T opBinary(string op)(T rhs, A pbs)
//		if(T != B) // Can have IF here. AMAZING!! "Only works if x_type != y_type in x * y"
		//if(T == B)
		{
		static if (op == "*") return data * rhs.data;
	//	return rhs.data;
		}
		
	void run()
		{
		writeln(__traits(identifier, T)); // THIS SPITS OUT the first one.
		auto x = new varidic_unit!(A); //note, no T     (makes a new one and spit out the rest minus the FIRST argument.) 
		x.run();
		}
	}


void test_template()
	{
	//!TFoo(float).value x = 2; //if the template is ONE parameter long, the parenthesis can be omited TFoo!int.t x;   // same as TFoo!(int).t x;
	
	alias ut = template_class_t!( int );
	
	ut x = new ut;
	x.data = 2; // THIS BLOWS UP, WHY? (DUH, need to make a NEW variable^^)
	
	
	auto y = new unit!(meter);
	auto accel = new varidic_unit!(meter, meter, __second); //meter^2 / s
	accel.run();
	
	(-accel);
//	(accel * accel); FAILS so far
	//auto asf = (accel * accel);
	}







import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

import std.string;
import std.conv;

import helper;
import molto;
import common;
import animation;

class object_t 
	{
	animation_t anim;
	
	string pretty_name; //temp use.
	string comment;
	string type; //for resaving, TYPEID like "MONKEY"
	
	world_t world;
	
	float x=0, y=0;
	float x_vel=0, y_vel=0; //for space floating
	bool free_floating=true;
	
	
	int m; //classic argument. each map has objects? or one list of objects and they list what map they have? At least that way, pointers/indexes never invalidate, right? And changing map is simply changing the [x,y,map].
	// also, do objects know bout their map, or simply, maps hold lists of objects? Or both? Objects need to know if they're talking to an object on the same map.

	this()
		{
		pretty_name = "???";
		}
		
	this(string name)
		{
		pretty_name = name;
		}

	this(string name, float x, float y, int map, world_t world)
		{
		this(name); //good or bad idea?
		this.x = x;
		this.y = y;
		this.m = map;
		this.world = world;
		}
		
	void draw(viewport_t v)
		{
	//	writefln("%f %f", x, y);
			
		al_draw_text(
			g.font,
			ALLEGRO_COLOR(1,1,1,1), 
			x-v.offset_x, 
			y-v.offset_y + 20, 
			ALLEGRO_ALIGN_CENTRE, 
			pretty_name.toStringz());
		
		if(anim !is null)
			{
//			anim.set_direction(dir.down);

					import std.math;
					float A = to!float(x/32)-20;
					float B = to!float(y/32)-20;
					float d = 
						sqrt(
							A*A + B*B
							);
					float val = d / 100.0;
//					al_draw_filled_rectangle(x1, y1, x2, y2, al_map_rgba_f(val,val,val, 0.5f));
		//			al_draw_filled_rectangle(x1, y1, x2, y2, al_map_rgba_f(val,val,val, 1.0f));
			
			ALLEGRO_BITMAP* bmp = anim.get_directed_cell();


//	const int ALLEGRO_ZERO = 0;
//	const int ALLEGRO_ONE = 1;
//	const int ALLEGRO_ALPHA = 2;
//	const int ALLEGRO_INVERSE_ALPHA = 3;
	
//	al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_DEST_MINUS_SRC, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

			al_draw_bitmap(
				bmp,
//				al_map_rgba_f(val,val,val, 1.0f),
				(x - v.offset_x) - bmp.w/2,
				(y - v.offset_y) - bmp.h/2, 0);

	// DEFAULT blender
	//al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);



			}else{
			al_draw_filled_rectangle
				(
				(x - v.offset_x)-10,
				(y - v.offset_y)-10,
				(x - v.offset_x)+10,
				(y - v.offset_y)+10,
				gfx.rgb(255,0,0)
				);
				}

		}
		
	void tick()
		{
		if(g.world.maps[0].is_space(x, y))
			{
			free_floating = true;
			}else{
			free_floating = false;
			}

		if(free_floating)
			{
			x += x_vel;
			y += y_vel;
			}
		}

	void attempt_to_move_rel(float x_rel, float y_rel)
		{
/*		if(anim !is null)
			{
			if(x_rel < 0.001)anim.set_direction(dir.left);
			if(x_rel > 0.001)anim.set_direction(dir.right);
			if(y_rel < 0.001)anim.set_direction(dir.up);
			if(y_rel > 0.001)anim.set_direction(dir.down);
			writefln("update anim %f,%f", x_rel, y_rel);
			}
	*/		
	
		int map_w=g.world.maps[0].width;
		int map_h=g.world.maps[0].height;
			
		if(	x < 0){x = 0; return;}
		if(	y < 0){y = 0; return;}
		if(	x >= map_w*g.TILE_D){x = map_w*g.TILE_D - 1; return;}
		if(	y >= map_h*g.TILE_D){y = map_h*g.TILE_D - 1; return;}
	

		if(g.world.maps[0].would_collide(x + x_rel, y + y_rel))
			{
			}else{
			x += x_rel;
			y += y_rel;
			
			x_vel = x_rel;
			y_vel = y_rel;
			}
	
		if(free_floating)
			{	
			free_floating = false; //temporary. not right place for this.NOTE.
			return;
			}
		}
	}


//module a5test; //THIS WAS HERE BEFORE. Not related to filename a5_test.d

// <----- DON'T FORGET. We could support using GPUs to compute FEA calculations / etc! Everyone has a videocard, why not use it on the server
// of course, we only need this IF we actually hit performance bottlenecks.

// Don't forget IMMUTABLE VARIABLES are implicitly shared in D since they can't change--who cares which thread reads it!


// "I don't think you'll find many responses, mainly because 'shared' is not yet fully supported by the runtime or Phobos, so people don't use it all that much.
// WHAT???
//http://forum.dlang.org/post/epwtewnnyscyrvbzxlhz@forum.dlang.org
/*
	Here's two popular libraries that implement @nogc containers:
	http://code.dlang.org/packages/emsi_containers
	http://code.dlang.org/packages/memutils

http://forum.dlang.org/post/ybmfzegcisjkyyqloxil@forum.dlang.org
*/



// DO WE NEED TWO VERSIONS? unique_handle, and shared_handle?
// unique_handle - only one object will ever be pointing to this resource. No manager needed? Encapsulate the resource inside the handle? Maybe i'm missing something... I need a diagram up in this bitch...
// shared_handle - a manager notifies all shared_handles when the resource changes.


// resource	- 0x238523
// handle - "TACO"
//

struct handle_manager_t(T)
	{
	super_handle!T [] instances_to_resource;  //or handles_to_resource? omg terminolgy brain fart.
	
	void invalidate_all_instances()
		{
		foreach(T x; instances_to_resource)
			{
			x.invalidate();
			}
		}
		
	}

//  cached type?
//  aiased type?  _alias!(int)?
struct super_handle(T) //should small DATA TYPES uses as numbers/ptrs, as opposed to large classes, need the _t postfix?   [float vs fixed vs integer] VS [network_handler_t]
	{
	T _alias;
	T *_actual;
	bool is_dirty;
	
	// NO IDEA if this code makes logical sense... something doesn't seem right and it's 1 AM.

	void invalidate()
		{
		is_dirty = true;
		}
		
	T read() //should be overloaded op, right?
		{
		debug{
			if(!is_dirty)
				{
				if(_actual != *(_alias))
					{
					assert(false, "Internal alias != actual value! Did the pointed to resource CHANGE after it was cached? And if so, did it not intentionally invalidate all handles?");
					}
				}
			}
			
		if(is_dirty) // either first time, or, handles have become invalidated.
			{
			_actual = *(_alias); 
			}
		
		return _alias;
		}
	
	this()
		{
		is_dirty=true;
		}
	
	this(T x)
		{
		_alias = x;
		is_dirty=true;
		}

	void retrieve_actual()
		{
		}
	}




// Do we need any kind of special classes for WEAPONS, etc? Or are they ALL just scripted objects?
// WHAT CAN WE OFF-LOAD to binary code that ALL weapons need?

enum body_hardpoints // Is there a difference between TARGET hardpoints and damage? (like feet, can you target and/or get hurt on feet but NOT target?)
	{
	head = 0,
	eyes,		// inside head, still target-able (blinding people, etc.)
	ears,		// inside head, no manual target? (Separate from eyes in SS13 so we can WEAR HEADPHONES AND EYE GLASSES/helmet at the same damn time.)
	mouth,		// inside head, still target-able (choking, holding someones mouth to prevent talking, etc.)
	torso,
	groin,		// inside torso
	right_arm,	
	left_arm,
	right_leg,
	left_leg
	
//	left_foot,	// Not normally targetted... but otherwise, we can't hurt BARE FEET ON GLASS. (Unless we combine with "leg")
//	right_foot
	// Actually, shouldn't we just combine it with "leg" damage


	// Do we need any kind of CUSTOM hardpoints for aliens, robots, biped animals?
	// That is, reserved extras for clever SCRIPTING additions later? 
	}


enum inventory_hardpoints //Where items can be MOUNTED on the body. NOT whether these items have their own inventories.
	{
	// HAIR, how do we get BALDED. Hair should be a "body part" / organ / something.
		
	head,	// helmets
	face,	// gas masks
	eyes,	
	ears,
	torso_underwear,	// jumpsuit?
	torso_armor,		// armor
	torso_overwear,		// overcoat	(not in SS13. Armor IS overcoat slot. Too many ?)
	torso_backpack,		// backpack
	belt,
	arms,				// needed?? new? arm-worn badges, gadgets etc?
	hands,				// GLOVES.
	legs,				// pants
	feet,				// shoes, boots. Do people need to wear socks under their [boots]? Seems pointless.
	extra				// maybe?    BADGES, etc???
	}
	
// --> Do we want ITEMS to have a LIST of hardpoints they can sit in? Or simply just ONE spot.
// Since left AND right arm are covered under "arms" perhaps this "feature" wouldn't be too useful. 


// Can ANYTHING have inventory slots? Because we could do something COOL like have BOOTS (or a hat) 
// have a hidden compartment.


// SURGUERY related
// ======================= ======================= =======================

class human_body_t
	{
	human_organ_t [] organs;
	}





// <--- How do we handle PUTTING STUFF INSIDE SOMEONE? (giggity.)
/*
 What if we treat a BODY simply like an INVENTORY. A list of organs.
 However, organs will need an (x,y) coordinate as well as 
 A list of connections of [LINKAGE_TYPE] connected between organs, 
 connected to specific SLOTS of the organ.

 EACH OBJECT that SUPPORTS SURGERY should have an ORGAN_CONFIG/SCHEMA/LAYOUT
 And the (encapsulated) SURGERY SYSTEM automatically Just Works (TM) !!!
 Add dogs. Add aliens. Etc.

 Need to have a way to link ORGANS to REAL_WORLD_OBJECT_TYPES so you can rip
 them out. Where the ORGANS have way more internal details, than the object_type
 versions which are simply the physical interaction with the objects like name, weight, etc.
*/
enum organ_linkage_types //note these linkages may actually have MULTIPLE PURPOSES (fuel + immune system) so have we 
// GONE ABSTRACT ENOUGH? Or do we need to split into yet another more abstract class system? Such as EFFECTS
// and each linkage type can enumerate these effects (or just simply DON'T use these at all and JUST use the effects!)
	{
	// HUMANS and general
	blood,		// carries fuel, immune system, and hemoglobin (REMEMBER, IT'S NOT RAW O2! You can't just inject O2 into your veins.)
	lymph,		// immune system
	digestion,  // food/fuel. nom nom
	intestinal, // food/fuel. poopyes. (is there a difference between this and digestion??)
	air,
				// 
				
			//what about mouth? Used for breathing AND digestion...
			// We either have to support
			// - Allow MULTIPLE types for linkages (air && digestion)
			// - hardcoded combination types (air_and_digestion type), could complicate IF code. (Have to check ALL combination cases. ugh.)
			// - Allow MULTIPLE LINKAGES with the same coordinates/connections. 
			//		- mouth -> lung [type: air] and,
			//		- mouth -> stomach [type: digestion] (oops, these aren't to the same place...)
			//
			// I meant something like this:
			//	organA -> organB [type: air]
			//  organA -> organB [type: water]
			//
			// This seems like the best possible route.
			
	// ROBOTS
	electrical,
	hydraulic, //more generic version of blood? Or implies mechanical body?
	pneumatic, //air. Just... use air as above?

	// PLANTS
	// ? water?
	// fuel?

	// ALIENS
	acid,
	plasma,
	psi, // ?? for more temporal / spiritual beings? Should it be "ooze" or some trans-dimensional linkage?
	} // Should be IN SCRIPT FORM?

enum organ_linkage_effects // Oh god. Could we ABSTACT THIS FURTHER? 
// Air is a gaseous fuel. Food is a LIQUID AND SOLID fuel.
	{
	fuel_inlet, 		// food before/during processing
	processed_fuel, 	// e.g. carbohydrates that directly fuel

	oxygen,				// should this be "air" or "gaseus fuel" or something more abstract?
	processed_oxygen, 	// ready to fuel muscles...

	garbage_byproducts, // poopy. but also lactic acid/etc from muscles...
	}
	
enum organ_linkage_effects2
	{
	food,			// Oxygen in. food and water in.
	fuel,			// Processed fuel ready for consumption
	biproduct,		// Biproducts after use. Muscles waste, poopies, etc.
	immune_system,  // Lymph veins, blood veins
	}

enum organ_linkage_effects2_type
	{
//	air,			// 
	gas,			// 
	liquid,			// 
	solid			// 
	}


struct TEST_ORGAN_LINKAGE
	{
	organ_linkage_effects2 A;
	organ_linkage_effects2_type B;
	bool is_input; //do we need this, or just swap the from/to? Also, what if we swap it to be is_output? Seems more natural BOOL usage. Or use ENUM for bool to be =INLET/OUTLET.
	human_organ_t from; //D syntax note: so if I DON'T specify it in the default argument list (See below), is it defaulted to 0/null/.init? It does compile!
	 human_organ_t to;
	}
	
	
// HOW DO WE LIST INLETS? Like "fake organs" that connect to the mouth? So mouth takes IN from [EXTERNAL]?
human_organ_t organ_external; // do we have one universal, or MULTIPLE external points for showing in a medical diagnostic/health screen?
human_organ_t organ_mouth; // is this an organ...? or should this go straight to "stomach and lungs"
human_organ_t organ_stomach;
human_organ_t organ_intestines; //or large_intestines...
human_organ_t organ_lung1;
human_organ_t organ_lung2;
human_organ_t organ_kidney1;
human_organ_t organ_kidney2;
human_organ_t organ_bladder;
human_organ_t organ_penis; //needed, it just straight bladder to external?
//human_organ_t organ_liver;


// This should all be built at run-time or something from a script or graph data structure.
/*
TEST_ORGAN_LINKAGE mouth1 = {organ_linkage_effects2.food, organ_linkage_effects2_type.liquid, true, organ_external, organ_mouth};
TEST_ORGAN_LINKAGE mouth2 = {organ_linkage_effects2.food, organ_linkage_effects2_type.solid, true, organ_external, organ_mouth};
TEST_ORGAN_LINKAGE mouth3 = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.gas, true, organ_external, organ_mouth};
TEST_ORGAN_LINKAGE nose	  = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.gas, true, organ_external, organ_mouth};

TEST_ORGAN_LINKAGE stomach1 = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.liquid, true, organ_mouth, organ_stomach};
TEST_ORGAN_LINKAGE stomach2 = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.solid, true, organ_mouth, organ_stomach};
TEST_ORGAN_LINKAGE stomach3o = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.liquid, false, organ_stomach, organ_intestines};
TEST_ORGAN_LINKAGE stomach4o = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.solid, false, organ_stomach, organ_intestines};

TEST_ORGAN_LINKAGE lung1  = {organ_linkage_effects2.food, organ_linkage_effects2_type.gas, true, organ_mouth, organ_lung1}; //do we need DUPLICATE links for lung1 and lung2??
TEST_ORGAN_LINKAGE lung2  = {organ_linkage_effects2.food, organ_linkage_effects2_type.gas, true, organ_mouth, organ_lung2}; //do we need DUPLICATE links for lung1 and lung2??
TEST_ORGAN_LINKAGE lung3o = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.liquid, false, organ_lung1, organ_mouth}; //output
TEST_ORGAN_LINKAGE lung4o = {organ_linkage_effects2.fuel, organ_linkage_effects2_type.liquid, false, organ_lung2, organ_mouth}; //output

TEST_ORGAN_LINKAGE butthole1 = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.solid, false, organ_intestines, organ_external};
TEST_ORGAN_LINKAGE butthole2 = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.liquid, false, organ_intestines, organ_external}; //ewww!

		// Is this how kidneys function??? hahah
		// Don't they filter from the blood system? (from the AORTA!) So this is WRONG, FYI. 
TEST_ORGAN_LINKAGE kidney1 	 = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.liquid, false, organ_intestines, organ_kidney1}; 
TEST_ORGAN_LINKAGE kidney2 	 = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.liquid, false, organ_intestines, organ_kidney2}; 

TEST_ORGAN_LINKAGE bladder1	 = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.liquid, false, organ_kidney1, organ_bladder}; 
TEST_ORGAN_LINKAGE bladder2	 = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.liquid, false, organ_kidney2, organ_bladder}; 
TEST_ORGAN_LINKAGE bladder3o = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.liquid, true, organ_bladder, organ_penis}; 

TEST_ORGAN_LINKAGE penis 	 = {organ_linkage_effects2.biproduct, organ_linkage_effects2_type.liquid, false, organ_penis, organ_external}; 


*/


class organ_linkage_t
	{
	organ_linkage_types type;

	int required_tech_level_to_see; //needed? Could we "show" linkages at a level but only show the organs one higher?
	int required_tech_level_to_identify; 
	}

class human_organ_t
	{
	string name;
	
	bool is_consumer; // aren't all?
	object_t [] consumption_type;
	bool is_producer; // aren't all? (Well, EYES are tied to ABILITY)
	object_t [] production_type; // need to support more than one output?

	// What if we produce an INTERNAL system, a FLUID, as opposed to just an object?
	// It might be better to NOT use object_t at all.
	// What about a NEW internal type? internal_object_t? So if you REALLY one someone's blood to
	// also exist as blood in the physical world, you'll need to make TWO objects (one internal, one normal.)
	
	ability_t [] abilities_supported; // list of abilities supported by this organ.
	/*
		 When multiple organs support an ABILITY how do we deal with it?	
		 For example
			- When ONE EYE is gone, your vision quality is HALVED. (Separate question.. is it really?)
			- When ONE KIDNEY is gone, kidney function is halved.
		 (vs)
			- When ONE HEART is gone, your brain is dead. Even though OTHER ORGANS also support the brain. 
		
		 -> HEY, notice! When there are MULTIPLE ORGANS of the SAME TYPE/NAME, they are redundant!
			- However, when they're DIFFERENT ORGANS, they all matter!
	*/

	// Do we need various SCRIPTS for the effects that organs each have when damaged?
	// DISEASES are each a SCRIPT, right?


	int required_tech_level_to_see; 		// 0 = all views notice it. (see below for more notes.) Think of this like a tiny organ inside the brain we didn't notice till better microscopes existed.
	int required_tech_level_to_identify; 	// Self-explainatory. What level is needed to understand what you're seeing.
	}
	
class ability_t  // Abilities include MOST FUNCTIONS OF A PERSON.
	{		// Walking, running, 
	string name;
		
	// HEY, we can even have THIS SHOW UP AUTOMATICALLY in medical scanners.
	int required_tech_level_to_see; //0 = all views notice
	int required_tech_level_to_identify; // needed here?
	
	human_organ_t [] required_organs; // associated_organs?
	
	bool all_organs_required; 	// needed? (see notes above)
	bool organs_are_redundant; 	// 
	}

class disease_t // Something that affects an ability.
	{
	string name;
	string script;
	}
	
	
//hardcoded for now...
enum diseases
	{
	cold
	}

enum symptoms
	{
	cramps,
	coughing,
	choking,
	weasing,
	crying,
	fatigue,
	shortness_of_breath,
	sneezing
	//diarreha - Do we support POOPY?
	}


enum human_systems
	{
	cardiovascular,
	digestive,
	endocrine,
	excretory,
	lympathic,
	integumentary,
	muscular,
	nervous,
	reproductive,
	respritary,
	skeletal
	}


// If possible, we can have viruses/drugs directly AFFECT these data structures for their effects
// while keeping a certain level of encapsulation/expansion.

// This is WAYYY to detailed, isn't it? There's got to be a way to prune it down to broader EFFECTS categories.
// For example, the organs associated with EATING. What qualities can you have? Throwing food back up. Increasing or reducing desire for food. Increasing/decreasing the amoutn of CALORIES gained from food. etc.

enum human_organs //1 == left, 2 == right. 1 and 2 are ebrainasier to tell appart visually than L and R at the end of a word. Seems like, at least. lungR vs lung2.
	{
	// (head) HAIR??
	// (body) hair? Simple support for making monkey's all HAIRY. heh.
		
	eye1,
	eye2,  // Do we need TWO types, or simply TWO INSTANCES with different X,Y coordinates (and maybe flipped X or Y axis?)
	ear1,
	ear2,
	nose,
	
	
	
	hair, // ?  "hair transplant" haha. Cutting hair could be internally a "condition", and it could be affected by drugs/viruses.
	skin, // YEP that's an organ!
	brain, // a brain... a brain... my kingdom for a brain.
	
	
	mouth, // not organ?
	teeth, // not organ?

	throat, // not an organ?
		larynx, // "voice box"
		pharynx,
		esophagus, //down to stomach from throat
 		
	stomach,
		
	lung1,
	lung2, //different SIZE for humans!
	heart,
	thyroid,
	kidney1,
	kidney2,
	liver,
	pancreas,
	gallbladder,
	small_intestine, // 
	large_intestine, // colon (4 sections/types?!) = ascending, transverse, descending, sigmoid. (which includes some or all of the below)
	//     cecum (beginning)+appendix,    rectum, anal canal, anus
	appendix,
	spleen,
	bladder,
	
	// ???
	penis,
	testicle1,
	testicle2,
	prostate,
	vagina,
	ovary1,
	ovary2,
	
	andrenal_gland,
	lymph_node,   //many?
	
	pituitary
	}

// https://en.wikipedia.org/wiki/Organ_(anatomy)#Organ_systems
// https://en.wikipedia.org/wiki/List_of_organs_of_the_human_body
enum other_body_parts //??
	{
	blood_veins,
	lymph_veins,
	blood_vessels,
		
		
	tooth,
	mouth,
	tongue,
	throat,
	bones, // MANY MANY
	muscles, // MANY MANY
	
	spinal_cord,
	skin,
	hair,
	nails,
	nerves
	}


/*
http://serendip.brynmawr.edu/bb/kinser/Structure1.html

	Frontal Lobe- associated with reasoning, planning, parts of speech, movement, emotions, and problem solving
	Parietal Lobe- associated with movement, orientation, recognition, perception of stimuli
	Occipital Lobe- associated with visual processing
	Temporal Lobe- associated with perception and recognition of auditory stimuli, memory, and speech




READ ME
https://en.wikipedia.org/wiki/Human_brain

don't forget brain stem

GREAT LINK
	https://www.mayfieldclinic.com/PE-AnatBrain.htm


http://bkpk.tk/brain-sections/


 - The cerebrum is the largest part of the brain and is composed of right and left hemispheres. It performs higher functions like interpreting touch, vision and hearing, as well as speech, reasoning, emotions, learning, and fine control of movement.

 - The cerebellum is located under the cerebrum. Its function is to coordinate muscle movements, maintain posture, and balance.

 - The brainstem includes the midbrain, pons, and medulla. It acts as a relay center connecting the cerebrum and cerebellum to the spinal cord. It performs many automatic functions such as breathing, heart rate, body temperature, wake and sleep cycles, digestion, sneezing, coughing, vomiting, and swallowing. Ten of the twelve cranial nerves originate in the brainstem.

 - cerebrospinal fluid (CSF) is produced INSIDE the brain!

12 cranial nerves and their functions:
	
	1 - Olfactory			smell
	2 - optic				sight
	3 - oculomotor			moves eye, pupil
	4 - trochlear			moves eye
	5 - trigeminal			face sensation
	6 - abducens			moves eye
	7 - facial				moves face, salivate
	8 - vestibulocochlear	hearing, balance
	9 - glossopharyngeal	taste, swallow
	10- vagus				heart rate, digestion
	11- accessory			moves head
	12- hypoglossal			moves tongue



*/
enum sections_of_brain
	{
	//Cerebrum
		temporal_lobe,
		frontal_lobe,
		occipital_lobe,
		parietal_lobe,


	//Cerebellum "little brain" (the lower part of the brain)
	cerebellum, //This structure is associated with regulation and coordination of movement, posture, and balance.
	
		// The cerebellum is assumed to be much older than the cerebrum, evolutionarily.
	
	//Limbic System - The limbic system, often referred to as the "emotional brain", is found buried within the cerebrum.
		thalamus, 		// functions such as relaying of sensory and motor signals to the cerebral cortex,[2][3][page needed] and the regulation of consciousness, sleep, and alertness.[citation needed]
		hypothalamus, 	// One of the most important functions of the hypothalamus is to link the nervous system to the endocrine system via the pituitary gland (hypophysis).
		amygdala,		// Shown in research to perform a primary role in the processing of memory, decision-making, and emotional reactions, the amygdalae are considered part of the limbic system.[3]
		hippocampus, 	// (part of temporal lobe??) "seahorse" from its shape. humans/mammels have two.  belongs to the limbic system and plays important roles in the consolidation of information from short-term memory to long-term memory, and in spatial memory that enables navigation. 
	
	 //in order:
		midbrain,
		pons,
		medulla, //aka medulla oblongota. responsible for autonomic (involuntary) functions ranging from vomiting to sneezing. The medulla contains the cardiac, respiratory, vomiting and vasomotor centers and therefore deals with the autonomic functions of breathing, heart rate and blood pressure.

// cingulate cortex
// neocortex

//	cerebral_cortex, //The cerebral cortex is the outer layer of neural tissue of the cerebrum in humans and other mammals.
// The cerebral cortex plays a key role in memory, attention, perception, awareness, thought, language, and consciousness.
 	
 	
 	brain_stem, // ???  midbrain, pons and medulla.
 	
	spinal_cord //yeah, technically it's apart of the brain!
	}


// Example alien species
// http://tolweb.org/accessory/Characteristics_of_Annelida?acc_id=57
enum annelid_organs
	{
	//palps, antennae, eyes, statocysts, nuchal organs and lateral organs
	clitellum,
	bladder, //OMFG. You can END an enum WITH A COMMA and still compile? FUCK YES.
	}
	
// Inventory Related
// ======================= ======================= =======================

enum BULK_TYPE
	{
	NONE = 0, 		// Debug? Or, used for things that can INFINITELY go into any slot. Like CASH maybe?
	tiny,			// exists?
	small,
	medium,
	large,
	huge,			
	massive			// can't fit in an anything?
	}

// - Do we want to CHANGE the inventory system, or merely ENHANCED it?
// For example, BULK vs WEIGHT. SS13 has NO WEIGHT.
// Also, SS13 has ONE item per slot, regardless of size.
// Except when they manually add STACKS. A BYOND / API crap work-around for "one item per slot."

// [SEE ./ideas/inventory and containers.md] 

// for example...
class object_type_t
	{
	string 		name;
	int 		inventory_slots; //number_of? num_? n_inventory_slots? naming convention...
	// what about inventory views?
	// - How do we support 2-D inventories like the lightbulb box in SS13?
	// - 1x5 for a belt
		
/*
	 ---> Do we need an INVENTORY SCRIPT? (or function/callback) so we can DECIDE when an object
	 can or cannot be added into our slot?
		 WHO, WHEN, WHERE decides applicable slots? Objects know, right, but what happens when we 
	 		ADD NEW OBJECTS? We want to use CATEGORIES where possible.
			- STANDARD (binary) categories like "weapon", "food", "drink" 
			- Do we support automatic CUSTOM CATEGORIES?
				- HOW?
	
			- What about MULTIPLE CATEGORIES (TAGS technically):
				- After all [food] + [drink] == [consumable]
				- So a box for edible consumables can naturally contain either food types.
				- So the next QUESTION is, do we allow TREE level/heirarchal tags?
				- So if an object allows [consumable] any NEW type of tag that inherits from [consumable]
					like [food] or [drink] will apply. 
				- OTHERWISE, we'll need to ONLY have tags that ONLY apply to our specific category.
				- CAVEATS? Could allowing a logical UNION of sets have any DRAWBACKS that I haven't thought
					of for script programming? (Something CUSTOM gets added into a category and now it
					can go into PLACES / INVENTORIES that other scripts never would have expected.)
					- Imagine a FOOD item but it's actually a gigantic, walking, EDIBLE tree.
					- If we're allowing "foods" into our box, that tree will fit!
						- HOWEVER, the tree will likely be HUGE/MASSIVE in BULK CATEGORY so this particular 
							situation might work.	
	
			- One more note about CATEGORIES/TAGS. If we allow COMPLEX HEIRACHIES, and scripters go crazy
				we could end up with RIDICULUSLY COMPLEX, spaghetti-level sets of filtering critera both
				in code, and scripts. 
	
			- One more note. What if an object supports a CATEGORY but it really needs a more SPECIFIC
				category! Like a gun that needs AMMO, but only AMMO of a certain type.
				e.g. Laser gun can use ANY battery. BUT NO BULLETS.
				
					Object -> Ammo -> Laser Battery
	
				Or more specific filtering: Laser rifle can use any [laser_rifle] batteries.
	
					Object -> Ammo -> Laser Battery -> Rifle sized -> [various sizes]
	
			- ANOTHER PROBLEM. What if you can use a [BATTERY] in a [LASER RIFLE] in addition
				to proper ammo! How do you divy that up?! After all, a [BATTERY] fits a lot of 
				things! WORSE STILL, only [BATTERIES]->[SMALL] will fit in your rifle, while
				[small, medium, large, huge] may fit in an APC. So how do you decide THAT case, eh?
	
				- In this case, it might be good to resort to some script based critera. This
				kind of code will not be run THAT OFTEN and is RELATIVELY fast/small code. Only run
				when something tries to enter something else. (Giggity.)
					- So you'd use normal, binary-level critera where possible and if the EXCEPTIONS tag
					is added, it'll run the bool item_classification_exceptions() function which will
					return true/false of whether or not this object can be accepted by the storage.
*/

	string 		script; // we should have scripts PER TYPE, right, not per instance. But per instance DATA.
	//??? script_data; // per instance
	
	//object_t [] inventory; // should be PER INSTANCE
	// Should we use some form of STATIC class data instead of two distinct classes? It's porbably fine without. 
		
		
	
	bool 		is_open_container;	// All items STAY VISABLE and ON TOP of the container.
	//	Works great for FORKLIFT lifting something or a CRYO TUBE.
	// 	Other wise, they become "hidden" unless the container is destroyed or "dumped" over.
	// 	OTHER NOTE: [NEW FEATURE] Using the normal inventory dialog for ACCESS.
	int 		container_x_offset, container_y_offset; //centroid position for items when visible.
	bool 		is_dumped_over; //has item been "dumped" open. Items that don't use NORMAL inventory dialog, OR, special cases like a trashcan being knocked over.
	
	bool 		uses_normal_inventory_access;   //needed? (see above note.) Uses normal inventory dialog with stacks, etc showing.
 	// Otherwise, you have to open/dump and close it to access the items (LIKE OLD SS13 CLOSET functionality.)
 	
 	// PREVIOUSLY there were only TWO(?) methods for INVENTORY.
 	// The player had the advanced inventory system. Items in his hands, or dragged, show inventory dialog.
 	// Items that CONTAIN items like closet, have to be "opened" (DUMPED in our code) or "closed" which releases all
 	// items so if the container is then dragged, the items stay on the ground.
 	// There is also special systems like the silly "Drag character to yours" to activate "steal their items" dialog.
 		
 		
 		
	bool 		is_stackable;
	int 		maximize_stack_size;
	
	float 		weight;
	BULK_TYPE 	bulk_type;


 	// NOTE, for some objects, we'll need TWO graphical layers. (PLUS BLENDING FUNCTIONS)
 	// CRYO TUBE
 	bool uses_top_layer;
 	int top_layer_blending_function; //replace with ENUM
 	
 	// CLIPPING RECTANGLE for TOP SPRITE (is this _NEEDED_?)
 	// The idea here is that a CRYO TUBE has a certain WINDOW viewport and 
 	// any drawing OUTSIDE OF THIS is clipped. So a larger sprite than a TUBE will be clipped at the
 	// tube boundaries.
 	//
 	// HOWEVER, other objects won't use their top layer as a "containment BOX". 
 	// So someone under an umbrella CAN SHOW outside the umbrella. Of course, we're only talking about
 	// objects that are listed as INSIDE THIS OBJECT'S INVENTORY. But a FORKLIFT could have a PERSON
 	// inside.
 	//
 	// Since we're doing that, WHY NOT just have a bool "is_clipped_at_sprite_boundaries"?
 	// 
 	//inside_clipping_rectangle (max size 256+256, seems pretty reasonable... except for HUGE sprites like Singularity and Demigods? Do they NEED a clipping rectangle then?)
// 	byte clipping_x; //if in doubt, just use a bloody ushort
// 	byte clipping_y;
// 	byte clipping_w;
// 	byte clipping_h;
	bool is_clipped_at_sprite_boundaries;
 
	// OPTIMIZATION -- How do we SORT THESE to reduce branching? 
	// More "structural" items will have these than say, random people.
	// BUT, maybe we can still use this feature like having a HAT that covers
	// more. 
	


	// Might not need this, but if we used a bounding-box collision for LESS THAN TILE SIZED objects
	// Again, not sure if we should introduce the PAIN IN THE ASS support for larger-than-tile-sized objects
	// either.
	// byte bounding_box_x;
	// byte bounding_box_y;
	// byte bounding_box_w;
	// byte bounding_box_h;




	
	//INSTANCE DATA, not ID data. (move to object_t)
	object_t [] passengers_ref; //NEED to be pointers/ref type. Use _p or _ptr? to indicate refs/ptrs?
	// This way we DON'T OWN the object, we're merely keeping refs as a list.

	// ---> HEY, can't we just "re-use" an object's INVENTORY for passengers, or DO WE NEED
	// a clear separation between lists of inventory and passengers?

	// GAME MECHANICs question: What purpose do these have? I mean, they can't be TOO phyiscally BIG
	// or they won't fit in our tile system and it'll look strange and function very difficultly/crudely
	// bumping into single tile walls. Unless they're "flying" in open fields or "ghosts" (flying through
	// blocked items)
	
	// OOOOH. Perhaps instead of thinking "Schoolbus", think [SMALL TANK] with gunner and driver.
	
	// Meanwhile, we already have SHUTTLE CRAFT.
	}


// not sure if really needed, but it'll help design other stuff.
enum weapons_e
	{
	pistol,
	rifle,
	sniper_rifle, //works with game mechanics?? WHAT IF WE SUPPORTED "picture-in-picture" view for "scopes" for long-range fighting.
	shotgun,
	flame_thrower,
	rocket_launcher,
	grenade_laucher, //don't forget different ammo types.
	axe,
	pipe,
	screwdriver,	// HEY... (see note 2 below)
	}
	// [Note 1] Should objects support (composition) a WEAPON_HANDLER for ammo consumption ,etc?
	// REMEMBER, if we use composition or inheritance, we'll need to SORT OBJECTS or split into
	// separate lists. SEPERATE LISTS is not that bad an idea for weapons, vs players, vs objects, etc.
	// especially for DRAW ORDER.
	
	// [Note 2] WHAT ABOUT THINGS THAT AREN'T NORMALLY WEAPONS? What list do they go into?
	// With COMPOSITION, it'll work object-wise. But what list if it's TWO KINDS of things?
	// Unless (and this could WORK and be TERRIBLE at the same time) we put them in MULTIPLE LISTS.
	// Object list.
	// weapon list.
	// player list. (etc)
	//
	// However, when it comes to DRAWING and (maybe?) DELETING that could be a bitch.
	
// SHOULD WE SUPPORT SPECIALIZED OBJECTS?

// A "weapon" is going to use a LOT of similar features. 
//	- Producing a projectile of some kind 
//	- Absorbing an ammunition of some kind (unless they're single-use only.)
//
// Do we want EVERYTHING to be completely based on scripting? Or can we "help" out?

// The POINT here is that we don't want a super-mega-general object_t class!
// We ONLY want METHODS and DATA in object_t that are COMMON to all (or 90% of objects)

// Another possibility is a COMPOSITION VERISON.
// - objects with position (all, but bare with me) include a POSITION_T
// - objects with a second sprite layer include a SECOND_SPRITE_LAYER_T
// etc









// Do we want (or need?) more spots, or support for clothing with multiple layers?
//enum clothing_types //should we have "clothing" all ATTACH to HARDPOINTS (above)
	
// Some damages may be ALIAS damages for some other ones. 
// Electrical is just BURNS for humans. But electrical resistance in some objects/robot/walls/etc
// it is a separate damage type and separate RESISTANCE value.
// As every object type has a RESISTANCE value for each of these types.
enum damage_types
	{
	burn, 			// negative? for cold damage. Or just both do positive.
	blunt,			// crushing.
	piercing,
	slashing,
	radiation, 		// DNA damage?
	electrical,		// Note, humans = burns. Other systems may treat electrical differently (like a wall.)
	
	// 
	exhaustion,		// perhaps? WON'T KILL YOU, but you'll get tired.
	temperature,	// NOT burns. But being "too cold" or "too hot" for too long, causing exhaustion. SWIMMING.
	mental,			// ?? Too much mental work, or being attacked MENTALLY by an alien species, drug, loud noises.  (Temporary, recovers)
	intelligence	// ?? Permenant unless cured. Just like [mental] but needs cured.
	
	//emotions		// Role-playing only? Sadness/Happiness? Anger/Apathy? 
	}

// - How do we handle diseases? Mostly scripting?



// GRID TUTORIAL
// http://www-cs-students.stanford.edu/~amitp/game-programming/grids/



// <------ We haven't decide on whether we're using GRID walls or EDGE walls, or both! (Or some special AUTO waller which has a fake height tile) And if so,
// how does the collision, and FEA code work??
	// Luckily, for FEA, walls HAVE NO VOLUME.
	// Not great for FEA/collision, we have to access THREE TILE OBJECTS for every object.
	// Did we just magically REDUCE our throughput by 3x?
	// Not likely, since many will already be in memory.




// WHAT DO WE DO FOR FEA?!
// Walls can have no volume... but they could have TEMPERATURE and PRESSURE?
//
// I think the INSIDE/VOLUME of the cell contains [pressure] and [volume]=1 cube and [temperature]
// The EDGES can have [temperature].
// BUT WAIT, you can't have a zero volume wall... 
// -> Imagine a GRID with ALL WALLS SEALED. Like a reactor. The inside is super-heated, the walls are cooler.
// -> There's SOME amount of [HEAT CAPACITY] in those wall edges. (No need for volume then?)
// -> The point being, you can't run FEA if you don't account for TRANSFER between the EDGE WALLS and the INSIDE.
// -> Is that going to MAKE THINGS COMPLICATED for the solver? That is, increasing the number of elements?
// -> FINITE-DIFFERENCE METHOD uses a REGULAR GRID (does it HAVE to?). So how the hell do we simulate WALLS?

// -> With the FEA involved... it REALLY SEEMS like we shouldn't go for EDGE WALLS, even if they may make it more
// 		modern...
// -> Sidenote: Fake walls that are autogenerated would be SIMPLE. The FEA just shows them as completely impassable. 
//				(Or is it completely PASSABLE?) Maybe NOT SIMPLE for the FEA. The graphics could be simple enough.
//				Ugh...

struct grid_node_s
	{
	bool center; //instead of bools, we'll have tile_t or whatever. Or DO WE ALSO need an FEA version of this? 
	bool left;
	bool top;
	// what if we make EACH tile have the top and left grid positions
	// So, the NEXT tile to the RIGHT has the RIGHT field (in its "LEFT" field)
	// and the NEXT tile to the SOUTH has the SOUTH field (it it's "top" field)
	}
	
/*
	What's the BEST PACKING for FEA?
	
	Where 1,2,3 = center, left, top
	
	[1,2,3], [1,2,3], [1,2,3], ...
	
	Any alternatives? Don't we need to know HOW BIG the inside fields are?
	
	Let's assume bool's for a moment.
	
	
	64-byte cache line, 8-bytes worth, is 16 TILES worth with no cross-packing (splitting the struct itself).
	
	000 000 ?, 000 000 ?
	000 000 ?, 000 000 ?,
	000 000 ?, 000 000 ?,
	000 000 ?, 000 000 ?
	
	Waste is 8-bits for every 64-bit cache.

	 - COULD WE FILL IT with anything else that's important?
	
	0123,4567,89AB,CDEF
------------------------------
	0001,1122,2333,444?
	0001,1122,2333,444?
	0001,1122,2333,444?
	0001,1122,2333,444?
	
	4-bits lost? Another vertical set?
	
	I've got to be overthinking this...
	
*/	

	
/*

   
   top, left, bottom, right
   1	2		3		4
   
  2-D grid:
   
   
   top (1)			top
   left	(2)			left (4)
   
   top	(3)			top
   left				left
   
   
	HOWEVER! This SEEMS CONFUSING AS HELL without the right diagram/explaination.
	Which means this is POTENTIAL for BUGS. 
	
	
	
	ONE CAVEAT: Remember we're not going to have ALL EDGES for a MAP of normal size!!
		- We'll need ONE EXTRA ROW AND COLUMN for the final walls! 
		- ALTERNATIVE: We can just FORBID letting people build on the 
			outermost rows/columns. Want to build a 20x20 ship? Get a 21x21 map.
			- This might actually be easier. 
				- NO CODE TRANSLATION anywhere. 
				- No difference between map_width and ARRAY_width. 
	
	
	Is there a slightly more elegant way to do this?
	
		- HELPER METHODS? that auto-translate each cell's walls, to the right ARRAY (i+1,j;i,j+1) position for that cell
		but you call it for ALL edges the same way.
		
		like this:
		*/

enum grid_edge
	{
	EDGE_TOP = 0,
	EDGE_BOTTOM,
	EDGE_LEFT,
	EDGE_RIGHT
	}

bool get_wall_edge(int x, int y, grid_edge which_edge)
	{
	pragma(inline, true) //set for FUNCTION level. might need -inline also enabled.
	//https://dlang.org/spec/pragma.html#inline
	
	grid_node_s [][] data;	 //fixme into a class
	
	with(grid_edge)
		{
		switch (which_edge)
			{
			case EDGE_TOP:
				return data[x][y].top;
			case EDGE_BOTTOM:
				return data[x][y+1].top;
			case EDGE_LEFT:
				return data[x][y].top;
			case EDGE_RIGHT:
				return data[x+1][y].top;
			
			default:
				assert(false, "BAD EDGE ENUM.");
			}
		}
	
	assert(false, "NOT POSSIBLE!");
	//return 0; //wtf  ... no wonder, I had the wrong damn true/false in the assert. MY BAD.
	}
		
		//this way we won't have a BUG in ONE PLACE we forget this distinction compared to the 80 places we use the functionality.
		

	
//	






// https://www.gamedev.net/resources/_/technical/game-programming/brain-dead-simple-game-states-r4262

// 
// Static analysis TONS OF CASE STUDIES
//		http://www.viva64.com/en/inspections/




// http://stackoverflow.com/questions/3480363/d-phobos-style-guide
/*
 - Don't specify how much to indent, but do indent to show structure.
 - Don't enforce a specific line length, but do keep line lengths readable.
 - Don't overlegislate naming, but do us a consistent naming convention.
 - Don't precsribe commenting styles (except where tools extract certain styles into documentation), but do write useful comments.

Some examples that they gave were that

 - Brace placement shouldn't matter but it should be consistent and readable.
 - On spaces vs tabs, they don't seem to care whether the coding standard says anything about it.
 - They're against Hungarian notation in C++ but think that it might be valuable in less type-safe languages.
 - They're completely against enforcing that there be a single return statement in a function.


https://dlang.org/dstyle.html
https://vibed.org/style-guide


NOTE: One good style to have is to not replicate the horrific C++ STL library:

void test_function()
	{
	vector_array.empty();
	}

	DOES NOT DELETE the array's contents. empty is actually returning a BOOL to tell
	you where or not it IS EMPTY.
	
	To CLEAR it, use vector::clear();


RULE
-->	This highlights the problem. If you're going to expose a FLAG condition, name it with the prefix
is_  or has_. Such as is_empty, or has_nodes. 

NEVER ALLOW CONFUSION between an ACTION function and a RETRIEVAL function.

 - And ALWAYS check return values.

MY POST: Enforcing Checks for Return code
http://forum.dlang.org/post/omvngddbslpjqnrngshi@forum.dlang.org

*/



// http://www.ss13.eu/wiki/index.php/Understanding_SS13_code#What_is_an_object.3F

class game_t
	{
	graphics_driver_t gfx;
	window_t [] windows; //do we need multiple windows? What's the use-case?

	world_t world; //contains maps -> layers -> data
	resource_handler_t resources;
	
	// network manager?
	// ???
	}
	

	
class window_t //screen or window? which is it??? BOTH?
	{
	string window_title;
	int width;
	int height; 
	int monitor; //if applicable. (read-only?)
	
//	void maximize(){} ?
	void maximize_to_borderless(){}
	void maximize_to_screen(){}
	void windowed_mode(){}
	void minimize(){}
	
	// Are these more SCREEN / Graphics_API related?
		void get_desktop_depth(){} //does 
		void get_dpi(){}
		void get_screen_width_inch(){}
		void get_screen_height_inch(){}
	
	bool resize(int x, int y, int w, int h){return 0;}
	
	
	//should we have a "viewport handler" that handles resizing them together? 
	// Is this going to be a dereference hell?
	//viewport_t [] viewports;
	
	viewport_handler viewports;
	}

class viewport_handler
	{
	viewport_t [] viewports;
	window_t attached_window;
	
	void resize(viewport_t vp, int x, int y, int w, int h)
		{
		vp.x = x;
		vp.y = y;
		vp.width = w;
		vp.height = h;
		}
	
	void two_port_resize(viewport_t vp1, viewport_t vp2, bool is_horizontally_split, int split_position_px)
		{
		if(is_horizontally_split)
			{
			vp1.height = attached_window.height;
			vp2.height = attached_window.height;
			
			vp1.x = 0;
			vp1.width = split_position_px-1; // WHAT ABOUT OFFSET-BY-ONE? 0 to X-1, X to width-1?? 
			vp2.x = split_position_px;
			vp2.width = attached_window.width - split_position_px-1;
			}else{ //vertical split
			// All I did was swap x with y, width with height, from the above. So FACT CHECK THIS.
			vp1.width = attached_window.width;
			vp2.width = attached_window.width;
			
			vp1.y = 0;
			vp1.height = split_position_px-1; // WHAT ABOUT OFFSET-BY-ONE? 0 to X-1, X to width-1?? 
			vp2.y = split_position_px;
			vp2.height = attached_window.height - split_position_px-1;
			}
		}
	
	// how do we handle SHARING A SCREEN with viewports?
	// With only TWO PORTS it should be easy because whatever is left, is given to the second one.
		
	// What about BORDERS? Are we going to simply draw everything INSIDE the screen, or are we going to
	// draw the VIEWPORT INSIDE a screen GUI?
	
	// We CAN support it (for say a GUI preview or whatever), we'll just need a different set of functions.
	
	}

class viewport_t
	{
	int x; //relative to screen. 
	int y;
	int width;
	int height;
	
	void draw(){}
	bool resize(int x, int y, int w, int h)(){}
	}






class resource_t
	{
	}
	
class audio_resource_t : resource_t
	{
	}

class bitmap_resource_t : resource_t // READ-ONLY?
	{
	bool is_immutable; // Don't mix BOTH of these classes in SAME ARRAY (requiring sorting to prevent branching), throw them in SEPARATE arrays.
	
	// we can use ASSERTs in setters to ensure we never attempt to modify an immutable bitmap. 
	}

//class writable_bitmap_resource_t : resource_t // Writable for script changes. (Be careful, fuckers!) Should this be a different class, or just a bool?
	//{
//	}




class draw_text_t
	{
	void draw(int offset_x, int offset_y){}
	void draw(float offset_x, float offset_y){}
		
	string data;
	
	int offset_x;
	int offset_y; //what about DPI? Does it matter? Ingame scripts may be a problem here unless we support a DPI-agnostic pixel mode (BUT, what if they NEED per-pixel accuracy?)
	
	int font_size;
	int font_number; // How do we do this? Hardcoded numbering? UGH. How to support custom fonts? Need a DYNAMIC BIND on string name... cached? UGH. SO MUCH CACHING.
//	bool is_immutable; //?
	}


struct xy_t  // best name? Best structure? What about ints? floats? doubles? absolute VS relative?
	{
	float x; //FLOAT?
	float y;  
	}



// WARN: HEY, should we rethink this to not "double-load"/abuse integers into meaning something more than
// a simple integer or index? I mean, it's not THAT crazy but it does require you to think about what you're doing.

class draw_stack_resource_t // Not sure if best here. BUT. Instead of DRAWING TEXT ONCE and caching into a writable bitmap, we just DRAW THE TEXT EACH TIME. (etc)
	{
	draw_text_t 		[] text_draws; 
	bitmap_resource_t 	[] bitmaps; //ptr to bitmaps used in drawing
		
	int  [] draw_order_selection;  //  drawing ITERATES THIS and selects which TEXT (pos) or BITMAP (negatives) to use.
	xy_t [] draw_offset; // X/Y OFFSET for each draw call 

	// Each offset is connected to the ORDER_SELECTION not text/bitmap structure. So we can simply iterate through both at same time. No wierd lookups.

	
	// Use NEGATIVE numbers for bitmaps??? WATCH FOR OFF-BY-ONE error.
	// note, we're talking  0 = (do nothing?)
	//   {1, 1, -1, -1}
	// draw first text twice, then bitmap twice.		
		
	
	bool is_immutable; //allows better caching. SET once done building.
		
	void draw()
		{
		foreach(int i, int value; draw_order_selection)
			{					
			if(value < 0)
				{
				int value2 = (-value) - 1; // -1 == 0, -2 == 1, -3 == 2, ... 
					//We *could* optimize this by re-using [value], BUT if 
					// someone changes [value] and then IF CHECKS AGAINST 
					// THE CHANGED VALUE, you've got an OBSCURE BUG.
					
					// ala: value = -value;   then later, someone if(value){} 
					// they're checking a DIFFERENT MEANING value with the
					// same variable name.
					
					// Perhaps we can re-use it but put a WARN around the if-else chain
					// to understand that VALUE can change meaning.
					
					// But... is this really SPEEDING UP anything significant?
					
					// If not, and profiling doesn't show results, STOP CARING
					// and keep the OBVIOUS, EASY-TO-READ version. 
					
//				bitmaps[ value2 ]     //WARN: Watch for off-by-one errors
				}
			else if(value > 0)
				{
				text_draws[value-1].draw(draw_offset[i].x, draw_offset[i].y); //WARN: Watch for off-by-one errors
				}
				
			else if(value == 0) 
				{
				// special case, or error?
				}
			}
		}
		
	void push_bitmap()
		{
		}
			
	void push_text()
		{
		}
		
	void pop()
		{
		}
		
	void finish() // done adding for now, but may change later.
		{
		}
		
	void finish_forever() //Will set is_immutable = true; and tell the resource manager "you can move this into a better cache location, such as the read-only list."
		{
		
		}
		
	// Does this have a FILE equivalent? Scripts will likely build these at load-time or run-time.
	// But we COULD cache some of these in files for convenience as well.


// WOAH WOAH WOAH. Do we need to COMBINE a bitmaps AND text into ONE stack? Maybe it's fine... maybe it's not needed?

	}

class resource_handler_t
	{
	resource_t [] audio_data;
	resource_t [] bitmap_data;
	
	resource_t lookup(string name){return new resource_t;}
	
	// NOTE difference between an audio SAMPLE and an INSTANCE. (or???)
	// that is, we need the waveform, seperate from a combination of CONFIGURATION for that particular waveform (such as volume)
	// and maybe even then, a list of RUNNING instances.
	
	// Likewise, do we need BITMAP vs TEXTURE?
	// Also, how are BITMAPs stored? Ideally we want TONS OF THEM attached to a SINGLE TEXTURE.
	//
	// Textures that CHANGE OFTEN will also
	}


















/*

	GAME TURNS PER SECOND
	
		- We could have this adjustable based on lag/bandwidth availability.
			- What's the alternative? And what's the failure mode of both versions?
				- Stuttering with people only updating RARELY across the screen. 
					- (Interpolate. However, it can still fail to be determinate. RUBBERBANDING.)
						- How the hell do you interpolate [RANDOM EVENT X]. Movement is easy. But what about constructing something. Or [strange thing Y].
						
			- Alternative: SLOW GAME DOWN. SS13 does this. I think. It's not so bad when running at 70-80% speed. But at SLOW speed (singularity escaping)
				it's BALLS SLOW and it's almost unplayable.
				
				
			ALSO NOTE, if we're not going TILE-BASED movement like SS13, these problems WILL BE MUCH MORE NOTICABLE.



	<---- ISSUE.
		Should we specify everything (both internally, and scripting) BY SECOND, or BY TICKS?
			- PROBLEM and WARNING.
				- If we do everything by second, then all code that only does that, will work fine with changing tick rates.
				- We can have helper functions (and script helper functions) that compute "TIME/TICKS_SINCE_LAST_UPDATE()"
					so a object can keep track of time between updates if necessary.
				- HOWEVER, if some asshole does a "int tick_count = 0;   tick_count++;" then whatever he DECIDES
					based on that number will be subject to changing update rate.
					
				- We can "fix" this through LARGE WARNINGS in documentation, as well as possibly, a "script test mode"
					that runs a script at multiple rates, and if any of the values change between them, it'll tell you
					that you're an asshole.

				- HOWEVER, HOWEVER. Some calculations WILL ALWAYS change with update rate, won't they?
					- WHAT HAPPENS if a script runs at RATE X, then updates to RATE Y, however, client B MISSED THAT FRAME.
						---> UPDATE RATE FRAMES MUST BE GUARNTEED AND SYNCED.
						
						Then again, no REAL code is run on the client. So maybe this isn't as bad an issue in our case.
*/








/*

SOLUTIONS from Game Progrmming Gems 3 (see page ~495)


		(FUN FACT: EVERYONE ONLINE SAYS THIS GAME HAD HORRIBLE DESYNC ERRORS.)




SIMULATING LAG:
	- We can use an external tool, and/or, put lag straight into our network layer--including the "single player" instance.
	
	---> WOAH, neat "feature.' Not sure how useful. But for HOSTS, we could have a button/setting that allows the local host player
		to "see" the lag of either the AVERAGE, or WORST player. So they know what those players are experiencing.
		
		Then again, "okay, there's lag." Now what? What's the benefit here, and what could a user possibly solve by using this? ...


 
NETWORK POINTERS.
	- Every object derives from a base class with a Pointer-To-ID conversion function using a look-up table.
		- CAVEAT: What happens when an object dies and a new one is instantiated in its place? A network packet can still refer to the old object.
			- Either 1) ID's are never re-used or super large range of references. (However, the LUT gets huge / slow! Maybe use HASH table.)
			- or 2) ID's can be re-used. But some small additional number to each ID which is invcremeneted when the object dies and the same ID
				is used for another object.

	

*/

// "C++ std::map is often implemented as red-black tree. It's the basic associative array. The other one (new) is std::unordered_map and is in fact a hash-map."

/*

NOTE <------- This paper was done for a LOCK-STEP RTS GAME. We "may or may not" be applicable to this scenario.






EVENT DRIVEN	

	- GAME STATE changes were moved into INTERFACE where possible.
		- "prototype building of rooms are all done locally, and only the final room is broadcast"



	- FINDING OUT-OF-SYNC was as simple as checksuming the NUMBER of objects and their positions... "and little else".
		
		
		"Once the game is out of state, looking at the differences tell you very little" 
		
				<----- SOLUTION!!!! I've talked about this before. Either keep an EXHAUSTIVE HISTORY of every change (whether full data, or, 
				incremental backups / fat data structures), or, NOTICE CHANGES IMMEDIATELY. The further away you notice "out of sync" from 
				the source of the triggering event, the more data has changed and it will obscure the source.
		




	- Main cause of out-of-sync bugs:
		- updating GAME STATE outside of a network packet interface. Send EVERYTHING through that interface, even for singe-player / local updates.
		- game code referring to the plaer that is local to the playeres machine
		- game code referring to the camera
		- game code referring to the system clock for timing, rather than the game turn. 
	
	- Event packets becoming irrelevant ("firing an employee that's already dead by time the packet hits.")






HOST MIGRATION
	- Do we want to support this as a "worst-case scenario" situation? The HOST dies, and the other players take over? (Shuttle automatically called?)
		- Will ANY players really have enough bandwidth to go around? (hopefully... I don't want to pay for bandwidth...)
		- Will players be able to DDoS the HOST to end the game? (eww...)
		- Will player computers be able to FIND EACH OTHER after the server dies? Do we REALLY WANT all players to know the IP's of each other?
			- They will anyway if we do some P2P mod downloading.... hmm.
		- Wouldn't it be easier for the HOST to just RELOAD A PERIODIC SAVEGAME? (Though some players won't reconnect... and we'll have to start up a 
			"resume savegame lobby" though players can still rejoin their SSD bodies after the game play begins again.)

*/







pragma(lib, "dallegro5");

version(ALLEGRO_NO_PRAGMA_LIB)
{

}
else
{
	pragma(lib, "allegro");
	pragma(lib, "allegro_primitives");
	pragma(lib, "allegro_image");
	pragma(lib, "allegro_font");
	pragma(lib, "allegro_ttf");
	pragma(lib, "allegro_color");
}

import std.stdio;
import std.conv;
import std.string;
import std.format; //String.Format like C#?! Nope. Damn, like printf.

import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;


import std.meta; //for aliasSeq!
// https://dlang.org/phobos/std_meta.html
import std.container : SList; //singlely linked list

// Interal files

import units;
import graphics;








/*



http://pages.cs.wisc.edu/~remzi/OSTEP/

GREAT ONLINE OS book. Includes huge section on concurrency.

	"Note that if the data structure is not too slow, you are done! No need 
	to do something fancy if something simple will work."

TIP: MORE CONCURRENCY ISN’T NECESSARILY FASTER
If the scheme you design adds a lot of overhead (for example, by acquiring
and releasing locks frequently, instead of once), the fact that it is more
concurrent may not be important. Simple schemes tend to work well,
especially if they use costly routines rarely. Adding more locks and complexity
can be your downfall. All of that said, there is one way to really
know: build both alternatives (simple but less concurrent, and complex
but more concurrent) and measure how they do. In the end, you can’t
cheat on performance; your idea is either faster, or it isn’t.



https://msdn.microsoft.com/en-us/library/windows/desktop/ee418650(v=vs.85).aspx
Windows Performance

The performance of synchronization instructions and functions on Windows vary 
widely depending on the processor type and configuration, and on what other 
code is running. Multi-core and multi-socket systems often take longer to 
execute synchronizing instructions, and acquiring locks take much longer if
 another thread currently owns the lock.
 
However, even some measurements generated from very simple tests are helpful:
 - MemoryBarrier was measured as taking 20-90 cycles.
 - InterlockedIncrement was measured as taking 36-90 cycles.
 - Acquiring or releasing a critical section was measured as taking 40-100 cycles.
 - Acquiring or releasing a mutex was measured as taking about 750-2500 cycles.


If one thread is generating data and another thread is consuming data, they may
 end up sharing data frequently. This can happen if one thread is loading 
 resources and another thread is rendering the scene. If the rendering thread
  references the shared data on every draw call, the locking overhead will be 
  high. Much better performance can be realized if each thread has private data
   structures which are then synchronized once per frame or less.

Lockless algorithms are not guaranteed to be faster than algorithms that use locks. 
You should check to see if locks are actually causing you problems before trying to 
avoid them, and you should measure to see if your lockless code actually improves 
performance.

Recommendations

 - Use locks when possible because they are easier to use correctly.
 - Avoid locking too frequently, so that locking costs do not become significant.
 - Avoid holding locks for too long, in order to avoid long stalls.
 - Use lockless programming when appropriate, but be sure that the gains justify the complexity.
 - Use lockless programming or spin locks in situations where other locks are prohibited, such as when sharing data between deferred procedure calls and normal code.
 - Only use standard lockless programming algorithms that have been proven to be correct.
 - When doing lockless programming, be sure to use volatile flag variables and memory barrier instructions as needed.
 - When using InterlockedXxx on Xbox 360, use the Acquire and Release variants.

*/

class pos_t 
	{
	int x, y;
	
	// should this have a REFERENCE to the map it's refering to?
	// 		(or an integer lookup to it?)
	
	// or should the objects be located INSIDE the relevent map?
	// NOTE THIS. <---
	
	int map_id;
/*
	 WHO OWNS AN OBJECT? Does each object specify the MAP it's located on,
	 OR, does each map have a list of objects?
	 
	 - In the second case, the map is implied--each map owns its objects
	 so the coordinate transforms are clearly relative to the map position
	 in global space. HOWEVER, CAVEAT. If an object CHANGES MAP, then it MUST
	 BE MOVED TO A NEW ARRAY. Now that, isn't terrible... EXCEPT what about
	 ALL THE REFERENCES TO THAT OBJECT held by other objects?
	 
	 - I know "my pants" contains "my wallet."
	 - But, when I change maps, I, my pants, and my wallet are new IDs.
	 - All previous references (like my pants holding an inventory reference
	   to the wallet inside) must be re-evaluated. 
	 - This COULD be slowish, but shouldn't be a problem. Boundary changes
		 should be rare in time, and relatively small numbers of objects.
	 - NOTE that the "picking" algorithm is simple here because all the objects
		 related in this case are MOVING TOGETHER at the same time. I move, with
	   my pants and wallet, at the same time and event.
	
	 - HOWEVER, now the HARD PART. What about an object that has references
	 		to our player? Or our player's object, like a PDA?
	
	 EXAMPLE: I move, with my PDA in my pocket, onto a ship.
	
	 - All my internal objects' references in the inventory tree, are updated.
	 
	 - What about the machine sending messages to my PDA, that's still located
	 		on the space station? How does IT know the new object ID?
	
		Legend:
			ID			- A unique ID assigned at creation. Always unique, and NEVER changes.
			Index		- An index into a linear array. Indexes can be SEGMENTED
							so each map has a #, and each index is unique. (ala 2-D array i,j = index, map_number) 
			Pointer		- A memory location
	
		SOLUTION #1 - SIMPLEST. More memory, maybe slower... OBJECTS NEVER CHANGE INDEX/PTR location.
					Which means each object must also mention the MAP_ID it is attached to.
					Changing maps is as simple as changing the position and map_id indexes
					for OUR ONE OBJECT and no others.
				
					CAVEAT: Objects may NEVER MOVE IN MEMORY, so there's NO WAY to do memory
					defragmentation. (Do we EVER need to move objects? We do if we need to SORT
					for some reason, but I don't know if we do.)

						--- ALT#1: WAIT, OR IS THERE? Even if we do this, we just need to
						make sure when we MOVE an object in memory, we change any other
						references to it. Which means we'll have to do a COMPLETE
	 					LINEAR SCAN o(n) of all objects.

						--- ALT#2: Do solution #2 anyway (but simpler). Keep reference
						 lists. Which will be helpful anyway for validating an object
						reference never CRASHES THE GAME.
	 
						QUESTION THREE: How do we handle object deletions when another
						object holds a reference to it?
	
						Lua/Script code:
	
						if(object_exists(&obj)){do stuff}
	 
							--> IF NOT ATOMIC, THIS COULD STILL FAIL.
	
						ALTERNATIVE? require callback/event handler functions for any object
						references for when an object dies? So you can assume a function
						runs UNTIL the callback occurs. BUT WHAT ABOUT ATOMICS? If we 
						run scripts in parallel? What happens when an object script is 
						running and the CALLBACK OCCURS DURING THAT SCRIPT. Do we terminate
						the main script? Run it like an exception???
	
						ALTERNATIVE? objects ARE NOT DELETED. At least not immediately.
						So a script can still reference a "dead" object, at least briefly,
						until the callback can run. But what kind of ERRORS will happen?
						(Reading data from a dead object, like health, is [UNDEFINED].)
						So in this case, deletions are run OUTSIDE of scripts. Scripts first,
						and deletions are queued up by D_PHYSICS() and LUA_SCRIPTS() marking them,
						and then all deletions are processed and scripts are notified NEXT
						TICK to run their callbacks or whatever handler they want to deal
						(in the script) with the fact those objects are gone .
	
							object_t 
								{
								etc 
								bool going_to_be_dead;  
								bool is_dead; 
								}
			
						SCRIPT:
								 TICK #2010
							void tick(){  read_data_from(obj&); } will still be there
	
								 TICK #2010.5 (after 2010 tick(), before 2011 tick() is executed)
							void deal_with_deletion(obj&)
								{
								remove_obj_from_reference_list(obj&);
								number_of_active_PDAs--;
								update_HTML();    prints all PDA user names in list.
								update_dialog();  sends update to screen
								}
	
						CAVEAT: Could a poorly (or WELL) written script FAIL SOMEHOW
							 using this method? NOT SURE.
	
						ALTERNATIVE: Same as above, but only handle when a message is attempted
						to send to a dead object. (Might not be as nice this way... you won't
						KNOW someone is dead until you TRY to send_message_to(obj&) them.)
	
						When PDA is destroyed, and broadcaster sends data to it, it'll get a 
						ping-back / exception saying "sorry, mate, he's gone now."
	
						ALTERNATIVE? do some sort of publisher/subscriber or other model
						that is inherently tolerant of objects being destroyed.
	
						For example, if a PDA is destroyed, it simply won't be there to listen
						to the messages being posted by the publisher. But can the publisher
						find out when the number of subscribers changes? Or does that break
						the encapsulation purpose of the sub/pub paradigm? Should all objects
						send messages blindly, with no clue whether they're be heard? And if so
						does that make coding general-purpose scripts insanely difficult?
	
	
	
		SOLUTION #2 - Objects change arrays (and array index #) upon TRANSFER, and ANY REFERENCES
					are TWO-WAY references. That is, any time a script makes a reference,
					it goes into the object's "reference table/list". Whenever that happens
					the object WE'RE POINT AT is notified and keeps a table of it's own
					called [referenced_by]. So when we change, all other objects know and
					update their records. [NEED ATOMIC OPERATIONS? No transfers occuring 
					while scripts are run. Otherwise, it could change mid-script and maybe
					blow up somehow.]
	
				SECOND: How do we handle DELETIONS in THIS CASE for scripts?
						Likewise, TRANSFERS will also need to occur outside of script phase?
	
		SOLUTION #3 - SLOW AS HELL, possibly. But objects are NOT refered to by a pointer
					or an INDEX. They're refered to by an object_id which can be located
					anywhere and must be SEARCHED FOR (and possibly cached which reverts to
					search if the cache is dirty--when objects are moved)?
	
					so instead of   ptr = 0x0203; or  index = 320 -> objects_list[320]
					we have:   object_id = 23201 which could be at ANY POSITION in 
					objects_list, so a linear search must be done. Periodic sorting
	 				will reduce search time. ADDITIONALLY, references to objects
					could be CACHED. So you use object_ref_t = 23201 which goes and
					finds the INDEX/PTR that matches ID=23201. (ala SQL) And then saves 
					it for future accesses. (ptr=0x111 or index=2010) Each access must
					still confirm it's attached to the right position, so we go there,
					confirm ID=ID, and then produce the message.
					(ALSO if we can only delete AFTER scripts run, once it is checked ONCE,
					it can never FAIL until either the next tick(), or even better, it only
					invalidates the REQUIRE_CHECK if SOME OBJECT SOMEWHERE is deleted, and
					even better, object_ref_t's REQUIRE_CHECK is specifically notified upon
					a deletion instead of polled using some sort of backwards reference list.)
				
					The ADVANTAGE of this is the COMPLETE DECOUPLING of	memory location
					from ID #. So an object 222 could literally be anywhere, in any static pool,
					in any array of any size, OR EVEN SWAPPED TO VIRTUAL MEMORY on the hard drive,
					and IT CAN MOVE and still have no changes in script code.
				
					CAVEAT: How do we assign object ID numbers? Do we run a o(n) linear scan
					of EVERY NUMBER until we find an empty spot? And do we need to constantly
					be SORTING this list (which invalidates caches too!)?
	
					I'm not really arguing for this case, but it's very different, so it's
					interesting for comparing-and-contrasting techniques and cost/benefits.
*/
}


class oref_t	// memory safe object reference for when scripts reference another object.
	{
	object_t *obj;  // ?
	bool is_cached; // ?
	}

//class or struct?
// WHO OWNS OBJECTS? per map, or world / GAME_INSTANCE?
// If we support creation/deletion of "maps" (like ships) we can simply delete everything inside that map when we're done.
// Then again, the static pools (are we using?) will take care of clean up... I think?

class object_t // DIS GOIN' BE BIG. Do we need to worry about DATA-ORIENTED at all?
	{
	double x, y; // for space? How do we do this? (Even if floats can = integers up to 16,777,216; we don't want to CAST to an INT EVERY BLOODY TIME we use an array lookup into a map!) 
	int cached_x, cached_y; // for ARRAY reference bounds.
	// UNION these? Still, we're wasting plenty of space if we plan on iterating through TONS of these!
	// But isn't this struct already huge? Maybe?
	
	// We *could* either have a space_object_t and a map_object_t, but then we'd need to
	// convert every time they jump into space. And every conversion could imply a BUG.
	
	
	// -> WAIT WAIT WAIT, what the hell am I thinking? Aren't we SUPPOSED to support pixel-perfect movement?
	// Maybe we'll just use x and y for CACHED faster array lookups instead of converting to double each time.
	// Update the cached_x, cached_y every time we move.
	
	
//	int w, h;  //do we support w,h? larger than single tile monsters?
	int map_id;



	bool pending_deletion;  // will be deleted after all scripts have run this tick()
	bool is_deleted;		// is officially junk data ready to be populated by the next creation.		
		
	void create(){}
	void destroy(){ pending_deletion = true; }		//queue? enqueue? Do we need to TELL OTHERS here? (or,)
	void finish_destroying(){}	//"delete" is a keyword, can't use that. ONLY WE should call this. Do we need to TELL OTHERS here?
	
	void build_cached_xy()
		{
		cached_x = to!int(x); // don't forget to update CACHED x and y.
		cached_y = to!int(y);
		}
	
	void move(double abs_x, double abs_y)
		{
		x = abs_x;
		y = abs_y;
		build_cached_xy();
		}
	void move_rel(double rel_x, double rel_y)
		{
		x += rel_x;
		y += rel_y;
		build_cached_xy();
		}
		
	void teleport(double abs_x, double abs_y, int map_id) //moves to new map, may need to fire off
	//	REFERENCE CHECKS to make sure scripts know what array it's in now if it moves in memory.
		{
		move(abs_x, abs_y);
		this.map_id = map_id;
		build_cached_xy();
		}
	
	// SCRIPTING
	// ======================================================
	
	string [] scripts; 
	// maybe? (also, same script, for many objects. USE REFERENCE TYPE.) 
	// On the bright side, we won't have to clone value types all over the place
	// since once we pay for using a ref type, unique ones don't need to be cloned.
	
	// QUESTION: Is there any case where we'll be screwed by refs? Such as deleting
	// one that actually (incorrectly) shows up in two places?
		
	// Data registers for the script
		int 	[] script_integers;
		double 	[] script_doubles; 
		string 	[] script_strings;
		oref_t 	[] outgoing_references; 		// list of objects this script knows about
		oref_t 	[] objects_that_reference_me; 	// list of objects that know about me
	
	// how do we expose and call NON-STANDARD functions? 
	// for scripts such as push(), kick(), punch() etc?? (or are those... standard?)
	// 
	// Perhaps:
	bool recieve_message(string function_name, 
						 int [] i_args, 
						 double [] d_args, 
						 string [] s_args, 
						 oref_t [] obj_args) //etc.  UGH. THIS GOIN' BE SLOW!
		{
		// Do we need to be able to send object ID's/pointers as well? Probably.
			
		// either return false if the function doesn't exist, or
		// maybe we send_message() back to the calling object 
		// with message type MSG_OBJECT_NOT_EXIST
		// (What happens if THAT one doesn't exist anymore? infinite repeating echo?!)
		return true;
		}
	
	void call_tick_script()
		{
		}
	
	void call_deletion_script() //deconstructor in the script. notify others of stuff. needed?
		{
		}
		
	
	
	
	
	
	
	/*
	 SHIT. When we're in SPACE, we need FLOATs. But when we're inside, we need INTs.
	 Or... do we? Durr, we can move pixel (and sub-pixel) coordinates.
	
	
	 ss13 only supports huge floating monsters, otherwise single tile.
	 collision detection becomes more difficult (but... IS IT reallly THAT
	 hard?)
	
	 ONE PROBLEM. How do we handle SCRIPTING for larger objects?
	 When a door gets a hand_push(pos_t from_here) event, does it assume
	 the position is THE position of the object?
	
	 If the script can access ALL the object's data, it can read .width and .height
	 if and when it's applicable.
	
	 When a 2x2 object moves, it can bump into TWO DOORS at once, and it'll
	 send them both a hand_push(pos_t from_here) command with... DIFFERENT 
		x/y coords? (the x/y of the piece that is touching the door)
	
	 QUESTION: Do we send the x/y of the piece touching, or the x/y of the 
				CENTER of the object?! Because that can have different effects!
		
		We could FORCE scripts to deal with this difference separately
		by having hand_push(&obj_t) and hand_push(&BIG_OBJ_T) overloads.
		
		So a different function is called when you talk to a BIG object.
	
	 FOR NOW, KEEP IT SIMPLE AND JUST DON'T IMPLEMENT BIG OBJECTS. GOSH. 
	 */
	}

struct particle_t //Question: which reference frame are we attached to?
	{
	double x, y;
	int map_id;
	// auto reference_frame;
	}


// TEST IDEA, custom FIXED array sizes (using a multiple of say 64, for cache lines?)
// using generics <---


enum LAYER_TYPE
	{
	DAMAGE = 0,
	FLOOR,
	WALL
	}

class layer3_t (int w, int h, LAYER_TYPE t) //also take T type? which yields different layer construction???
	{
	// How do we determine COMPOSITION from this?
	// Simply enough, we could just use DIFFERENT CLASS NAMES instead of generics.
	
	static if(w <= 0 || h <= 0)
		{
		static assert(false, "Invalid dimensions for layer3_t!");
		}
	
	static if(t == LAYER_TYPE.FLOOR)
		{
		short [w][h] map_id;		
		float [w][h] data;
		long  [w][h] temp;
		}
	static if(t == LAYER_TYPE.WALL)
		{
		short [w][h] map_id;		
		float [w][h] data;
		long  [w][h] temp;
		}
	static if(t == LAYER_TYPE.DAMAGE)
		{
		float [w][h] data;
		long  [w][h] temp;
		}
	// and float, and byte, and ...
	// need custom type support?
	}


//alternative...
class damage_layer_t(int x, int y){}
class floor_layer_t(int x, int y){}
class wall_layer_t(int x, int y){}

// do we need any extra information? Or should a map_t ITSELF be the ship/etc
// and all data should be inside that. That is, don't use these classes at all.

// couldn't we also use an alias/(C++ typedef) with the parameters?
// that is, for example, a ship3_t is simply an alias for map3_t!(1000,64);

class ship3_t
	{
	map3_t!(1000, 64) map;
	}
	
class shuttle3_t
	{
	map3_t!(64, 64) map;
	}

// ala

class galaxy_t //best name? galaxy? univese? world?			//GAME INSTANCE? 
	{
	map3_t!(1000,64)[] 	ships;
	map3_t!(64,64) 	[]	shuttles;
	
	// verses 
	ship3_t 	[] ships2;
	shuttle3_t 	[] shuttles2;
	
	// ALSO, do we want the WORLD to have a fixed maximum of maps per type?
	// Or can we get away with dynamic arrays since these are HUGE swafts of
	// data anyway.
	
	}


class location_t  // bounding rectangle for rooms ("I'm in [the bar].")
	{
	string name;  // name of a room, or location
	//enum type; // Needed? ala BAR, KITCHEN, ARMORY. 
	// So you can easily "find all kitchens" and color them on a minimap 
	// or whatever.
	
	int x, y, w, h;

//	bool destroyed; // possible? how do we deal with this case? just ignore it?
	}



//SIZE WARNING. If we throw this ANYWHERE ON A STACK, it will be HUGE.
// If say, stack limit is 16MB, ONE double percision layer of 1445x1445 will
// be just under that. And that's just ONE of the layers.
// We ALSO better make sure we're not creating this on-the-fly. We could
// "double buffer" (more like page flipping by changing the pointer!) them 
// for [BEFORE CALCULATIONS] data and [AFTER CALC] data.

class map3_t (int w, int h)
	{
	string name; //ship name, etc.   ala "USS Nautico" and "Tangorine Station 13" 
		
	int width=w;
	int height=h;
	
	// the most common case gets it's own function, more custom ones later.
	void draw(int offset_x, int offset_y)
		{
		}

	void draw_zoomed(int offset_x, int offset_y, float zoom_factor)
		{
		}

	
	// Do we want separate variables, or a combined layer array?
	// Probably the first for speed instead
	// of requiring another dereference to determine the layer.
	// ala layer[LAYER_DAMAGE].data[30][30].toomuch();
	
	layer3_t!(w,h, LAYER_TYPE.DAMAGE) 	damage_layer;
	layer3_t!(w,h, LAYER_TYPE.FLOOR) 	temperature_layer;
	layer3_t!(w,h, LAYER_TYPE.WALL) 	other_layer;
	
	// I mean, is the above REALLY BETTER than this: ???
	damage_layer_t!(w,h) 	damage_layer1;
	floor_layer_t!(w,h)  	floor_layer1;
	wall_layer_t!(w,h)		wall_layer1;
	
	
//	int [2][2][2][2][2] xldsl; YOU CAN DO THIS hahah.
	
	// HEll, why not just:
		// WARNING: Static arrays in D cannot exceed 16MB!!
		// Will this explode?
		// 1000 * 1000 * 8 (double) / 1024 / 1024 = 7.623 MB
		// 1000 * 1000 * 8 (double) / 1024 / 1024 = 0.95 MB (DUH, forgot we can go UP TO 16MB not 1 MB)
		
	// 1000 * 1000 WORKS -- Maximum size? 
	// 1200 * 1200 WORKS
	// 2000 * 1000 WORKS
	// 2100 * 1000 FAILS
	// 2098 * 1000 FAILS
	// 2097 * 1000 WORKS = 2,097,000 (Hmmmm, 2^21 = 2,097,152, coincidence?)
	//	is 2,097,152 * 8 bytes = 16,777,216 = 16 MB?! DURRRR. = 2^24
	
	
	// 4096 * 4096 * 1 byte = 16,777,216 = 16MB
	// 2048*2 * 2048*2 * 1 byte = 16MB  (MATH, BITCHES.)
	// 2048 * 2048 * 4 byte = 16MB
	// 4,194,304 * 4 byte = 16MB
	// 2,097,152 * 8 byte = 16MB
	// 1448.15468787^2 * 8 byte ~= 16MB
	// 1445^2 * 8 = 16,704,200 < 16MB  <--- CLOSE
	//
	// Max SQUARE SIZE is 1445 x 1445 (if we use a double anywhere.)
	// Rectangles can of course be anything that adds up to less than 16MB.


	// HEY
	// github BUG REQUEST for larger static arrays (turns out it's some insane
	// old assembler compatibility with some random shit).
		// https://github.com/dlang/dmd/pull/6081
	
		// https://github.com/dlang/dmd/pull/6503	(rebooted as this)
	
	// This may be resolved in a future compiler build (but not MINE as of Jan-29, 2017)
	// There MAY be a much newer version of the LDC compiler ARLEADY
	// and DMD may be updated as well. But if github issue isn't resolved yet,
	// so maybe not?

	
	// - We could have multiple GRIDS perfectly aligned with each other?
	// - Treat them like "warping" from one ship to another... almost.
	// - HOWEVER, this could have BIG PROBLEMS if we write code that
	//		assumes we're 'teleporting" to a new ship but really we're just
	//		on another part of the same ship/planet/etc.
	// - DURRR, why not just use [TEMPLATE MAGIC] so that any MAP designed
	//	above 1000x1000 (or whatever) uses DYNAMIC ARRAYS. It might even
	//	be possible to AUTOMATICALLY ADJUST when then static array template fails.
	// 	Though, we'd definitely want to make a LOG FILE mention of it.
	//
	//  Also, a hardcoded limit is still pretty good for people. Tell them
	//  "Don't go over 1000x1000" 
	
	double [w][h] TEST;
	ushort [w][h] bg_layer2;
	ushort [w][h] wall_layer2;
	ushort [w][h] floor_layer2;

	int    [w][h] damage_layer2;
	float  [w][h] temperature_layer2;
	float  [w][h] other_layer2;
	
	location_t [] labeled_rooms;
	}

void test3_version()
	{
	map3_t!(1445, 1445) map; 
	writeln("Seems to be working so far...");

	}

// 10000 FOOT QUESTION: Should we do ANY of this linear array shit at all?
//
// Yes-and-no.
//
// -> Particles, follow binary-coded rules, sure.
//
// -> Objects (here is the big IF) --- RUN SCRIPTS. WHICH ARE INSANELY SLOW
//										 compared to binary code.

// Script time comparison
//
// http://4.bp.blogspot.com/-Ojzabe1JG-k/UiXWXcyGnOI/AAAAAAAAAf0/ctFuTlP1MZo/s1600/speed.floats.png
// https://codeplea.com/public/content/script_time.png
// http://flux242.blogspot.com/2013/09/javascript-vs-perl-vs-python-vs-lua.html
//
// --> Programming languages benchmark (C is 1.0 second first test, 2.3 the second)
//		Dlang is 1.1s and 2.4s using LDC (slightly slower for GDC)
//
// 	https://attractivechaos.github.io/plb/plb-lang.png
//
//		Lua-JIT is 6.8x slower one test, 2.2x the second.		(luajit.org)
//		Lua:Lua is 50x.5 slower one test, 68.3 the second

// IDEALLY
// Use homogenious, sequential data:
// per
// http://harmful.cat-v.org/software/OO_programming/_pdf/Pitfalls_of_Object_Oriented_Programming_GCAP_09.pdf
/*

instead of  (ignore variable names)

class object_t
	{
	local_transform_t x;
	world_transform_t y;
	(etc)
	}

object_t [] array_of_objects;

do:

local_transform_t [1000] x;
world_transform_t [1000] y;
(etc) 			  [1000] z;

 -> A PROBLEM ARRISES THOUGH!

What if SOME object types DON'T USE the full list of arrays?

 -> Do we simply leave it empty and unused in that array? (what about fragmentation? 
	holes of useless data in linear array == less dense packing, undoing the optimization)
 
 -> Do we NOT leave it empty, and then we have to have DIFFERENT index numbers?
 
 -> Do we have some sort of split where:
	
	particle_t	has linear arrays (local + world transform)
	object_t	has linear arrays (local + world transform + OTHER STUFF)
 
Using GENERICS we could automate TONS of this for various configurations.

	---> DIFFERENT OPTION:
	
		ONLY PUT COMMON STUFF ACCESSED RAPIDLY, into those arrays and leave the rest PER OBJECT? 
		(or, in a "per special object" linear array) 

		(WAIT, ARE THESE TWO CASES DIFFERENT???)


		1A. 
			
			locals [] x;
			worlds [] y;
			object_t_custom_data [] z;
			particle_t_custom_Data [] q;

			where,
		
			class object_t exists solely in linear arrays.
	
		2B.
			
			locals [] x; // COMMON linear arrays
			worlds [] y;
			
			object_t [] z; // Discrete objects that reference linear arrays for COMMON data.
			particle_t [] q;

			where 
			
			class object_t
				{
				int index_into_common_linear_arrays; //faster, packed data.
				
				//slower, random data
				int discrete;
				int data;
				int per_object;
				}
				
				
	LOOK AT THE PRESENTATION LINK URL about rearranging heirachies.
	
		WARNING: "Multiple threads with heavy cache use may thrash the cache."
*/



// LIGHTING?

// Fluid drawn on top of tiles?
//	- Fluid drawn with intermediate tiles blended in. Use random curve depth in filler tiles to make it seem more natural/detailed.


// DON'T FORGET, whatever VIEW we use, we'll need ART ASSETS. And picking a popular view means easier time using pre-made assets.

// WHAT ABOUT FAKE ISOMETRIC?
// - 2-D + wall height over 1 tile! (You can walk behind a wall.)
//		- Secret of Mana / Chrono Trigger
// 		- The key here is that walls CAN GO OVER the size of a floor tile.


// Jagged Alliance 1 moved walls UP and to the LEFT. Both.

// Isometric? What about OTHER ANGLES?
//
// - Spacebase DF-9
// - Diablo 1/2 (Cavalier oblique)
// - Theme Hospital
// - Crusader: No Remorse is 2:1  width/height

// Fallout is axonometric

// If we go ISOMETRIC, we sure as hell can't move TILE BASED. We need to be able to move north/south/east/west for arrow keys!

// OBLIQUE
// http://toxicdump.org/blog/view/7/Haunted-Mansion-DevBlog-_02---Oblique-vs-Isometric-perspective-and-perfect-depth-sorting



//KATKO
//=============================================================================

// CODE GUILDLINES

/*


	units
		Should we integrate units of measure, into variable names?
		
			px - pixels
			pt - point (font size)
			mm - millimeters
			m - meters
			t - time
			T - temperature (or the opposite, whatever literature normally uses)
			hz - hertz (cycles per second? so just add "cycle" so unit<cycle,__second> )

			What about relative and absolute coordinates though?
				- apx or a_px - absolute pixels?
				- rpx or r_px - relative pixels?
				
				- Relative... to what? For example, a tile on a map, relative to the map start index, 
				relative to the center point of the station map in absolute space and rotated from
				 the galatic center. Kind of adds up to the impractical.
				
				- We could do abs/rel based on surrounding code context / comments, instead of 
				encoding it straight into the variable name. Variables SHOULD have clues to
				their context based on their INTERFACE (class name and structure), 
				function name, surrounding code, and if failing that, comments.

			Should we consider a UNITS SYSTEM? Perhaps a STATIC template units system that
			checks for unit conversion errors?
			
			
			https://code.dlang.org/packages/units-d		-- ported Boost::units
			
				http://www.boost.org/doc/libs/1_60_0/doc/html/boost_units.html
			
			https://code.dlang.org/packages/d-unit
			
			http://forum.dlang.org/thread/mailman.1208.1360603449.22503.digitalmars-d@puremagic.com

			---> I DO NOT need a hardcore units library. Just simple unit checking.
				--> Alternatively, there may be some other way to guarantee units cannot be accidentally checked. Through interface design.
				
				--> For example, instead of raw ints/floats/etc, use: (could be NASTY though.)
					
					millimeter_t 	x; //has methods for operator overloading that takes a (meter_t) argument.
					meter_t			x2;//has methods for millimeter_t
					
					time_t 			y;
					temperature_t 	z;
					
					raw_t			value;

				--> What if they COMPILE AWAY to raw variables outside of debug mode?
					--> HOW DO WE VERIFY both modes though? (ugh.)

				--> Do we REALLY want to hardcode every possible case? 
					What happens when we need something that MIXES THEM ALL?
				
					class mix_units<unit1, unit2, unit3>
				
				
				--> Does thiS HELP ANYTHING?
				
				--> Does this method deal with UNIT MULTIPLES?! 
					--> If we REMOVE the ability to multiply by it's OWN TYPE:
							-->    meter_t * meter_t    		FAILS
							-->    meter2_t(meter_t, meter_t)   WORKS		<-- meter SQUARED
							--> 
							  
							
							auto acceleration_1 = meterot_t(meter_t, time_t, time_t);  - (UGLY SYNTAX. "Meter squared, OVER time")
							auto acceleration_2 = meterot2_t(meter_ot_t, time_t);
							
							
							
								
								meter2ot_t		- okay ish?	NOTE though, we're using t for TIME and for _type. BUT, just a time variable is time_t???
								meter2_ot_t		- ugly?
								meter2_o_t_t	- UGLY?
								meter_meter_over_time_t - VERBOSE
								meter_squared_over_time_t - VERBOSE
								meter2__t_t		- Implied "over" with second underscore?
								
								- BUT, impossible to fuck up, right?....I hope?
			
					// WAIT <---- should we use t for TIME?!?!? what about SECOND/MINUTE/HOUR.
			
								meter2__sec_t	- meter^2 / sec 	(as opposed to "time")
								meter2_o_sec_t
								meter2_sec_t	- BAD. Can't tell if meter^2 * second, or meter^2 OVER second
			
			
					--> What about variables with TONS OF UNITS? Is the variable gonna be HUGE?!
							
							
					--> We'd have to DEBUG either DOZENS of combinations or do some TEMPLATE MAGIC 
						that allows this.
				
				
					--> WILL TEMPLATES MAKE FOR EASIER WRITING?
					
						auto x = unit<meter_t, meter_t, __second_t>(300)	//NOTE, __ prefix means OVER. so __second_t is 1/second
						
						auto x = u<m, _s, _s>(300); 						// super compact notation
						
					--> HOW DO WE DEAL WITH UNIT CHANGES? (derivatives/integrals)
						
						x.reset_units(m,_s) //manually go from m/s^2 to m/s
						
						auto y = unit<sec>(10) * x; //HOW DOES THIS WORK? seconds * _seconds * meters = meters 
							   == unit<sec>(10) * unit<meter, _second, _second>(300);
						
	classes/interfaces
	
		class lower_case_t		- t for type
		class lower_case_ti		- ti for type interface (and interface class)
		interface lower_case_ti - same. 

		WHAT ABOUT PRIMATIVES? datatypes as opposed to larger, monolithic things.
			
			bitmap_t
			fixed_t
			
			or
			
			bitmap
			fixed
			(etc)

			ADVANTAGE of bitmap_t is you can do:
			
			func1(bitmap_t bitmap) //instead of func1(bitmap the_bitmap)
				{
				//do stuff to bitmap
				}



		--> class variables 
		
			_data;  //prefix underscore. Necessary? Useful?
			
		--> class methods

			method();

	
	local variables
		local_variable
		i, j, k				- clear, loop index variables
		itr					- temporary iterator

	global variables
		int CONSTANT_VARIABLE	- 
		int VARIABLE			-
		enum ENUMERATOR {ENUMERATOR_VALUES}	-
		
	
	
	Special cases:
		Lookup variables?	
			map_tile_id - id for index into an array of map tile TYPES.	Best way?
			 
		
	

	INTENTATION

		Style: Whitesmiths Style (with exception on else/elseif?)
			Advantage: Blocks are conceptually all on one intent. More-or-less Python with brackets, too.



		KATKO STYLE: (open to debate...) Only difference is else/elseif

			if(x == 3)
				{
				foo();
				} else {		// I perfer else to be indented and combined. But is it worth adding an EXCEPTION to a known style?
				bar();			// else is so damn simple (no arguments) that, why but it together?
				}			
			
			Conceptually, we're talking
			
			if(CONDITION)
				BLOCK	/ yes
				BLOCK	/ yes
				BLOCK	/ yes
				(else REVERSES/invertes/separates)
				REVERSE BLOCK	/ no
				REVERSE BLOCK	/ no
				REVERSE BLOCK	/ no
			
			With both blocks clearly belonging to the condition above and the empty intent area drawing the eye upward to the CONDITION

		Allman:

			if(x == 3)
			{
				foo();
			}
			elseif ( y == 2) 
			{
				bar();
			}

		Whitesmith:
		
		 - elseif is treated as a statement.
		
			if(x == 3)
				{
				foo();
				}
			elseif ( y == 2)		// I mean, this isn't insanely terrible.
				{	 
				bar();
				}
			else					// Likewise
				{
				barbar();
				}



*/




class transform_stack_t
	{
	SList!(transform_t) data;
	
	void push(transform_t t)
		{
		data.insertFront(t);
		}
		
	void push(double x, double y, double z, double r)
		{
		auto t = new transform_t(x, y, z, r);
		data.insertFront(t);
		}
		 
		//rotation could be float, or even 8/16-bit fixed. How many roations do we really need for a 2-D game?
		// CAVEAT: LARGE STRUCTURES have huge radius, which means a tiny angular rotation ends up as a huge linear movement

	void pop()
		{
		data.removeFront();
		}
	}
	
class transform_t 
	{
	double x, y, z, r;
	
	this()
		{
		x = 0.0;
		y = 0.0;
		z = 0.0;
		r = 0.0;
		}
	
	this(double x, double y, double z, double r)
		{
		this.x = x;
		this.y = y;
		this.z = z;
		this.r = r;
		}
	}		


class matrix_t (int rows, int columns)
	{
	double [rows][columns]	data;
	// ?
	} 

class world_t
	{
	string name;
	map_t [] maps;
	}


//can we make all four cases, still use STATIC SIZED arrays for speed?
// ala map_t(size==SHUTTLE), 

enum MAP_SIZE
	{
	SHUTTLE = 64,
	SHIP = 256,
	STATION = 1024,
	PLANET = 2048 //note, no two values can be the same if we do it this way.
	} //we could simply reference an array if we wanted to have duplicates.


enum MAP_TYPE
	{
	SHUTTLE = 0,
	SHIP,
	STATION ,
	PLANET 
	}
	
int [] MAP_SIZE2 = [64, 256, 1024, 2048 ];

// We're talking MAX sizes here. So even though it's easily possible to have a 
// long, narrow, ship. We don't need the MAX values to be anything but square.
// The point being, we don't need a difference between width and height.
//
// width == height
//
// Surely we can change this at a later time, if necessary. Highly skewed aspect
// maps, using fixed allocation, will waste the most memory.
//
//
// <---------------------------!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//
// --> HEY, QUESTION. Since we're STATICALLY allocating the map sizes using
//	generics... couldn't we just allocate the EXACT MAP SIZE required??? 


template map_size(MAP_SIZE x) //mixin
	{
	static if(x == MAP_SIZE.SHUTTLE)
		{
		const char [] map_size = "layer_t!(64, 64) [] layers;";
		}
	static if(x == MAP_SIZE.SHIP)
		{
		const char [] map_size = "layer_t!(256, 256) [] layers;";
		}
	static if(x == MAP_SIZE.STATION)
		{
		const char [] map_size = "layer_t!(1024, 1024) [] layers;";
		}
	static if(x == MAP_SIZE.PLANET)
		{
		const char [] map_size = "layer_t!(2048, 2048) [] layers;";
		}
	}

template sizer(MAP_SIZE s)
	{
	const char [] sizer = "1024";	 //NOTE, this is only working with 1D VARIABLE not 2D
	// so 1024,1024 DOES NOT WORK. See dlang forum post.
	// https://forum.dlang.org/post/kupestlhnosxqhfefolo@forum.dlang.org
	}

string mix()
	{
	return " \"64,32\" ";
	}
	
class map2_t (MAP_SIZE s) //TEST
	{
	mixin(map_size!(s));		//not QUITE elegant yet...
	
	//verses
	//layer_t!(X,Y) [] layers // where X and Y = some function of enum MAP_SIZE s
	layer_t!(( mixin( sizer!(s) ))) [] layers2;  // OMG, THIS WORKS?!?!?! Kind of ugly AS-IS but we can improve it.
	layer_t!(64,64) [] layers3;
	}

template sizer2D()
	{
	const char [] sizer2D = "64,64";	 // CAN'T USE COMMAS in mixins
	}	
template sizer1D()
	{
	const char [] sizer1D = "64";	
	}	
	
template sizerB(MAP_SIZE s) //works
	{
	const char [] sizerB = "ushort [" ~ MAP_SIZE[s] ~ "][" ~ MAP_SIZE[s] ~ "] tile_id;";	 //you can use enums like associated arrays???
	}	

// won't let me us (T)
// HERE
template sizerC(string type, MAP_SIZE s, string name) //WHY can't type be of type T ala GENERIC???
	{
	const char [] sizerC = type ~ " [" ~ MAP_SIZE[s] ~ "][" ~ MAP_SIZE[s] ~ "] " ~ name ~ ";";
	}	

template sizerD(MAP_SIZE s)
	{
	const char [] sizerD = " int [64,64] x;";
	}	


class map_t
	{
	string name;
	bool is_planet;
	bool is_station;
	bool is_ship;
	bool is_shuttle;
	
	matrix_t!(4,1) global_transformation; //position (vec3) and rotation (vec1). We don't support full 3-D rotation. If we go isometric... do we?
	//or, should global_transformation just be a struct of four floats/doubles?

	void draw()
		{
		}

	layer_t!(1024,1024) [] layers; //TEMPLATES RULE
	
	// Should layers be of ARBITRARY SIZE, but with a fixed multiplier in size?
	// 64x64 minimum (for shuttle) and any combination thereof?
	// That way, we can still work on 64 tiles at a time using the same code regardless of total size. 
	}
	
	
	
//use one size fits all LAYER? 
// or a COUPLE DIFFERENT?
// or sub-typing polymorphism using superclass/subclasses? ala an interface


/* ALSO, are we going to support EDGE WALLS? What kind of DATA STRUCTURE will fit that?

	[][][]
	[][][]
	[][][]
	
	vs
	
	- - -
	 - -
	- - -
	 - -
    - - -

	ALSO, what do we do about those edges for BORDER cells at the edge of the map? They don't have all their points.
*/

/*
	LIST OF LAYERS

					VALUE
	floor			damage
	wall			damage
	background		ANIMATION step?
	
	temperature
	
	
	
	HOW DO WE TRACK ANIMATIONS?
		- If we force ALL of a single tile to animate together, we can track in the tile_id structure
		- If we can SEED animations at different positions, we'll need to track it in the MAP itself.
			- Which is kind of strange because we'll be INCREMENTING HUGE AMOUNTS OF DATA. 1000x1000 increments every frame?!

		- HYBRID. Animate together (if at all), and specific ones will be handled using a different data structure.
			(LIKE AN OBJECT?! It already will work fine for APCs/screens/etc.)
		
*/





// HEYO, we want to be certain <------------
// that RAPID ACCESS DATA is packed NEXT TO EACH OTHER with NO REDIRECTION.
//
// 64 bytes. (SHRINK DATASIZE IF IT'S HELPFUL)
// HEY, if we PACK DATA across BYTE BOUNDARIES (say like 4 values, 4-bits each, across two bytes.) 
// Memory is SUPER SLOW, so will it be FASTER if we UNPACK THE DATA after it hits the CPU?

// tiles_id
//		8-bit - 256 - SUPER CLOSE to too few.
//		16-bit - 65536 WAY TOO MANY, eh?


// 64-bytes
//	2 * 2 * 2 * 2 * 2 * 2 * 2 * 2
//
// 		64 / 2 elements = 32
// 		64 / 4 = 16
// 		64 / 8 = 8 						double
// 		64 / 16 = 4 					ulong / float
// 		64 / 32 = 2 bytes				ushort
//		64 / 64 = 1 bytes				ubyte
//
// 512 bits
//
// 512 - 1, 2, 4, 8, 16, 32, 64, 128, 256, 512
//
// 		512 /  2 elements = 256 bits
// 		512 /  4 elements = 128 bits		
// 		512 /  8 elements = 64 bits		double / long
// 		512 / 16 elements = 32 bits		int / float  (float = only two per cache line, UGH.)
// 		512 / 32 elements = 16 bits		ushort	
// 		512 / 64 elements = 8 bits		ubyte
// 		512 / 128 elements = 4 bits		nibble
// 		512 / 256 elements = 2 bits		
// 		512 / 512 elements = 1 bit
//
// Into single cache line, we can fit 64 tiles.


// We can pack a lot of bools into a cache line too.





struct primal_object
	{
	string serialize(){return "";} //TODO
	
	uint pointer_to_ID(primal_object *obj)
		{
		return -1; //TODO
		}
		
	primal_object *ID_to_pointer(uint ID)
		{
		return &this; // Can't have pointers to CLASSEs in dlang (but structs work)  ... interesting.
		// can use REFERENCES however

//http://forum.dlang.org/post/mailman.417.1342394084.31962.digitalmars-d-learn@puremagic.com
 		}
	}




// FEA
// ===========================================================================

// See great PPT here:
// http://parallelcomp.github.io/FiniteDifference.ppt (NEXT LINK IS PDF form I think... more or less, MORE DETAILS but not as far.)
 
// also https://fenix.tecnico.ulisboa.pt/downloadFile/3779576462709/cpd-21.pdf
// NEXT LECTURE?


// and http://www.uio.no/studier/emner/matnat/ifi/INF3380/v11/undervisningsmateriale/inf3380-week11.pdf

// also http://www.utdallas.edu/~sminkoff/save_papers/39096.pdf

// Recommend using GHOST POINTS. Replicated tiles so that each point, which
// depends on another point, can work with completely independant access.

	// - Ghost points: memory locations used to store redundant copies of data held by neighboring processes
	// - Allocating ghost points as extra columns simplifies parallel algorithm by allowing same loop to update all cells

// This may BE APPROPRIATE for a gigantic NETWORK task across multiple computers
// than a single computer /w multiple CPU's solution. Keep that in mind.
// 2-D HEAT example lists 4 PROCESSORS.



// STRATEGY. Write down SPECIFIC VERSIONS and then NOTE if there are GENERIC simularities
// that can be merged together.


// NOTE NOTE NOTE: WE CAN RUN MULTIPLE SOLVERS for different maps. So this specific
// situation is  "Embarassingly Parallel."

// The next question is, "Can we parallelize large, single map scenarios, like space stations."

// One more HYBRID scenario. Is there ever a case where we can use heuristics to CUT a full map
// into a embarassingly parallel scenario? Imagine Two super complex buildings, connected
// by a single corridor/bridge. There is NO CHANCE for anything away from the "Bridge" affecting
// anything on the other side of the bridge.


class fea_solver_t
	{
	void solve_tick(){}

	/* 
		- We could INTERNALLY split jobs spatially (see *2) into various thread work jobs?
		- Can we build some kind of COMMON INTERFACE so that you can try various solvers (even swapping at run-time)?
	 
	
	 Options:
	 
	 SPLIT jobs across maps (but only BENEFITS when you have more than 1 map!)
		- Maps are ALWAYS unrelated EXCEPT when colliding. (Revert to single-threaded there?)
		- Only data transfers occur at clear bondaries. 
			- Objects "teleport" to new maps AFTER scripts run. 
			- Text messages over telespace can be offset. ("Scripts: Never rely on message speed/syncronization order.")
			- Scripting signalling achieved through setting flags, and the receiver clearing the flag?
		- NO FEA BONDARY ISSUES.
	
	 SPLIT maps into sub-sections (*2)
		- Simple method, A:
	
		- Split every X distance. (Square / isometric grid size.)
			- Simple splitter.
			- What happens at boundaries?!
				- Do jobs/cores need to be SYNCED? 
				- Do we have to wait for all jobs to complete?
				- DO WE HAVE TO SHARE DATA between cores? (Ghost Points).
				- (Simpler, slower alternatie, RE-CALCULATE ghost points instead of sharing.)

		- Split AT TUNNELS/BRIDGES
			- The idea is you have large "rooms" and small cooridores connecting them. 
				- As few as two "rooms" being two huge space station sections, connected by a single, thin, bridge/walkway/tunnel.)
				- At the TUNNEL, you have the LEAST chance for 1) [Room A] affecting [Room B] on the other side.
				- 2) 
			- Requires actually recognizing tunnels/bridges! (Unless manually selected by MAP EDITORS.)
				- POTENTIALLY HUGE (AND SLOW) PROBLEM!!!
					- Hyperplane splicing. Try to find an infinite line (pos+rotation) we can intersect that cuts the
						MOST of two centroids with least connection "girth"/width.
					- Some sort of other heuristics?
					- Manual via map editor and admin intervention.
			- What happens when the map changes?
				- If the mechanism is a SLOW to run on "map load" we can pre-cache that easily.
				- But, if we need to UPDATE that FEA map, we may have a problem.
				- ESPECIALLY if the map is not just changing but RAPIDLY changing, requiring a re-building the net each time.
				- That is, station VS station_with_big_ass_hole = 1 RECALC update (not to be confused with FEA updates.)
				- BUT, station VS station_being_eating_more_each_frame = 100% RECALCs per frame update. SLOW AS BALLS.
	*/
	
		
	
	
	
	
	// How do we resolve pressure, temperature, etc? Do they HAVE to be at the same time?
	// Are they directly related? 
	
	
	// What methods do we really need?
	// We need to be able to READ values and WRITE values.
	
	// Are we reading the data STRAIGHT from a map layer? No? So we're only
	// hitting specific LAYERS inside the map.
	


	void attach_to_map(int map_id){}
	
	// MAP METHODS (Solver uses THESE (inline) methods to talk to map_type, layer_type, etc instead of directly)
	// ------------------------------------------------------------------------------------
	float get_pressure(int x, int y){return 0;}
	float get_temperature(int x, int y){return 0;}
	float get_volume(int x, int y){return 0;} // Volume is always CONSTANT for a map tile, right?
	float get_heat_resistance(int x, int y){return 0;}
	// HOWEVER, what if we're talking about AN OBJECT? THAT could have a different volume.
	
	// OBJECT METHODS
	// ------------------------------------------------------------------------------------
	float get_object_pressure(int object_id){return 0;}
	float get_object_temperature(int object_id){return 0;}
	float get_object_volume(int object_id){return 0;}  // NOT CONSTANT
	float get_object_heat_resistance(int object_id){return 0;}

	//	 what about objects that "leak"? Leak rate? Leak volume?
	//   What about objects that GENERATE? temperature, pressure, etc? (see pumps below)
	
	// What if OBJECTS RUN, make their tiny changes locally, and then FEA runs.
	// That way, SCRIPTS RUN too, finish up, and then we run FEA calculations.
	
	
	// PIPE/PUMP METHODS - Seperate from objects, directly linked to a PIPING SYSTEM
	// ------------------------------------------------------------------------------------
	
		// HOW DO WE REPRESENT THIS IN DATA???
	
	

	fea_element_t [][] data;
	
	layer_t!(1000,1000) *x; //does this make sense? How do we have pointers to template structures which can statically change?
	layer_t!(1000,1000) *y;
	layer_t!(1000,1000) *z;
	}


/*

position types


LOCAL/RELATIVE COORDINATES
*ASSUMES CURRENT MAP_ID (potential BUG if we ever mistake which map we're talking about.)

	struct pos2_t	(int x, int y) 
	struct pos2f_t	(float x, float y);

ABSOLUTE COORDINATES - specifies map with coordinate

	struct pos3_t	(int x , int y , int map_id)
	struct pos3f_t	(float x, float y, int map_id) //note last is still int...
	struct pos3d_t	(double x, double y, int map_id)  // do we need float vs double? What about conversion speed?

bool is_same_map(pos3_t A, pos3_t B){if A.m == B.m)return true; else return false;}


*/


const int NUMBER_OF_GASES = 5;	//needed? use compile-time reflection?
const int NUMBER_OF_LIQUIDS = 5;

enum gas_types //DO WE SUPPORT chemical combinations? CO + O = CO2??
	{
	O2 = 0,
	N2,		// nitrous "puts people to sleep" and laughing gas
	CO2,    // puts people to sleep.
	AR, 	// Argon. Anti-flame / flame-retardant?
	plasma, // Flammable gas. (Do we want plasma to be the only flammable gas?)
	} //Do we want a special case for AIR which is most common?
	
enum liquid_types //both in POOL FORM, containers, and cubic form
	{
	water = 0,		// flame-extinguishing, slippery.
	plasma,			// flammable, sticky?
	oil,			// flammable, sticky? / slippery.
	sludge			// maybe?	recycling biproduct. slippery. ?
	}
//enums don't need semicolons!

class gas_t
	{
	string name; //uhh, do we need more data?
	float percent;  //do we want percent... or invidiaul partial presssures?
	}

class fea_pipe_t //pipe, canister, or pump. (etc) linked to the piping system.
	{
	float temperature;
	float pressure;
	float volume; //note for each PIPING element
	
	
	//USE PER SECOND because TICK_RATE could change???
	float heat_generated_per_second; 
	float temperature_generated_per_second; 
	float pressure_generated_per_second;
	float volume_generated_per_second;
	//float moles_generated_per_second;
		// How do we support some or all of these? PV=mRT
	
	// HOW TO HANDLE LINKAGE INTO MAP
	int x, y, map_id; //could htis be a tuple? XYZ or int X,Y,M? (note not FLOATs for exact positions)
	
	// LINKAGE INTO PIPE (by reference type with auto garbage collection?)
	fea_pipe_t pipe1; 
	fea_pipe_t pipe2; 
	fea_pipe_t [] pipes; 
//	int number_of_connections;
	// would using a (dynamic?) array here be really slow?
	// ---> MOST PIPES will only be connected to TWO pieces. (next pipe and previous)
	
	// GAS COMPOSITION (percent for partial pressures. Or just partial pressures?)
//	gas_t [] gases;	
// Do we really want a dynamic array, or just an array for ALL TYPES?

	gas_t [NUMBER_OF_GASES] gases;

	}



//what if? Each FEA solver could have it's own setup.
// What if we buffered certain arrays together?
// e.g.  if we WRITE to a tile, we write it here, in packed form, AND, in the normal map tile.
//	HOWEVER, if FEA changes something, we'll need to place the values back into the map.
struct fea_element_t
	{
	float temperature; 	//only two floats per cache line. 	NOTE, CANNOT have NEGATIVE temperature. That's ONE BIT we can save. 
	float pressure;
	bool is_solid;		// whole byte? That won't be nice unless we throw them into an array like the other class.
	//WOAH, is_solid means IS_PASSABLE. Careful. We could have a GAS that turns into a LIQUID or SOLID.
	
	// How do we deal with that scenario.. and... DO WE DEAL with it? It's a fringe case but MORE complexity!
	}
	
// Could we use some sort of CUSTOM DATA TYPES designed specifically for the ranges they'll be used in?
// -> This will work for the NETWORK LAYER, however, for FEA... we're also using the same CPU (are we?) for PACKING. So can this even help?



// Don't forget the possibilty of custom FIXED point, that we can probably keep in fixed point throughout.

// SEE BITPACKING
// http://lemire.me/blog/2012/03/06/how-fast-is-bit-packing/
	// https://github.com/lemire/simdcomp
// https://github.com/powturbo/TurboPFor
/*
Conclusion: Bit packing and unpacking can be quite fast. In particular, it can be cheaper to unpack integers from a small number of bits
 to 32-bit integers than to copy the same 32-bit integers. Exact results will vary depending on your compiler and CPU.
*/



// <---------- IMPORTANT. Don't get so bogged down in implementation details! The damn thing has to RUN before we can bother PROFILING it.



class layer_alt_t (MAP_SIZE size)
	{
	mixin(sizerB!(size));
	mixin(sizerD!(size));
	//byte	mixin(sizerB!(s))	damage;
	//float	mixin(sizerB!(s))	temperature;
	
	// Maybe this? Slightly ugly... rename SizerC to BuildLayer! 
	// or SizeLayer!...?
	// -> Creates ARRAY of [size] looked up by enum MAP_SIZE, variable named tile_id, temperature, damage, etc.
	mixin(sizerC!(ushort, size, tile_id));		
	mixin(sizerC!(float	, size, temperature));	
	mixin(sizerC!(byte	, size, damage));		

	}


class layer_t (int width, int height = width) //HOT DAMN TEMPLATES RULE (see below)
	{
		
	// NOTE the ordering. We want a full layer (struct of arrays), not an array of structs. Unless the access is going need ALL those values.		
	ushort 	[width * height] 	tile_id; //16-bit, 0-65535, lookup value (see lower notes)	
	byte 	[width * height] 	damage; 			// floor/tile uses this for DAMAGE tracking.	(rename to value1?)
		// if each TILE can have a health of 0-100 (percent) = (101 unique values), that would be 101/256 = 0.39453125 minimum change (or ~0.5 points of damage minimum)
		// For ACTORS we may want more fine-grained damage since actors are HUGE structures compared to a map tile their larger data size won't matter.
		// ushort [] damage; // actors
	
	float 	[width * height] 	temperature;		// floor/tile uses this for TEMPERATURE. Otherwise, not sure. (rename to value2?)
		// Damage	   - normal floor/wall layers
		// Temperature - heat layer (separate layer? then you'd need two for wall & floors, so no...)
		// Others??
	
	// tile_id
	// ------------------------------------------------------------------------
	// 0 is SPECIAL VALUE that never looks up to a ID. 
	// NOTE for simplicity sake, 0 will exist in the array but never be used.
	// OTHERWISE, we'd no longer follow the OFF-BY-ONE / starting-at-zero array indexing and it'd be confusing as hell to remember.
	
	// that is, 0 = NOTHING, 1 = array[0], 2 = array[1], ...	(0 = array[-1] which would CRASH if anything accidentally read from it.)
	//
	// this way 0 = NOTHING, 1 = array[1], 2 = array[2]			
	//		and we just never use array[0]						(0 = array[0] which would NOT CRASH during an accidental read.)
	
	}



//should these be subclasses? We don't want run-time type ID. Some MAY benefit from extra data.
//do we want long class names like this? especially with LONG_WORDS_BETWEEN_UNDERSCORES_TI vs A_TIM_BO_TI

// Do we NEED these to be separate? floor/tile seem pretty related.
class floor_tile_id_t		
	{
	// IF WE BITPACK, it may make sense to LEAVE ROOM for expansion lest we have to refactor everything.
	// That, or do some sort of magical template/mixin that automatically adjusts everything.	
	
		
	// BLOCKING
	// ----------------------------
	// object types
	bool blocks_players;
	bool blocks_objects;		// different than players?
	bool blocks_particles;		// fx
	
	bool blocks_vision;
	bool blocks_thermal_vision;
	bool blocks_sound;
	
	
	//blocks_ghosts?
	//blocks_flying?
	
	// weaponry / chemical types
	bool blocks_lasers;
	bool blocks_bullets;		// kinetic weaponry like bullets
	bool blocks_radiation;
	bool blocks_gases;
	bool blocks_liquids;
	bool blocks_fire;
	bool blocks_acid; // ???
	
	
	bool leaks_at_damage; // ???
	float leak_damage;    //
	float rust_rate;		// combining with water? (not exactly accurate)
	float oxidize_rate;		// combining with oxygen?
			// Should we explicitly enumerate each, or support a list of 
			// damage/reaction types, and a list of rates, and so on?
	

	// Conduction?
	// ----------------------------
	bool conducts_electricity;

	// MOVEMENT
	// ----------------------------
	bool normally_can_grip;		//better names? all needed?
	bool low_gravity_can_grip;
	bool no_gravity_can_grip;
	bool can_grip_when_being_sucked_out_airlock;
	// difference between leg grip and hand grip?


	// O SHIT BOY
	// CANT DO THIS
	// float health = 100; //percent 
	// gotta put it in the layer itself, not the map_id field
 	
	// Damage Resistances:
	// --------------------
	struct resistances  	// Will this be FAST or need another dereference? (SLOWER.)
		{
		float resist_kinetic;
		float resist_laser;
		float resist_pulse; //pulse weaponry and damage (maybe?)
		float resist_acid; // acid / dissolving?
		float resist_fire; // fire / melting?
		
		float resist_water;
		float resist_hydrocarbons; //gases, oils
		float resist_oxygen;	// OXYGEN dissolves stuff. (reactive, anyone?) <------ Should we implement RUST on metal?!
		
		float resist_blunt; //brute is... blunt?
		float resist_piercing; // sharp / piercing
		float resist_slashing;
		}
		
	
	// Temperature Weakening/Endurance (think brittle steel)
	// -------------------------------------------------------------------
	float low_weakening_temperature; 	// see below
	float high_weakining_temperature; 	//
	float low_temp_damage_factor; 		// below this temperature, MULTIPLY DAMAGE by this factor
	float high_temp_damage_factor; 		// above this tempearture MULTIPLY DAMAGE BY this factor
	
		bool  fx_show_icicles; // SPECIAL EFFECT of ice forming around when below minimum temp.
		float icile_factor; // y=mx+b,  m
		float icile_offset; // y=mx+b,  b
		color_t ice_color;

		bool fx_show_melting; // Think metal glowing red/white before melting away.  
		float melt_factor; // y=mx+b,  m
		float melt_offset; // y=mx+b,  b
		color_t melt_color;
		
	float minimum_temperature; // triggers "freezing" which converts to new tile.
	float maximum_temperature; // triggers "melting"/"sublimation" converting to a new tile OR OBJECT OR FLUID/GAS? (HOW)
	bool sublimates = false;  
	bool boils = false;
	bool freezes = false;
	}

class wall_tile_id_t : floor_tile_id_t //not sure
	{
	}

//  https://dlang.org/spec/interface.html
interface x  
	{
	void foo();  //woah, I didn't realize Dlang explicity has interfaces. NEAT. Now to learn it...
	}

class bg_tile_id_t	//bg or background? bg is pretty damn obvious for background
	{
	bitmap_t x; 
	}

//=============================================================================
int main(char[][] args)
{
	int x;
	x = 5;
	if(x == 5) x++;
	
	
	//test3_version();

//	test_packed();
	//testme(x, 10);
	
//	test_functionasdg();
	
//	test_third();
//	test_template();

//	return 0;
	
static if (true)
	{
	
	return al_run_allegro(
	{
		if (!al_init())
		{
			auto ver = al_get_allegro_version();
			auto major = ver >> 24;
			auto minor = (ver >> 16) & 255;
			auto revision = (ver >> 8) & 255;
			auto release = ver & 255;

			writefln("The system Allegro version (%s.%s.%s.%s) does not match the version of this binding (%s.%s.%s.%s)",
				major, minor, revision, release,
				ALLEGRO_VERSION, ALLEGRO_SUB_VERSION, ALLEGRO_WIP_VERSION, ALLEGRO_RELEASE_NUMBER);
		
			return 1;
		}
		
		ALLEGRO_CONFIG* cfg = al_load_config_file("test.ini");
		//version(Tango)
		//{
			//char[] arr = fromStringz(al_get_config_value(cfg, "", "name"));
			//Stdout.formatln("Entry is: {}", arr);
		//}

		ALLEGRO_DISPLAY* display = al_create_display(500, 500);
		ALLEGRO_EVENT_QUEUE* queue = al_create_event_queue();

		if (!al_install_keyboard())      assert(0, "al_install_keyboard failed!");
		if (!al_install_mouse())         assert(0, "al_install_mouse failed!");
		if (!al_init_image_addon())      assert(0, "al_init_image_addon failed!");
		if (!al_init_font_addon())       assert(0, "al_init_font_addon failed!");
		if (!al_init_ttf_addon())        assert(0, "al_init_ttf_addon failed!");
		if (!al_init_primitives_addon()) assert(0, "al_init_primitives_addon failed!");

		al_register_event_source(queue, al_get_display_event_source(display));
		al_register_event_source(queue, al_get_keyboard_event_source());
		al_register_event_source(queue, al_get_mouse_event_source());

		ALLEGRO_BITMAP* bmp = al_load_bitmap("./data/mysha.pcx");
		ALLEGRO_FONT* font = al_load_font("./data/DejaVuSans.ttf", 18, 0);

		with(ALLEGRO_BLEND_MODE)
		{
			al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
		}

		auto color1 = al_color_hsl(0, 0, 0);
		auto color2 = al_map_rgba_f(0.5, 0.25, 0.125, 1);
		writefln("%s, %s, %s, %s", color1.r, color1.g, color2.b, color2.a);
		
		bool exit = false;
		while(!exit)
		{
			ALLEGRO_EVENT event;
			while(al_get_next_event(queue, &event))
			{
				switch(event.type)
				{
					case ALLEGRO_EVENT_DISPLAY_CLOSE:
					{
						exit = true;
						break;
					}
					case ALLEGRO_EVENT_KEY_DOWN:
					{
						switch(event.keyboard.keycode)
						{
							case ALLEGRO_KEY_ESCAPE:
							{
								exit = true;
								break;
							}
							default:
						}
						break;
					}
					case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
					{
						exit = true;
						break;
					}
					default:
				}
			}

			al_clear_to_color(ALLEGRO_COLOR(0.5, 0.25, 0.125, 1));
			al_draw_bitmap(bmp, 50, 50, 0);
			al_draw_triangle(20, 20, 300, 30, 200, 200, ALLEGRO_COLOR(1, 1, 1, 1), 4);
			al_draw_text(font, ALLEGRO_COLOR(1, 1, 1, 1), 70, 40, ALLEGRO_ALIGN_CENTRE, "Hello!");
			al_flip_display();
		}

		return 0;
	});


	}
}

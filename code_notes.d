

// NEVER. USE. STD. REGEX. Unless you don't mind over 400MB of RAM and ~5 seconds of compile time (possibly PER file invokation)

	Command being timed: "./go_extra_dmd"
	User time (seconds): 5.72
	System time (seconds): 0.56
	Percent of CPU this job got: 96%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:06.52
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 613212


//after commenting out ini_parser which is only module loading and using std.regex.

	Command being timed: "./go_extra_dmd"
	User time (seconds): 2.48
	System time (seconds): 0.22
	Percent of CPU this job got: 106%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 0:02.54
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 222968






// WTF. std.json;
// jsonvalue:

jsonvalue val = /*...*/;
layers[0].data[i][j] = to!int(val.integer); //"integer" outs a long. lulbbq. Because in JS it's string or integer or bool, where integer != INT. Thank god they didn't use .int extension.







// THIS CODE ISN'T MEANT TO RUN or be COMPILED but actually be MARKUP HIGHLIGHTED notes
// we could also do this in MarkDown, but code in here will look exactly like it will IF COPIED into real  code.



Why the hell am I using a D file for RANDOM NOTES?!?!? Do I really want to COMMENT BLOCK every bloody paragraph, and MANUALLY WORD WRAP IT?





// Memory Allocation
// ================================================================================================

// Concurrency
// ------------------------------------------------------------------------------------------------

 - We can support concurrency by SPLITTING job domains, and having those separate simulations on their own CPU core.
 - Domains are separated by a BOUNDARY.
 - Job domains are not completely separate. They have BRIDGES where certain data can travel to (or "EFFECT") another
	domain across the boundary. 
 - Some bridged data can become TEMPORALLY asyncronous or "stetched" within a range, beyond which a (MEMORY) BARRIER
	CONDITION occurs demanding all parallel work stop and syncronize the domains.

	The easiest way to split [THE WORLD] (set of all data) into job domains is physically:
	------------------------------------------------------------------------------------------------------
	
	- Thanks to the space nature of the game. Two separate Space Stations (or ships) can be run
	as separate simulations as long as there are no BRIDGES between them.
	
	- When there ARE bridges, if the data does not have to be immediately syncronized 
		for simulation to continue, then there is no need for HALT all operations and syncronize
		(a memory barrier). Bridges can be: 
	
		- PDA and other communication messages. These CAN be delayed X frames. A fairly long time. (seconds)
			- This data is [LOW PRIORITY] but also [SMALL] in size.
	
		- Objects being transfered across the boundary. (From one ship to another, like a 
			teleportation or from SHIP to SPACE.)
			- This is still a rare / low-density occurance except for specific scenarios such as 
			boarding parties between two crashed-together ships.
			- However, it should still be FAST as possible.


	---> It might be good to keep objects IN SPACE that are NEARBY to a station, to still be owned by that station. We could keep them
		in another list with floating-point coordinates. Otherwise, every object that jumps into space (and back, over and over)
		will have to SYNC TO ANOTHER BOUNDARY.
	
	Another option (or, in addition to the above): SPLIT-INSIDE-MAPS
	------------------------------------------------------------------------------------------------------
	
	- Advantage: Finer-grained parallelism even when there is only ONE map/ship/station.
	- Disadvantage: MUCH denser syncronization across BOUNDARIES.
	
	Basically, we SPLIT the map using some kind of [splitting algorithm]. The algorithm will try strategies
	to minimize the chance of objects that crossing boundaries to reduce barrier syncronization hits.
		
		(NOTE: WE CAN RECORD STATISTICS ON BOUNDARY SYNCS PER SECOND / MEMORY HALTS.)
		
		NOTE: Do not forget NON-SPATIAL bridges. A computer that accesses a MACHINE somewhere further away requires
				syncronization!
		
	Algorithm Candidates
		- SPLIT-INTO-SQUARES
			- Split the map into equally sized squares.
			- PRO: One of the simplest possible method. 
			- PRO: Does NOT have to re-run every time the map is modified. (People adding/removing station pieces.)
			- CON: Boundaries are NOT selected based on object density (one core may have nothing, one may have 99% of objects)
			- CON: Boundaries are NOT selected in a way that reduces the chance of collisions / boundary crosses.
				- Objects could be fighting/building/moving constantly on opposite sides of two boundary lines 
				causing constant memory contention as objects physically move around in memory.
			- CON: NON-SPATIAL bridges. (per note above)
		- SPLIT-WITH-BSP
			- Use a BSP tree with the object density as the critera function. The more objects in an area,
				the smaller the area. 
			- PRO: Cores are given an approximately equal object load based on [OBJECT DENSITY]. 
				- Or even (OBJECT x COMMANDS_TYPICALLY_SENT_PER_SECOND) function normalization. That is, 
				objects that send more data count as a larger "object" than objects that interact with
				very few things. So cores are given approximately equal [MESSAGE DENSITY]
			- PRO: ??
			- CON: Boundary crossing may not be minimized depending on who sends data to what.
			
			
			
		---> Is it possible to SPLIT based on RELATIONSHIPS? So that we ensure the most objects that know about
			other objects (and hence, communicate with them), are on a single core? 
			- (Spatal locality included as a factor since objects will bump into each other.)
			- Is this a VERY HARD algorithm to implement? Or just needs some analysis/dev-time to understand?
			- This algorithm will have to CONSTANTLY UPDATE AND SHUFFLE objects around as relationships change
			to reduce MEMORY CONTENTION.
			- Any objects that are CAUSING CONTENTION are LOGGED and prime candidates for moving.
				- Works for 1-to-1 relationships, but what if its A->B->C. We move A to the B domain. But then C
					wants it, and we move it back (or C to that domain.) But then everything eventually gets moved
					to a single domain. Is this some insanely hard NP algorithm?
				- Requires that MEMORY CONTENTION use ATOMIC LOCKS for accessing other core's data so that we don't
				HALT on EVERY single interaction. The goal function would be reducing the RATE of locks spinning. 
				(right term?) That is, one core holding a lock, and another core WAITING to use that memory location.
					- ALSO, WARNING. LOCKS ARE EXPENSIVE. We do not want fine grained locks whenever possible.
					- HOWEVER, if we ONLY USE LOCKS when we are CROSSING BOUNDARIES, the number of locks is still
						low (except during a theoretical explosion of boundary crossing rate during rare in-game events.)
					- Much less bad to [sync one object] than to [HALT AND SYNC] all cores. Of course, was that ever
					a problem? The issue is a little hazy, but it should be an issue of MICRO LOCKING vs MACRO LOCKING
					of data structures. Freezing one object, verses freezing all objects.
					
					
					
					
					
	---> BIG NOTE: Have we made any consideration of DEAD LOCKS? If we are just telling the other core to 
	[ADD THIS MESSAGE TO YOUR QUEUE FROM MY SIDE] that should be just fine, right? The only SYNC LOCKS are when
	messages ... bump into... there should not be ANY syncing in that case, right? As long as ALL CORES get to finish
	handling their [MESSAGE QUEUE] before the frame begins.
	
	HOWEVER, when an OBJECT MOVES to the otherside, we will need to figure that out. HOWEVER, thanks to the TWO PHASE
	setup, objects will only move AFTER all scripts have run.
	
	
	
	"When lock hold times are always extremely short and locks are fi ne-grained, a spin lock can offer
better performance than other locking mechanisms." (p190)

from Professional Parallel Programming with C# Master Parallel Extensions with .NET 4.pdf
	
// other
// ------------------------------------------------------------------------------------------------
/*
 I will call this distance the critical stride. Variables whose distance in memory is a multiple of the
critical stride will contend for the same cache lines. The critical stride can be calculated as
(critical stride) = (number of sets) x (line size) = (total cache size) / (number of ways).

(p89)



(p97) <------
  The reason why this effect is so much stronger for level-2 cache contentions than for level-1 cache
  contentions is that the [level-2 cache cannot prefetch more than one line at a time.]

(p98) "This will happen if the size of a matrix line (in bytes) is a high power of 2."

			 KB		time-per-element
	511x511 2040 	38.7
	512x512 2048 	230.7 <----
	513x513 2056 	38.1

A simple way of solving the problem is to make the rows in the matrix longer than needed in
order to avoid that the critical stride is a multiple of the matrix line size. I tried to make the
matrix 512520 and leave the last 8 columns unused. This removed the contentions and the
time consumption was down to 36.


(p101)

An uncached write is more expensive than an uncached read because the write causes an
entire cache line to be read and written back.

The so-called nontemporal write instructions (MOVNT) are designed to solve this problem.
These instructions write directly to memory without loading a cache line. This is
advantageous in cases where we are writing to uncached memory and we do not expect to
read from the same or a nearby address again before the cache line would be evicted. Don't
mix nontemporal writes with normal writes or reads to the same memory area.

The nontemporal write instructions are not suitable for example 9.5 because we are reading
and writing from the same address so a cache line will be loaded anyway. If we modify
example 9.5 so that it writes only, then the effect of nontemporal write instructions becomes
noticeable. The following example transposes a matrix and stores the result in a different
array.

	Matrix size 	Time per element-Example 9.6a	Time per element-Example 9.6b
	64x64 			14.0							80.8
	65x65 			13.6 							80.9
	512x512 		378.7 							168.5
	513x513 		58.7 							168.3
	
As table 9.3 shows, the method of storing data without caching is advantageous if, and only
if, a level-2 cache miss can be expected. The 64x64 matrix size causes misses in the level-
1 cache. This has hardly any effect on the total execution time because the cache miss on a
store operation doesn't delay the subsequent instructions. The 512x512 matrix size causes
misses in the level-2 cache. This has a very dramatic effect on the execution time because
the memory bus is saturated. This can be ameliorated by using nontemporal writes. If the
cache contentions can be prevented in other ways, as explained in chapter 9.10, then the
nontemporal write instructions are not optimal.
*/

/*

	- DDR has a width of 64-bits
	- Caches have a width of 64... byte = 512-bits (so wait, does it do 8 reads every time?)
	--> WOAH. Multiple memory requests can be queued up. (I think!) so it'll feed up to 8
		different waiting cache pages.
		
	--> Since DDR is so much insanely slower than a CPU, maybe that means multiple requests
		can be queued up for exactly this case.


	http://www.ilsistemista.net/index.php/hardware-analysis/3-the-phenom-phenomii-memory-controller-and-the-ganged-vs-unganged-question.html?start=3
	
		Ungained works better in a multi-threaded setup.
		
	http://softwareengineering.stackexchange.com/questions/270192/relation-between-cache-line-and-memory-page
	http://softwareengineering.stackexchange.com/questions/267786/understanding-memory-update-propagation-in-x86-x86-64-cpu-l1-l2-l3-caches-and-ra?rq=1
	
	http://cs.stackexchange.com/questions/21331/cpu-bit-its-cache-line-the-bus-between-memory-and-cpu-and-its-registers?newreg=dd134d1516e74013b74320e930e9768a
		GOOD READ


	http://mrfunk.info/?page_id=181
		Interesting side read.
		"Thoughts and Conjecture on Near Memory"




	MMX/SSE/SSE2 <-------

	AVX (256-bit)  (~AMD FX),  AVX512 (newest intels, "likely Zen/Ryzen"--has two 256-bit FMAC each core)

	with 512 you can run calculations on EIGHT DOUBLES at the same time.
	
	
	
	Good compilers should be able to use SIMD automatically. (what about setting target platform?)
	
		"-msse2, -mavx, etc. for Linux"
		
		
		
	Instruction set Header file
		MMX mmintrin.h
		SSE xmmintrin.h
		SSE2 emmintrin.h
		SSE3 pmmintrin.h
		Suppl. SSE3 tmmintrin.h
		SSE4.1 smmintrin.h
		SSE4.2 nmmintrin.h (MS), smmintrin.h (Gnu)
		AES, PCLMUL wmmintrin.h
		AVX immintrin.h
		AMD SSE4A ammintrin.h
		AMD XOP ammintrin.h (MS), xopintrin.h (Gnu)
		AMD FMA4 fma4intrin.h (Gnu)
		all intrin.h (MS)
		x86intrin.h (Gnu)
		Table 12.2. Header files for intrinsic functions
*/


// Don't forget alloca() which allocates memory ON THE STACK. Super fast. PLUS freed on function end! (RAII?!)
//  And supported in D.
// - However, there is a known bug with exceptions/finalizers being called in D messing up stack size.
// - Only affects certain OS.
//
//http://www.agner.org/optimize/optimizing_cpp.pdf (p93)

// Don't use alloca() with inline functions because for EACH CALL of a LOOP, the stack keeps growing
// instead of being reset each "call" (which would be a loop iteration at that point.) In otherwords,
// because of shit compilers. OTOH, GCC documentation for inline warns about this case.


// Garbage Collection
// ================================================================================================

// http://forum.dlang.org/post/vqqfrchixikyqaclxzea@forum.dlang.org









// Misc
// ================================================================================================

// "if you adhere to practice of always writing a constant in the beginning, "
// if ((res = setsockopt(....) == -1))    NO! The parenthesis are wrong!
// if ((res = setsockopt(....)) == -1)    correct verison.
// HOWEVER, INSTEAD FROM NOW ON, DO THIS: ???
// if( -1 == (res = setsockopt(....)) )
// but it CAN still blow up...


// error patterns (trend in errors) and how to avoid them
// http://www.viva64.com/en/a/0078/


// C++ Optimization [2013] [pdf] (downloaded)
// ================================================================================================
/*
 Thread-local storage should be avoided, if possible,
and replaced by storage on the stack (see above, p. 26). Variables stored on the stack
always belong to the thread in which they are created.
*/

/*
https://dlang.org/migrate-to-shared.html

	Yes, reading or writing TLS variables will be slower than for classic global variables. 
	It'll be about 3 instructions rather than one. But on Linux, at least, TLS will be slightly 
	faster than accessing classic globals using PIC (position independent code) compiler code 
	generation settings. So it's hard to see that this would be an unacceptable problem. But
	 let's presume it is. What can we do about it?

	1.Minimize use of global variables. Reducing the use of globals can improve the modularization and maintainability of the code anyway, so this is a worthy goal.

	2.Make them immutable. Immutable data doesn't have synchronization problems, so the compiler doesn't place it in TLS.

	3.Cache a reference to the global. Making a local cache of it, and then accessing the cached value rather than the original, can speed things up especially if the cached value gets enregistered by the compiler.

	4.Cowboy it with __gshared.



NOTE, immutables are okay!

Cache a reference... sounds insane. It's still another pointer lookup...


__gshared int x; // Enable "FUCK TLS." mode


I *think* the main goal of TLS is that you can use STATIC variables for each thread, not globally...

Maybe?

https://en.wikipedia.org/wiki/Thread-local_storage

	TLS is used in some places where ordinary, single-threaded programs would use global variables 
	but where this would be inappropriate in multithreaded cases. An example of such situations is 
	where functions use a global variable to set an error condition (for example the global variable 
	errno used by many functions of the C library). 


http://arsdnet.net/this-week-in-d/jun-07.html

	Walter took to the mic briefly to mention that TLS globals are not actually very efficient due to 
	these indirect lookups - local variables are far more efficient to access.






http://stackoverflow.com/questions/506093/why-is-thread-local-storage-so-slow
	READ ME
		http://stackoverflow.com/questions/18156711/thread-local-storage-variables-in-d-confusion-about-interaction-with-module-ini
			and me!




https://p0nce.github.io/d-idioms/

LOTS OF D IDIOMS


https://dlang.org/phobos/std_typecons.html#.scoped
	Puts a CLASS on the current stack/scope.

	Otherwise, use STRUCT value types for RAII. (Except "structs can't have default constructor". ...hmmm.)
	
https://dlang.org/spec/statement.html#ScopeGuardStatement

	See also SCOPE GUARDS which runs CODE (written in middle of scope) at END of scope.
	
		However, they're RUN IN REVERSE ORDER. Like a stack! Icky!
*/



// STATIC ANALYSIS and other tools llike debugging
// ================================================================================================

// GOOD PDF
// http://www.agner.org/optimize/optimizing_cpp.pdf
	// "A simple alternative is to run the program in a debugger and press break while the
	// program is running. If there is a hot spot that uses 90% of the CPU time then there
	//  is a 90% chance that the break will occur in this hot spot. Repeating the break a
	// few times may be enough to identify a hot spot. Use the call stack in the debugger 
	// to identify the circumstances around the hot spot."
// Random pausing profiling VIDEO
// 		https://www.youtube.com/watch?v=xPg3sRpdW1U
// SO post about it from same guy:
// 		http://scicomp.stackexchange.com/questions/2173/what-are-some-good-strategies-for-improving-the-serial-performance-of-my-code/2719#2719
// Another random pausing:
//      http://stackoverflow.com/questions/375913/how-can-i-profile-c-code-running-in-linux/378024#378024
// GOOD ONE:
//		http://stackoverflow.com/questions/1777556/alternatives-to-gprof/1779343#1779343

// don't forget AMD CodeXL which allows two types of HARDWARE PROFILING for AMD processors!

// PROFILING TECHNIQUES/etc
// http://yosefk.com/blog/how-profilers-lie-the-cases-of-gprof-and-kcachegrind.html




// GDB vs LLDB debugging commands:
// http://stackoverflow.com/questions/9707883/gdb-vs-lldb-debuggers
// LLDB Advantages?
// http://stackoverflow.com/questions/9376370/what-are-the-advantages-of-lldb-over-gdb-in-ios-development

// Guide to Faster, Less Frustrating Debugging
// http://heather.cs.ucdavis.edu/~matloff/UnixAndC/CLanguage/Debug.html

// helper script to demangle D names for VALGRIND / kcachegrind
// https://wiki.dlang.org/Other_Dev_Tools



//https://en.wikipedia.org/wiki/Dynamic_program_analysis#Example_tools
// Valgrind!  Also others. LLVM has AddressSanitizer


// http://www.viva64.com/en/b/0382/
// Static checking THE STATIC CHECKER.

// https://github.com/dlang/tools

// DUB ? (Need to get it running on mine...)

// http://wiki.dlang.org/
// http://wiki.dlang.org/Development_tools
// http://wiki.dlang.org/Libraries_and_Frameworks
// http://wiki.dlang.org/Open_Source_Projects
// http://wiki.dlang.org/Bindings


// side Note: a Leaf node has no children.


// SAVEGAMES
// ================================================================================================
//	https://www.gamedev.net/resources/_/technical/game-programming/how-to-implement-saveload-functionality-for-games-with-levels-r4505


// When we reload the RECENT save, do we really want to wipe everything and start over? That'll be a fairly long loading time?
// - Is there any way to keep assets stored?
// - Is there a way to only update the changes? (but STILL be assured we're not leaving STATE left over!)








// Concurrency notes:
// ==============================================================================================================

// WOAH, CHECK THIS OUT, tons of parallel patterns (referenced material for the MSDN article)

// http://web.archive.org/web/20090422011759/http://parlab.eecs.berkeley.edu/wiki/patterns/patterns
//
// for example
//		http://web.archive.org/web/20100209034028/http://parlab.eecs.berkeley.edu/wiki/patterns/task_parallelism

// "Concurrency is about dealing with lots of things at once. Parallelism is about doing lots of things at once." - Rob Pike


// https://en.wikipedia.org/wiki/MapReduce
// MAP-REDUCE. Map is the parallel part, and REDUCE is where results are combined together.
// Note, the REDUCE doesn't have to occur just at the end (like in summing), it could combine at other
// discrete stages.

// Wait, this TERM might not be correct, see wiki. Also, this one is for HUGE DATA across networks. 




// SEE PAGE p240 for NEXT READING of C++ concurrency book. HOW TO SPLIT AN ARRAY/MATRIX. Which is what we're doing!
// <-------------------------------------------------------

/*
" It may therefore be better to divide the result matrix into small square or almostsquare
blocks rather than have each thread handle the entirety of a small number of
rows. Of course, you can adjust the size of each block at runtime, depending on the
size of the matrices and the available number of processors. As ever, if performance is
important, it’s vital to profile various options on the target architecture."


p252 - HIDING LATENCY with multiple threads

	Mentions a virus scanner using a PIPELINE. One thread to search for files, one to read them, one to scan the data, etc.

	See p232, mentions video decoding, frame rate, etc.
*/

// [Decomposition] - Cutting a large work into discrete tasks.
// Coordination - How to coordinate these tasks as they run in parallel
// Scalable Sharing - Techniques for sharing the data from these tasks (?)

/*

https://msdn.microsoft.com/en-us/library/ff963542.aspx

 If too fine granularity, overhead of task switching becomes the problem. If too coarse, opportunities for parallelism may be lost.
 
"In general, tasks should be as large as possible, but they should remain independent of each other, 
 and there should be enough tasks to keep the cores busy. "

"The overall goal is to decompose the problem into independent tasks that do not share data, while
 providing sufficient tasks to occupy the number of cores available. When considering the number of
  cores, you should take into account that future generations of hardware will have more cores."

"Keep in mind that tasks are not threads. Tasks and threads take very different approaches to 
 scheduling. Tasks are much more compatible with the concept of potential parallelism than threads
  are. While a new thread immediately introduces additional concurrency to your application, a
   new task introduces only the potential for additional concurrency. A task's potential for
    additional concurrency will be realized only when there are enough available cores."

http://stackoverflow.com/questions/13429129/task-vs-thread-differences
http://stackoverflow.com/questions/4130194/what-is-the-difference-between-task-and-thread
http://www.albahari.com/threading/

	- A task is something you want done.
	- A thread is one of the many possible workers which performs that task.


	- A task is future/promise.
	- A thread is a way of fulfilling the promise.


--->
"Constraints can arise from control flow (the steps of the algorithm) or data flow (the availability of inputs and outputs)."

--->
 "Adding synchronization reduces the parallelism of your application."  
 "Every form of synchronization is a form of serialization.
 
 "Your tasks can end up contending over the locks instead of doing the work you want them to do. Programming with locks is also 
 error-prone."

Fortunately, there are a number of techniques that allow data to be shared that don't degrade performance or make your program
 prone to error. These techniques include the use of [immutable], [read-only data], limiting your program's reliance on shared 
 variables, and introducing new steps in your algorithm that [merge local versions of mutable state at appropriate checkpoints].
  Techniques for scalable sharing may involve changes to an existing algorithm.

*/



// Parallel LOOP, multiple threads ONE task. [Dara Parallelism]
// 	- All units of data have SAME operation applied to them. No calculates DEPEND on other results.
//
// Parallel TASKS, multiple threads for multiple different operations (tasks). [Task parallelism]
//	- Tasks tend to have a work queue and don't necessarily execute instantly.
//
// Parallel Aggregation  
//	- Parallel loop with MERGE AT END.
/*
However, not all parallel loops have loop bodies that execute independently. For example, a loop that calculates
 a sum does not have independent steps. All the steps accumulate their results in a single variable that represents
  the sum calculated up to that point. This accumulated value is an aggregation.

Nonetheless, there is a way for an aggregation operation to use a parallel loop. This is the [Parallel Aggregation pattern].

[...]

The Parallel Aggregation pattern uses unshared, local variables that are merged at the end of the computation to give the final result.

[...]

 Parallel aggregation demonstrates the principle that it's usually better to make changes to your algorithm than to 
 add synchronization primitives to an existing algorithm. 
 */



//pipeline pattern

/*
Pipelines

The Pipeline pattern uses [parallel tasks] and [concurrent queues] to process a sequence of input values. Each task implements 
a stage of the pipeline, and the queues act as [buffers] that allow the stages of the pipeline to execute concurrently, even 
though the values are processed in order.

[...]

---> Pipelines allow you to use parallelism in cases where there are too many dependencies to use a parallel loop.

*/

// SEE THESE --> https://www.cise.ufl.edu/research/ParallelPatterns/PatternLanguage/AlgorithmStructure/Pipeline.htm
// GREAT NOTES - Parallel coding for .NET, LOTS OF CONCEPTS with graphs
//
// READ INTRO, it has TONS of high level "if this X problem, USE THIS Y PATTERN" 
//
/* "A future is a stand-in for a computational result that is initially unknown but becomes available at a later time.
 The process of calculating the result can occur in parallel with other computations. The Futures pattern integrates 
 task parallelism with the familiar world of arguments and return values. If the parallel tasks described in Chapter 3
  are asynchronous actions, futures are asynchronous functions. (Recall that actions don't return values, but functions do.)"
*/
// So think about it. Futures can be PASSED/MOVED AROUND to other functions and objects before they're actually READY or USED.
// However, THEY ARE NOT PIPELINES. 

// Introduciton 		- https://msdn.microsoft.com/en-us/library/ff963542.aspx
// Futures 				- https://msdn.microsoft.com/en-us/library/ff963556.aspx    <--- Will these help with the two phase problem?
// Pipelines 			- https://msdn.microsoft.com/en-us/library/ff963548.aspx
// Parallel Aggregation - https://msdn.microsoft.com/en-us/library/ff963547.aspx


// Appendix A: Adapting Object-Oriented Patterns (includes IMMUTABALE TYPES) - https://msdn.microsoft.com/en-us/library/ff963554.aspx

// https://www.cis.upenn.edu/~milom/cis501-Fall10/lectures/10_multithreading.pdf		hardware multithreading
// https://www.cis.upenn.edu/~milom/cis501-Fall05/lectures/12_smt.pdf					multithreading
// http://www.inf.ed.ac.uk/teaching/courses/pa/Notes/lecture09-multithreading.pdf
// https://ocw.mit.edu/courses/electrical-engineering-and-computer-science/6-823-computer-system-architecture-fall-2005/lecture-notes/l23_multithread.pdf
// http://www.drdobbs.com/the-challenges-of-developing-multithread/212600019


//http://stackoverflow.com/questions/3227042/maintaining-order-in-a-multi-threaded-pipeline
//http://stackoverflow.com/questions/14281799/multi-threading-a-stage-in-the-pipeline-pattern-with-executorservice


// https://dlang.org/phobos/std_parallelism.html
// https://dlang.org/phobos/std_concurrency.html


// IDEA: Favor TWO-STEP scripting. Scripts ALL run, and set flags. THEN, a second phase runs which REACTS to those flags.
//
// - This way, (hopefully?) no scripts can ever depend on data that CHANGES depending on the order of the scripts execution,
//	 and no scripts can ever depend on the run order. (maybe??)

// ---> IS IT POSSIBLE to create a system this way, such that, the RUN ORDER has no effect on the outcome?
//		- IF NOT, can we have a MOSTLY / EFFECTIVELY blackbox solution that works EXCEPT in certain cases, like object deaths?
//			- At that point, we'll reduce to SERIALIZING only the dependency chain involved.

// "Set flags" / REQUEST phase
// TICK #1 - Phase 1 (NOTE: NO FIXED ORDER for ANY of these three commands.)
//
// 		Object A, "I want Object C to die."
// 		Object B, "I send chat message to Object C."
// 		Object C, "Hurr durr, I like life."
//
// "React to flags." / ACTION phase
// Tick #1 - Phase 2
//
//		Message to Object C is sent - SUCCESS
// 		Object C dies. ("Omgawd, no.")
//
//		---(or)---
//
// 		Object C dies. ("Omgawd, no.")
//		Message to Object C is sent - FAIL ("no object found")
//
// 		NOT SURE??? Maybe all DELETIONS are handled AFTER messages? How do we handle object deaths without forcing everyone
//		to be aware of it? Or, (I think mentioned earlier), we have an "object_reference_died_handler(...)" that
//		gets called async AFTER the normal scripts get run, for object reference cleanup inside scripts.
//
// TICK #2 - Phase 1, script phase:
//
//		Object A - ???
// 		Object B - ???
//
// Is the ERROR in my UNDERSTANDING, or is the error simply that I'm not using a concept like "message passing"
// properly to encapsulate the two stages?

// DON'T FORGET -> We were also--on a previous occassion--talking about using a TWO-STEP object death setup to prevent
//	ever accessing a dead reference. 
//		- Objects denote themselves as "effectively dead"
//		- After all scripts are run, objects ARE deleted (which meanas references ARE invalidated), and ALL REFERENCES OBJECTS/LISTS
//		are now updated to show "this is a dead reference", then (MAYBE?) an async, PURE FUNCTION, deconstructor script callback 
// 		is run FOR ALL SCRIPTS that had a reference to that object.
//
/*		

//best terms? 
// - (Request) Action/reaction phases?
// - Request/react phases?
// etc.

 DO WE HAVE ANY OTHER PHASES? Seperate object REACT from object DELETIONS/CLEANUP?
*/

//SCRIPT PSEUDO CODE (would actually be in Lua, right? Unless we can get D scripting to work...)
// --------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------
object_t [] object_ref_list; 

void on_touch(object_t BY_WHO)
	{
	object_ref_list.push(BY_WHO); //anyone who touches us, add to the list of objects we know about.
	}

void tick() //called in PHASE 1, script phase
	{
	foreach(object_t o; object_ref_list)
		{
		o.send_message("I see you! Have you heard about our great haircare products!?"); //spam message every frame
		}
	}
	
// IF CALLED, it's called ASYNC (NO GUARANTEED ORDER) in PHASE 2 
// - DO NOT CALL ANYONE ELSE.
// - DO NOT SEND MESSAGES.
// etc?
void pure dead_object_callback_handler(object_t O) //note "pure"
	{
	object_ref_list[O].delete();   //find object O in array, and remove it from our list of newscasts. 
	
	// Super simple here, because we don't care about specific individuals.
	// BUT, what if we did? We'd need to set whatever flags necessary.
	//
	// For example, if we only had ONE TARGET (camera is watching and following THIS OBJECT)
	// Each frame we update the player.camera.xy = object.xy  
	// we would then delete the object reference from our internal list, and set the
	// camera to the LAST valid position, to ourself, or another object.
	
	// WARNING: if we allow people to manually update their reference list, we need to have
	// ASSERTS to make sure they aren't morons who either forget or INCORRECTLY manage their list.
	}
// --------------------------------------------------------------------------------------------------
// --------------------------------------------------------------------------------------------------




// Course/fine granularity - course = Split-into-macro-level-jobs,  fine=subtasks-are-paralleled

// False Sharing -- Sharing Cache lines making it SEEM like threads are touching each other's data.
//		Simply SPACE STUFF FAR ENOUGH APART.
//
// Ghost points - replicated data points so multiple threads can read(?) without contention.
// (or is this for NETWORK programming issue?)
// 		- TWO READS SHOULD go once, and be copied into each individul core's cache.
//		- IF one of those is then MODIFIED, the cache will have to be replicated (which may pause a core! SUPER SLOW)



// from [CplusplusConcurrencyInAction_PracticalMultithreading.pdf]
// high/low contention - how likely the processors will have to clash with/wait for each other.


// futures <---- maybe useful concept I don't fully understand yet?

// cache ping-pong - multiple cores reading and updating the same counter variable, constantly invalidates the other
// cores cache validity, so it spends TONS OF TIME constantly trying to keep the caches up to date with each other.
// (core1: writes to X, core2: reads from X then writes to X, core3: repeat, core4: repeat. Every other core's cache is
// invalid after an update!)
//		"A processor STALL prevents ANY MORE THREADS from running on that processor, as opposed to a mutex which the OS can swap jobs."
//
// ALSO NOTE according to the C++ book, ANY TIME fixing the cache is time the core CANNOT run a different, waiting thread.
// (It's an action that can't just throw your thread on hold while the core resolves it and runs a currently waiting thread.)

// WARNING ----> You're not using that, right? WHAT ABOUT A >MUTEX<. Reading and writing to the same place!

// Hazard pointers, pointers that point to objects to warn you someone is still using that object.
// reference counting



// parallel caching
//http://www.drdobbs.com/parallel/understanding-and-avoiding-memory-issues/212400410
// eliminate false sharing -- herb stutter
//http://www.drdobbs.com/go-parallel/article/showArticle.jhtml?articleID=217500206


// DEEP PEER-REVIEWER PAPER on false sharing
// http://scholarworks.sjsu.edu/cgi/viewcontent.cgi?article=1001&context=etd_projects

//http://stackoverflow.com/questions/944966/cache-memories-in-multicore-cpus
//https://scalibq.wordpress.com/2012/06/01/multi-core-and-multi-threading/
//http://www.cs.cmu.edu/afs/cs/academic/class/15418-s12/www/lectures/02_multicore.pdf
// SEE THIS PDF <--- lecture. Prefetching, multicore,etc.

	/* "To get passed memory throughput limitations"
		- Fetch data from memory LESS often.
			- Reuse data in a thread (traditional locality optimizations)
			- Share data across threads (cooperation)     [What happens if T2 is on a different CORE with its own cache??]
		- Request data less often (instead, do more math: it is "free")
			- Term: "Arithmetic intensity" - ratio of math to data access   <-------- !!!
	
	Related google searches for that term:
		http://www.nersc.gov/users/application-performance/measuring-arithmetic-intensity/
		https://crd.lbl.gov/departments/computer-science/PAR/research/roofline/
			SEE THIS <---graphs
		https://software.intel.com/en-us/forums/software-tuning-performance-optimization-platform-monitoring/topic/601488
		https://en.wikipedia.org/wiki/Roofline_model
			SEE THIS <---
		https://people.eecs.berkeley.edu/~waterman/papers/roofline.pdf
			SEE THIS <--- Looks detailed, starter article for a magazine.
	*/

/*
	"there simply aren't wires connecting the various CPU caches to the other cores.
	If a core wants to access data in another core's cache, the only data path 
	through which it can do so is the system bus."
	
	VS
	
	https://en.wikipedia.org/wiki/Smart_Cache
	
	"Intel Smart Cache shares the actual cache memory between the cores of a multi-core processor."


	How could we TEST THIS and instead of trying to sift through all these morons with conflicting answers?
*/

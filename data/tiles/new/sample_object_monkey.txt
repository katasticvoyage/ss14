[Monkey]
name="Monkey" 										# Pretty name
id="OBJ_MONKEY"										# internal name
description="Something that's not quite human."		# onLook
	 
	[Stats]
	health=25		# 100 = human
	armor=0
	speed=5.0
	team=1 			# 0=human/Nanotrassin, #1=nature
	is_player=0

		[Attack1]
		attack_icon=""
		attack_strength=5
		attack_cooldown=5
		attack_text1="{%s} bites {%s}."

		[Attack2]
		attack_icon=""
		attack_strength=5
		attack_cooldown=5
		attack_text2="{%s} claws {%s}."

		[Attack3]
		attack_icon=""
		attack_strength=10
		attack_cooldown=3
		attack_text3="{%s} pounces on {%s}."
		end_of_attack=11111111111111

	end_of_stats=222222222222222

	[Graphics]
	file=monkey.png
	#file2
	#file3
	#file4
	#size=32,32  #,0,0   # w,h, line padding x/y
	width=32
	height=32

	down=1,1
	left=1,2
	right=1,3
	up=2,1
	dead=3,3

		#sprite cell locations for various states
		# for file #2 (file #1 is implied)
		# up=#2,1,1
		# down=#2,15,20
		# etc
		# UHH, but wait, we don't want to use our COMMENT SYMBOL # for it!
		# or, change comment symbol to //
	end_of_graphics=44444444444444

end_of_root=3333333333333333

// for fun, incredibly simple chat popup attempt.

import common : viewport_t, globals_t, g;
import molto;
version(DigitalMars)
	{
	pragma(lib, "dallegro5_dmd");
	}
version(LDC)
	{
	pragma(lib, "dallegro5_ldc");
	}

version(ALLEGRO_NO_PRAGMA_LIB)
	{
	}else{
	pragma(lib, "allegro");
	pragma(lib, "allegro_primitives");
	pragma(lib, "allegro_image");
	pragma(lib, "allegro_font");
	pragma(lib, "allegro_ttf");
	pragma(lib, "allegro_color");
	}
import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;


enum BUBBLE_TYPE
	{
	CHAT,
	THOT,
	NARRATIVE //ala square frame with dashes describing an RP event / narrative
	}

class chat_handler_t
	{
	chat_bubble [] bubbles; //because we have relatively few, we don't need data-oriented design where each bubble has only one function and everything is branchless. We also don't need to cache unused ones as "dead == true" instead of reallocating.
	
	void spawn_bubble(string text, float x, float y, BUBBLE_TYPE type)
		{
		// Do all bubbles start with the same [server constant] length?
		// Also, how do we integrate (or do we need) history of chats
		// so you can look up, say the last 3 seconds of chats?
		chat_bubble c;
		c.x = x; 
		c.y = y; 
		c.type = type; 
		}
		
		
	void update_bubbles()
		{
		}
	
	void draw_bubbles(viewport_t v)
		{
		const int FONT_GLYPH_WIDTH = 16; //FIXME
		const int FONT_GLYPH_HEIGHT = 16; //FIXME
		foreach(b; bubbles)
			{
			float x1 = v.x - v.offset_x;
			float y1 = v.y - v.offset_y;
			float x2 = v.x - v.offset_x + FONT_GLYPH_WIDTH*32; //fix
			float y2 = v.y - v.offset_y + FONT_GLYPH_HEIGHT*1; //fix
			
			//FIXME: USE MOLTO <-------
			al_draw_filled_rectangle(x1, y1, x2, y2, al_map_rgb(255,255,255));
			al_draw_text(g.font, al_map_rgb(0,0,0), x1 - (x2-x1)/2, y1, ALLEGRO_ALIGN_LEFT, "HELLO WORLD"); //fix text
			}
		}
	
	}
	
struct chat_bubble
	{
	float x;
	float y;
	string text;
	int total_lifetime;
	int maximum_lifetime;
	BUBBLE_TYPE type;
	}

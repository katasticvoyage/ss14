

// NOTE this code isn't LINKED YET.


// Could we make the CLIENT (and shared) source code open-source and leave just the servers binary?
// or would that be too many "clues" for hackers? Obviously, eventually it will be full open source.
// But we don't want the barrier to entry for hackers being "download the github, compile, and start hacking."
// That's a little too easy. We want the relatively fewer people with reverse engineering knowledge
// to have to touch it instead of any person with a compiler the way One Hour One Life is.


class datetime{}

class frame_t /// OBJECT frame. One per logical frame. 
	{
	int frame_number; 
	datetime timestamp;	//timestamp, vs framenumber? The question is, what if one PC drifts and the frame number is different internally from the servers and vice-versa.
	object_t[] objects;
	map_t[]	maps;
	event_t [] events;
	void serialize(){}
	void unserialize(){}

	void apply_delta_frame(delta_frame_t d) //start with copy of previous frame, then apply deltas = f2
		{
		//f1 + d = f2
		}

	// - HOW DO WE HANDLE... sending LESS than all data?
	// delta data.

			// START with simplist version! Just sync all every frame! Then work from there.

	// Also, are there diff algorithms that could be useful?

	// Also, is there a template solution for having shared objects?
	// Think how the Unreal Engine has shared data, non-shared data, etc.
	}	



class delta_frame_t
	{
	int frame_number; 
	datetime timestamp; 
	object_t[] objects;
	map_t[]	maps;
	event_t [] events;
   // ?
	void serialize(){}
	void unserialize(){}
	}








// Do we need EVENT-BASED updates, or do we simply make sure objects that MUST be synced have their data copied over?
// And how do we handle PLAYER REQUESTS for stuff? WASD, mouse clicks, etc. Those should be event requests to the server.
class event_lut_t //event type look-up table
	{
	string name;
	string description;
	string formatted_description; // needed? for like, events like templates.
 	string write_formatted_event(event_t t) //it takes an event, of type, and puts the VALUES inside it, into the description.
		{ //tihnk like windows event viewer where they have templates/mailmerge/etc with fields that get filled in.

		string temp = format("Description: %s", t.data[0]); 
		return temp;
		}	
	}

class event_t //MUST BE SYNCED
	{
	int lut_id; 
	int []data; //what how do we do this? no idea yet.
	}

//https://www.google.com/search?ei=jmSsWoyVGcnazwLiqae4Cg&q=game+syncing+programming&oq=game+syncing+programming&gs_l=psy-ab.3..33i21k1.1438.2426.0.2469.12.10.0.0.0.0.145.913.5j4.9.0....0...1.1.64.psy-ab..3.9.909...0j0i22i30k1j33i160k1.0.it2xjuiqSQg
//https://www.cakesolutions.net/teamblogs/how-does-multiplayer-game-sync-their-state-part-1
//https://gamedev.stackexchange.com/questions/28820/how-do-i-sync-multiplayer-game-state-more-efficiently-than-full-state-updates


// [PROJECTILES]
// - HAVE TO BE SYNCED. Gun bullets have to line up.

// [PARTICLES]
// DO NOT have to be synced. Fire FX. etc. 
//	- WHAT IF WE have TWO KINDS.

// [Emitters]
// - Emitters positions are synced, but their construction of [Particles] are allowed to be interpreted/deleted as necessary per-client.
// - A [Smoke emitter] has a position, and if it just sits there and the client goes crazy (or GFX settings change!) it doesn't matter
//	if lag or whatever desyncing happens.

// -> STATISTICS. Our architecture SHOULD KNOW:
//
class global_sync_statistics_t
	{
	client_connection_sync_statistics_t [] clients;
	}

//https://code.dlang.org/packages/units-d  units of measurement support? useful?
//https://github.com/nordlow/units-d

class client_connection_sync_statistics_t // PER CONNECTION stats. 
	{
	int objects_synced;
	int objects_unsynced; 	// intentionally unsynced
	int objects_missed; 	// MISSED because of bandwidth/etc failure. (can this happen?)
	int emitters_synced;
	int emitters_unsynced;
	int emitters_missed; // (etc)
	int ping_ms;
	int bandwidth_bytes_sec;	
	}

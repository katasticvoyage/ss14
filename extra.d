/*
	HOLY FRAEKING SHIT. UBUNTU VIRTUAL DESKTOPS. Are actual, larger than screen desktops!!! So you can actually run this program with 2X the width, and scroll
	the display back-and-forth between the two "fake" monitors!!!
		- That means I can test "larger than display" resolutions for GUI alignment, as well as performance!

	[FUN]
	
		- Make simple chat (and think) popup.
		
		
		- OMG. ADD normal mapping for lighting!
			https://iaco79.wordpress.com/2015/03/27/lights-and-shadows-for-2d-worlds/ <--------
			
			http://archive.gamedev.net/archive/reference/articles/article2032.html
			
			https://stackoverflow.com/questions/18525214/efficient-2d-tile-based-lighting-system
			
			http://smashriot.com/environment-shadows-for-2d-sprites/
			
			<------
			https://www.kickstarter.com/projects/2dee/sprite-dlight-instant-normal-maps-for-2d-graphics
		
			https://www.gamasutra.com/view/news/312977/Adding_depth_to_2D_with_handdrawn_normal_maps_in_The_Siege_and_the_Sandfox.php
	
			https://forum.unity.com/threads/now-free-light2d-gpu-lighting-system.310732/
			
			https://en.sfml-dev.org/forums/index.php?topic=21653.0


	[TODO]

		---> Clicking on MINIMAP brings you/camera to that spot.
			[works!]

		----> USEFUL. Floodfill, that only floods where WALLS aren't. So you can floodfill "ROOM FLOORS" whereas normally floodfill on the floor layer basically destroys the entire station.
			[works]

		----> Move stuff into other source files. Smaller files are easier to reason with (except where they cross reference other ones.)
			
		----> UNDO/REDO
		
		----> object_monkey.ini

		----> GET FILE save_map2() working officially!
		
			- FIXME. It writes the file output with MAX_MAP_W/H. Not ACTUAL w/h.
			- Need to scale those layers on FILE LOAD and not at compile time!
			--> WHAT ABOUT WRITING OBJECT DATA???? 
	
		- Should store the selected_tile for each selected_layer, so we restore the point in the atlas where we were when we go back to floor or wall.

		- WHAT HAPPENS, when the MOUSE IS OUT OF FRAME?
			- We should have some kind of global setting (other than 0,0)
			to mark "out of bounds". Even just (x=65,535, y=65,535), or some other
			magic number.
			- Because we don't want to set (0,0) every time we click and the mouse
			is off somewhere special.

		- Minimap definitely generates incorrectly. The neighboring tiles probably "bleed" into each other (BIG TIME). Still, it works "good enough" for now.
		
		- MAP_W and MAP_H are used both as maximum map width/height, as well as ACTUAL
		width and height! Either use dynamically sized arrays and use .length /width/height, or, encode the map width/height by passing a map_t (map_t.data, map_t.w, map_t.h).  [see above note about save_map2]

		- Dialog/Mouse stuff.
		
		- Animated/detailed tiles (ala tile0002.ini).

		- Selection stuff. (Fun problem to solve!) 
		- Rectangles, circles, lasso, floodfill, etc.
 			- fill selection with tiles. 
 			- Movable selections one the selection is built. Add to/remove from selection with control/alt click.
		
		- Actually USE gfx3_t!
		- ACTUALLY PORT button_t over from module_inventory.d!!! Sick of manually writing these hooks.

		-> Should support expanding the map size from CANVAS top/down/left/right, as well as moving the entire map around the array so you can "recenter" a ship/station after adding to it, as well as expand the map size as necessary.
*/

version(DigitalMars)
	{
	pragma(lib, "dallegro5_dmd");
	}
version(LDC)
	{
	pragma(lib, "dallegro5_ldc");
	}

version(ALLEGRO_NO_PRAGMA_LIB)
	{}else{
	pragma(lib, "allegro");
	pragma(lib, "allegro_primitives");
	pragma(lib, "allegro_image");
	pragma(lib, "allegro_font");
	pragma(lib, "allegro_ttf");
	pragma(lib, "allegro_color");
	}

import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

import std.compiler;
import std.stdio;
import std.conv;
import std.string;
import std.format; 
import std.algorithm; 

import helper;
import molto;
import editor;
import common;
import object_t;
import map;
import animation;

//import ini;



bool am_i_a_server=false; //is there an easy way to deal with this without tons of exception cases everywhere?

//scan ini + build pieces of an object, from tiles loaded from an atlas?
// FIXME: Just load the file manually for now?
// ALSO, should an atlas be able to load multiple files and re-arrange them? Arrays? Or simply one atlas per file? (that could explode later, also lots of code writing...)
class object_builder_t
	{
	void load_object(string path_to_object_metadata)
		{
		File file = File(path_to_object_metadata, "r");
		//file.write("");
		}
	}

class object_handler_t
	{
	object_t [] objects;
	
	void setup()
		{
		}
	
	void draw(viewport_t v)
		{
		foreach(o; objects)
			{
			o.draw(v);
			}
		}
	}

class dialog_system_t {}

/*
extern (C) void glTexParameteri(int target,
 	int pname,
 	float param);
 	
 	NEED opengl LIB. How to integrate with Allegro5/DAllegro?
*/
int setup()
	{
//	int GL_TEXTURE_2D = 3553;
//	int GL_TEXTURE_MAG_FILTER = 10240;
//	int GL_NEAREST = 9728; // from: https://docs.factorcode.org/content/word-GL_NEAREST,opengl.gl.html
//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST); //kat
		
	g.world = new world_t; 
	g.world.atlas = new atlas_t("./data/tiles/floors.png", 30, 30, LAYER.FLOOR);
	g.world.atlas2 = new atlas_t("./data/tiles/walls.png", 10, 10, LAYER.WALL);
	g.world.atlas_hull = new atlas_t("./data/tiles/lattice.png", 5, 5, LAYER.HULL);
	g.world.atlas_decal = new atlas_t("./data/tiles/decals.png", 15, 15, LAYER.DECAL);
	
	// not actually USED HERE.
	g.world.atlas_obj = new atlas_t("./data/tiles/monkey.png", 3, 3, LAYER.OBJECT);
	// this one should be???
	
	g.world.atlas_obj_door = new atlas_t("./data/tiles/public.png", 6, 5, LAYER.DECAL);
		//currently we need an atlas before loading maps.
		// we may want to reverse it somehow and only load textures NEEDED for that map/setup.
		// but for now, just load everything.
	
	g.world.maps ~= new map_t(g.world.atlas); //FIXME, merge these two lines!@$!@
//	g.world.maps ~= new map_t(g.world.atlas);
	g.world.maps[0].load_map2("./data/maps/temp_input.txt");

//	g.world.maps[1].load_map2("./data/maps/temp_input_map2.txt");
//	g.world.maps[1].universe_offset_x = -64;
//	g.world.maps[1].universe_offset_y = -64;

	g.editor = new editor_t;

	// Object creation:
	//-----------------------------------------
	{ 
	animation_t anim;
		auto o = new object_t("Player 1", 100, 100, 0, g.world);
		anim = new animation_t(true); //monkey
		o.anim = anim;
//		g.world.objects ~= o;
	}
	{ 
//		auto o = new object_t("Player 2", 100, 200, 0, g.world);
	//	g.world.objects ~= o;
	}
	{
	animation_t anim;
	object_t obj;

		anim = new animation_t(false); //walking door
		obj = new object_t("Player 2B");
			obj.x = 350;
			obj.y = 256;
			obj.m = 0;
			obj.anim = anim;
		//		obj.world = g.world; //this would explode... world doesn't exist yet.	
//		g.world.objects ~= obj;
	}
	{
	animation_t anim;
	object_t obj;

		anim = new animation_t(true);
		obj = new object_t("Player 3B");
			obj.x = 200;
			obj.y = 200;
			obj.m = 0;
			obj.anim = anim;
		//		obj.world = g.world; //this would explode... world doesn't exist yet.	
//		g.world.objects ~= obj;
	}
	g.viewports[0] = new viewport_t(0, 0, g.SCREEN_W, g.SCREEN_H, 0, 0);
	g.viewports[1] = new viewport_t(320-1, 100, 320, 240, 0, 0);

	return 0;
	}
	
int logic()
	{
	const float SPACE_SPEED = 0.15f;
	const float SPACE_ROT_SPEED = 0.00001f;
	g.space_x += SPACE_SPEED*.25;
	g.space_y += SPACE_SPEED*.75;
	g.space_r += SPACE_ROT_SPEED;

	const float CAMERA_MOVE_SPEED = 8; //not using ATM since it's set to follow a character.

	// KEYBOARD/MOUSE HANDLING
	// -----------------------------------------------------------------------
	int corrected_x = to!int(g.mouse.x + g.viewports[0].offset_x) / g.TILE_D;
	int corrected_y = to!int(g.mouse.y + g.viewports[0].offset_y) / g.TILE_D;
	corrected_x.cap(0, g.world.maps[0].width); //omg UCFS is amazing
	corrected_y.cap(0, g.world.maps[0].height);
	
//	writefln("Mouse position in til [%s %s]", corrected_x, corrected_y);
	g.editor.cursor.move_to(corrected_x,corrected_y);

	const float MOVE_SPEED = 4;

	if(g.keys.pressed_1)
		{
		g.editor.cursor.set_tile_at_cursor( g.editor.cursor.stored_tile );		
		}
	if(g.keys.pressed_2)
		{
		g.editor.cursor.grab_tile();	
		}
		
		
	if(g.keys.pressed_5)
		{
		g.editor.cursor.set_layer(LAYER.FLOOR);
		}
	if(g.keys.pressed_6)
		{
		g.editor.cursor.set_layer(LAYER.WALL);
		}
	if(g.keys.pressed_7)
		{
		g.editor.cursor.set_layer(LAYER.HULL);
		}
	if(g.keys.pressed_8)
		{
		g.editor.cursor.set_layer(LAYER.DECAL);
		}
	
//		g.world.maps[0].expand_to(100,100);

	if(g.keys.pressed_w)
		{
		g.world.objects[0].attempt_to_move_rel(0,-MOVE_SPEED);
		g.world.objects[0].anim.set_direction(dir.up);
		}
	if(g.keys.pressed_s)
		{
		g.world.objects[0].attempt_to_move_rel(0,MOVE_SPEED);
		g.world.objects[0].anim.set_direction(dir.down);
		}
	if(g.keys.pressed_a)
		{
		g.world.objects[0].attempt_to_move_rel(-MOVE_SPEED,0);
		g.world.objects[0].anim.set_direction(dir.left);
		}
	if(g.keys.pressed_d)
		{
		g.world.objects[0].attempt_to_move_rel(MOVE_SPEED,0);
		g.world.objects[0].anim.set_direction(dir.right);
		}
	if(g.keys.pressed_up)
		{
		g.world.objects[1].attempt_to_move_rel(0,-MOVE_SPEED);
		g.world.objects[1].anim.set_direction(dir.up);
		}
	if(g.keys.pressed_down)
		{
		g.world.objects[1].attempt_to_move_rel(0,MOVE_SPEED);
		g.world.objects[1].anim.set_direction(dir.down);
		}
	if(g.keys.pressed_left)
		{
		g.world.objects[1].attempt_to_move_rel(-MOVE_SPEED,0);
		g.world.objects[1].anim.set_direction(dir.left);
		}
	if(g.keys.pressed_right)
		{
		g.world.objects[1].attempt_to_move_rel(MOVE_SPEED,0);
		g.world.objects[1].anim.set_direction(dir.right);
		}
	if(g.keys.pressed_i)
		{
		g.editor.cursor.previous_tile();
		al_wait_for_vsync();
		al_wait_for_vsync();
		al_wait_for_vsync();
		}
	if(g.keys.pressed_o)
		{
		g.editor.cursor.next_tile();
		al_wait_for_vsync();
		al_wait_for_vsync();
		al_wait_for_vsync();
		}
	if(g.keys.pressed_q)
		{
		g.draw_walls = false;
		g.world.maps[0].minimap.invalidate();
		}
	if(g.keys.pressed_e)
		{
		g.draw_walls = true;
		g.world.maps[0].minimap.invalidate();
		}
	if(g.keys.pressed_f)
		{
		g.editor.cursor.flood_fill();
		}

	foreach(o; g.world.objects)
		{
		o.tick();
		}

	return 0;
	} //main loop

// THIS ISN'T ACTUALLY USED YET. DURR.
/*int draw()
	{
	foreach(v; g.viewports)
		{
		v.draw();
		}
	
	return 0;
	}
*/

//-------------------------------------------
class dialog_t
	{
	float x,y,w,h;

	int z; //sorting order , what happens if we ever have TWO the same?!
	// or should Z order be "inherint" like, literally the array indices? Hmm... probably not...

	void onPress(float x_rel, float y_rel){}
	void onRelease(float x_rel, float y_rel){}
	}

class dialog_handler_t
	{
	dialog_t [] dialogs;

	bool check_bounds(dialog_t d, float x, float y)
		{
		if( x >= d.x)
		if( y >= d.y)
		if( x <= d.x + d.w) //FIXME, check for off-by-one errors
		if( y <= d.y + d.h)
			{
			return true;
			}
			
		return false;
		}

	void press(float x, float y)
		{
		foreach(d; dialogs)
			{
			if(check_bounds(d, x, y))
				{
				d.onPress(x,y);
				break; //should we only allow one? That'll work if they're z sorted.
				}
			}
		}
		
	void release(float x, float y)
		{
		foreach(d; dialogs)
			{
			if(check_bounds(d, x, y))
				{
				d.onPress(x,y);
				break; //should we only allow one? That'll work if they're z sorted.
				}
			}
		}
		
	int request_next_available_z()
		{
		return 0;//todo
		}
	
	void bump_z_with_new(dialog_t d) //if Z exists, move it down one (and any others needed)
		{
		//todo
		}
	
	void sort_by_z()//todo
		{
		import std.algorithm.sorting;
		
		dialogs.sort!"a.z < b.z";
		}
	}

int init()
	{
	if (!al_init())
	{
		auto ver = al_get_allegro_version();
		auto major = ver >> 24;
		auto minor = (ver >> 16) & 255;
		auto revision = (ver >> 8) & 255;
		auto release = ver & 255;

		writefln("The system Allegro version (%s.%s.%s.%s) does not match the version of this binding (%s.%s.%s.%s)",
			major, minor, revision, release,
			ALLEGRO_VERSION, ALLEGRO_SUB_VERSION, ALLEGRO_WIP_VERSION, ALLEGRO_RELEASE_NUMBER);
	
		return 1;
	}
	
	//ALLEGRO_CONFIG* cfg = al_load_config_file("test.ini");
	g.display = al_create_display(g.SCREEN_W, g.SCREEN_H);

	if (!al_install_keyboard())      assert(0, "al_install_keyboard failed!");
	if (!al_install_mouse())         assert(0, "al_install_mouse failed!");
	if (!al_init_image_addon())      assert(0, "al_init_image_addon failed!");
	if (!al_init_font_addon())       assert(0, "al_init_font_addon failed!");
	if (!al_init_ttf_addon())        assert(0, "al_init_ttf_addon failed!");
	if (!al_init_primitives_addon()) assert(0, "al_init_primitives_addon failed!");

	g.space_bg_bmp = al_load_bitmap("./data/tiles/bg2.png");
	g.font = al_load_font("./data/DejaVuSans.ttf", 18, 0);

	with(ALLEGRO_BLEND_MODE)
		{
		al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
		}

	g.fps_timer = al_create_timer(1);// / 10.0);
//		g.double_click_timer = al_create_timer(1 / 60.0);

	g.queue = al_create_event_queue();
	al_register_event_source(g.queue, al_get_display_event_source(g.display));
	al_register_event_source(g.queue, al_get_keyboard_event_source());
	al_register_event_source(g.queue, al_get_mouse_event_source());
	al_register_event_source(g.queue, al_get_timer_event_source(g.fps_timer));

//	al_start_timer(g.double_click_timer); 
	al_start_timer(g.fps_timer); 
	//this one just has to run to be a delta, it doesn't matter if we start it only when the game starts or right now.
		
	gfx = new gfx_t();
	g.gfx3 = new gfx3_t(g.display, g.queue, g.font);
	return 0;
	}

void shutdown()
	{
	}

int main(string[] args)
	{
//	test_function2();//test
	//test_dialog_anything();
	//auto ini = new ini_parser("./data/tiles/new/sample_object_monkey.txt");

static if(true)
	{
	if(args.length > 1)
		{
		foreach(int i, line; args[1..$])
			{
			writeln(i, " - ", line);
			}
		if(args.length == 3)
			{
			g.SCREEN_W = to!int(args[1]);
			g.SCREEN_H = to!int(args[2]);
			writefln("Setting screen to %dx%d.", g.SCREEN_W, g.SCREEN_H);
			}
		}
	
	version(DigitalMars)
		{
		writeln("Using DMD compiled version.");
		}
	version(LDC)
		{
		writeln("Using LDC compiled version.");
		}
	
	return al_run_allegro(
	{
		if( init() ) return -1;
		if( setup() ) return -1; //<---- NOTE our setup routine is here.

		bool exit = false;
		while(!exit)
		{
			ALLEGRO_EVENT event;
			while(al_get_next_event(g.queue, &event))
			{
				switch(event.type)
				{
					case ALLEGRO_EVENT_DISPLAY_CLOSE:
					{
						exit = true;
						break;
					}
					case ALLEGRO_EVENT_TIMER:
					{
						g.fps = g.frames_passed;
						g.frames_passed = 0;
						//writefln("%d", g.frames_passed);
						break;
					}
					case ALLEGRO_EVENT_KEY_DOWN:
					{
						switch(event.keyboard.keycode)
						{
							case ALLEGRO_KEY_ENTER: //SAVE ENTER
							{
								string path="./data/maps/temp_input_v2.txt";
								writefln("SAVING FILE TO %s", path); 
								g.world.maps[0].save_map2(path); //NOTE
								break;
							}
							case ALLEGRO_KEY_0:
							{
								g.keys.pressed_0 = true;
								break;
							}
							case ALLEGRO_KEY_1:
							{
								g.keys.pressed_1 = true;
								break;
							}
							case ALLEGRO_KEY_2:
							{
								g.keys.pressed_2 = true;
								break;
							}
							case ALLEGRO_KEY_3:
							{
								g.keys.pressed_3 = true;
								break;
							}
							case ALLEGRO_KEY_4:
							{
								g.keys.pressed_4 = true;
								break;
							}
							case ALLEGRO_KEY_5:
							{
								g.keys.pressed_5 = true;
								break;
							}
							case ALLEGRO_KEY_6:
							{
								g.keys.pressed_6 = true;
								break;
							}
							case ALLEGRO_KEY_7:
							{
								g.keys.pressed_7 = true;
								break;
							}
							case ALLEGRO_KEY_8:
							{
								g.keys.pressed_8 = true;
							break;
							}
							case ALLEGRO_KEY_9:
							{
								g.keys.pressed_9 = true;
							break;
							}



							case ALLEGRO_KEY_UP:
							{
								g.keys.pressed_up = true;
								break;
							}
							case ALLEGRO_KEY_DOWN:
							{
								g.keys.pressed_down = true;
								break;
							}
							case ALLEGRO_KEY_LEFT:
							{
								g.keys.pressed_left = true;
								break;
							}
							case ALLEGRO_KEY_RIGHT:
							{
								g.keys.pressed_right = true;
								break;
							}
							case ALLEGRO_KEY_W:
							{
								g.keys.pressed_w = true;
							break;
							}
							case ALLEGRO_KEY_S:
							{
								g.keys.pressed_s = true;
							break;
							}
							case ALLEGRO_KEY_A:
							{
								g.keys.pressed_a = true;
							break;
							}
							case ALLEGRO_KEY_D:
							{
								g.keys.pressed_d = true;
							break;
							}							
							case ALLEGRO_KEY_I:
							{
								g.keys.pressed_i = true;
							break;
							}
							case ALLEGRO_KEY_O:
							{
								g.keys.pressed_o = true;
							break;
							}
							case ALLEGRO_KEY_Q:
							{
								g.keys.pressed_q = true;
							break;
							}
							case ALLEGRO_KEY_E:
							{
								g.keys.pressed_e = true;
							break;
							}
							case ALLEGRO_KEY_F:
							{
								g.keys.pressed_f = true;
							break;
							}

							case ALLEGRO_KEY_ESCAPE:
							{
	//							g.world.maps[0].save_map("./data/maps/map000.txt"); //TEST
//								g.world.maps[0].save_map("./data/maps/map000_walls.txt"); //TEST, FIXME
								exit = true;
								break;
							}
							default:
						}
						break;
					}
					case ALLEGRO_EVENT_KEY_UP:
					{
						switch(event.keyboard.keycode)
						{
							case ALLEGRO_KEY_0:
							{
								g.keys.pressed_0 = false;
								break;
							}
							case ALLEGRO_KEY_1:
							{
								g.keys.pressed_1 = false;
								break;
							}
							case ALLEGRO_KEY_2:
							{
								g.keys.pressed_2 = false;
								break;
							}
							case ALLEGRO_KEY_3:
							{
								g.keys.pressed_3 = false;
								break;
							}
							case ALLEGRO_KEY_4:
							{
								g.keys.pressed_4 = false;
								break;
							}
							case ALLEGRO_KEY_5:
							{
								g.keys.pressed_5 = false;
								break;
							}
							case ALLEGRO_KEY_6:
							{
								g.keys.pressed_6 = false;
								break;
							}
							case ALLEGRO_KEY_7:
							{
								g.keys.pressed_7 = false;
								break;
							}
			

							case ALLEGRO_KEY_UP:
							{
								g.keys.pressed_up = false;
							break;
							}
							case ALLEGRO_KEY_DOWN:
							{
								g.keys.pressed_down = false;
							break;
							}
							case ALLEGRO_KEY_LEFT:
							{
								g.keys.pressed_left = false;
							break;
							}
							case ALLEGRO_KEY_RIGHT:
							{
								g.keys.pressed_right = false;
							break;
							}
							case ALLEGRO_KEY_W:
							{
								g.keys.pressed_w = false;
							break;
							}
							case ALLEGRO_KEY_S:
							{
								g.keys.pressed_s = false;
							break;
							}
							case ALLEGRO_KEY_A:
							{
								g.keys.pressed_a = false;
							break;
							}
							case ALLEGRO_KEY_D:
							{
								g.keys.pressed_d = false;
							break;
							}
							case ALLEGRO_KEY_I:
							{
								g.keys.pressed_i = false;
							break;
							}
							case ALLEGRO_KEY_O:
							{
								g.keys.pressed_o = false;
							break;
							}
							case ALLEGRO_KEY_8:
							{
								g.keys.pressed_8 = false;
							break;
							}
							case ALLEGRO_KEY_9:
							{
								g.keys.pressed_9 = false;
							break;
							}
							case ALLEGRO_KEY_Q:
							{
								g.keys.pressed_q = false;
							break;
							}
							case ALLEGRO_KEY_E:
							{
								g.keys.pressed_e = false;
							break;
							}
							case ALLEGRO_KEY_F:
							{
								g.keys.pressed_f = false;
							break;
							}
							
							default:
						}
						
					}
					break;
					
					case ALLEGRO_EVENT_MOUSE_AXES:
					 	{
						g.mouse.capture_movement_state(event.mouse.x, event.mouse.y);
						break;
						}

					case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
					{
						writefln("MOUSE BUTTON %d pressed", event.mouse.button);
						if(event.mouse.button == 1)
							{
							g.mouse.button_down(CLICK_TYPE.CLICK_LEFT);
							}
						if(event.mouse.button == 2)
							{
							g.mouse.button_down(CLICK_TYPE.CLICK_RIGHT);
							}
						break;
					}

					case ALLEGRO_EVENT_MOUSE_BUTTON_UP: //ON MOUSE RELEASE
					{
/*						if(event.mouse.button == 1)
							{
							mouse.button_up(CLICK_TYPE.CLICK_LEFT);
							}
						if(event.mouse.button == 2)
							{
							mouse.button_up(CLICK_TYPE.CLICK_RIGHT);
							}
						break;*/
					}
					default:
				}
			}
			al_clear_to_color(g.gfx3.rgb(0, 0, 0));
			logic();
			
			//draw();
			g.viewports[0].draw();
		//	g.viewports[1].draw();
		//	g.viewports[1].offset_y += 0.25f;

			// KAT
			// test_inst(); //template test.
			// test_funct2(); OMFG, this shouldn't be in the MAIN LOOP WTF. This is likely the memory leak!
	
			al_draw_textf(g.font, ALLEGRO_COLOR(1,1,1,1), 20, 20, 0, "%2.2f FPS", g.fps);
			al_draw_textf(g.font, ALLEGRO_COLOR(1,1,1,1), 20, 20+g.font.h*1, 0, "[%f,%f]", g.world.objects[0].x,g.world.objects[0].y);
			al_draw_textf(g.font, ALLEGRO_COLOR(1,1,1,1), 20, 20+g.font.h*2, 0, "[%f,%f]", g.world.objects[1].x,g.world.objects[1].y);

			al_draw_textf(
				g.font, 
				ALLEGRO_COLOR(1,1,1,1), 
				20, 20+g.font.h*3, 0, 
				"Number of objects [%d]", 
				g.world.objects.length);

			//writefln("%f %f", g.world.objects[0].x, g.world.objects[0].y);
			//al_wait_for_vsync(); // MAYBE. Reduces laptop CPU usage from ~100% to ~25%.
			al_flip_display(); //NOTE, we're drawing EVEN if there are no updates!
			g.frames_passed++;
		}

		shutdown();
		return 0;
	});

	}else{
		return 0;
		}//static if
}

class particle_t 
	{
	float x,y;
	int m;
	}

class projectile_t 
	{
	float x,y;
	int m;
	}

class tile_type_t 
	{ 
	} //same as wall???

class wall_t
	{
	bool blocks_people;
	bool blocks_high;	// tall things
	bool blocks_low;    //crawling/short things

	bool blocks_kinetic; // bullets
	bool blocks_laser;
	bool blocks_acid;
	bool blocks_plasma; // same as acid?
	bool blocks_pulse; 
	bool blocks_sound; // a variable amount?
	bool blocks_flame; // not same as heat!
	bool blocks_heat; // ?
	bool skip_heat_calculations; // ?
	bool blocks_liquid;
	float heat_resistence_coefficient;
	float sound_resistence_coefficient;

	float health;
	float resistance_to_kinetic;
	//etc

	wall_t destructs_into; // ?
	}

class tile_instance_t
	{
	tile_type_t type; //or int index?
	//instance specifics.
	// any needed?
	}
	
struct tile_type
	{
	ulong i=0; 
	alias i this;
	// see some notes in notes.d
	}
	

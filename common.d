import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

import object_t;
import map;
import editor;
import molto;
import helper;

import std.stdio;
import std.format;

// NOTE this can't be in globals since it needs to be an array dimension argument
// ala:
// int [MAP_W][MAP_H] map; 
const int MAP_W = 25; //this should be MAXIMUM map width/height
const int MAP_H = 25;
const int MAX_MAP_W = 100;
const int MAX_MAP_H = 100;
// WARNING, this kind of "breaks" the intent of having globalst, which
// is to HIGHLIGHT every global access.

enum COLOR
	{
	BLACK = ALLEGRO_COLOR(0,0,0,1),
	WHITE = ALLEGRO_COLOR(1,1,1,1),
	RED = ALLEGRO_COLOR(1,0,0,1),
	GREEN = ALLEGRO_COLOR(0,1,0,1),
	BLUE = ALLEGRO_COLOR(0,0,1,1)
	}
	
struct dialog_manager
	{
	void draw_text(viewport_t v, string text){}
	}

struct mouse_t
	{
	bool is_being_held = false;
	int x, y;
	int starting_x, starting_y;
	//void click_at(int x, int y){}

	long click_event_timestamp = 0;

	void button_down(CLICK_TYPE button_type)
		{
		if(button_type == CLICK_TYPE.CLICK_RIGHT)
			{
//			context_menu1.check_collision(mouse.x, mouse.y);
			g.editor.cursor.grab_tile();
			}
			
		// DOUBLE CLICK DETECTION
		// THIS IS NOT WORKING YET. It currently detects "every other" click. 
		if(button_type == CLICK_TYPE.CLICK_LEFT)
			{

			if(
				x > g.world.maps[0].minimap.x &&
				y > g.world.maps[0].minimap.y &&
				x < g.world.maps[0].minimap.x + g.world.maps[0].minimap.minimap_bmp.w*g.world.maps[0].minimap.scale_factor &&
				y < g.world.maps[0].minimap.y + g.world.maps[0].minimap.minimap_bmp.h*g.world.maps[0].minimap.scale_factor
				)
				{
				float rel_x = x - g.world.maps[0].minimap.x;
				float rel_y = y - g.world.maps[0].minimap.y;
				writefln("MOUSE CLICK ON MINIMAP at %f,%f", rel_x, rel_y);
				g.world.maps[0].minimap.onClick(rel_x, rel_y);
				}






			if(y < g.TILE_D*2) //top atlas is 0 to TILE_D-1, then the second atlas, walls, is TILE_D to TILE_D*2-1
				{
				writefln("TOUCHED");
				//ASSET picker
				g.editor.cursor.stored_tile = x/g.TILE_D;
				if(y < g.TILE_D)
					{
					g.editor.cursor.set_layer(LAYER.FLOOR);
					}else{
					g.editor.cursor.set_layer(LAYER.WALL);
					}
				}else{
				// Pick from map
				g.editor.cursor.place_tile();
				}				
							
/*			if(click_event_timestamp != 0)
				{
				long click_event_timestamp_end = al_get_timer_count(g.double_click_timer);
				long delta = click_event_timestamp_end - click_event_timestamp;
				
				writefln("Delta is %d", delta);
				click_event_timestamp = 0;
				//https://www.allegro.cc/manual/5/al_get_timer_speed
				writeln("END MOUSE double click check.");
				}else{
				writeln("BEGIN MOUSE double click time.");
				click_event_timestamp= al_get_timer_count(g.double_click_timer);
				}
			
			
			mouse.capture_starting_position();
			mouse.is_being_held = true;*/
			}
		}

	void button_up(CLICK_TYPE button_type)
		{
		is_being_held = false;

		if(x == starting_x)
			{
			if(y == starting_y)
				{
				clicked_without_drag(button_type);
				}
			}else{	
			clicked_with_drag();
			}
		}
	
	void capture_movement_state(int _x, int _y)
		{
		x = _x;
		y = _y;
		}
	
	void capture_starting_position()
		{
		starting_x = x;
		starting_y = y;
		}
	
	void clicked_with_drag()
		{
		writefln("Mouse click with drag at %d, %d", x, y);
//		dialog_handler.mouse_collision_drag();
		}

	void clicked_without_drag(CLICK_TYPE button_type)
		{
		writefln("Mouse clicked without moving at %d, %d", x, y);
//		dialog_handler.mouse_collision(button_type);
		}
	}

enum CLICK_TYPE
	{
	CLICK_LEFT = 1,  //Allegro = 1
	CLICK_MIDDLE,  // ?? 3?
	CLICK_RIGHT = 2, //Allegro = 2
	CLICK_4,  // ??
	CLICK_5  // ??
	}

	


struct keys_t
	{
	bool pressed_down=0;
	bool pressed_up=0;
	bool pressed_left=0;
	bool pressed_right=0;
	bool pressed_w=0;
	bool pressed_s=0;
	bool pressed_a=0;
	bool pressed_d=0;
	bool pressed_i=0;
	bool pressed_o=0;
	
	bool pressed_q=0;
	bool pressed_e=0;
	bool pressed_f=0;
	
	bool pressed_0=0;
	bool pressed_1=0;
	bool pressed_2=0;
	bool pressed_3=0;
	bool pressed_4=0;
	bool pressed_5=0;
	bool pressed_6=0;
	bool pressed_7=0;
	bool pressed_8=0;
	bool pressed_9=0;
	}

struct globals_t
	{
	gfx3_t gfx3;
		
	dialog_manager dm; //test
		
	bool draw_walls=true;	//rename "draw wall layer?"
	bool draw_floors=true; 
	bool draw_objects=true; 
	bool draw_hull=true; 
	bool draw_decals=true; 
	// map triggers, waypoints, etc.
		
	editor_t editor;
		
	mouse_t mouse;
		
	ALLEGRO_FONT *font;
	ALLEGRO_TIMER *fps_timer;
	ALLEGRO_TIMER *double_click_timer;
	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT_QUEUE *queue;

	int SCREEN_W = 1300; //thank god we can change this at run time.
	int SCREEN_H = 740;
	const int TILE_D = 32; //set to 64 for 2x mode (and set floors2x.png)

	ALLEGRO_BITMAP *space_bg_bmp;	
	keys_t keys;
	world_t world;

	float fps=0;
	int frames_passed=0;

	viewport_t [2] viewports;
		
	float space_x=0, space_y=0, space_r=0;//temp
	}
	
globals_t g;

enum LAYER
	{
	FLOOR = 0,
	WALL = 1,
	HULL = 2, //structural reinforcement/grid/under flooring 
	DECAL = 3,
	
	OBJECT = -1, //NOTE, will this explode stuff?
	ZONE,
	WAYPOINT,
	PIPING,
	LIGHTING,
	BLAH_BLAH
	}

class world_t
	{
	object_t [] objects;
	map_t [] maps;
	atlas_t atlas; //floors FIXME
	atlas_t atlas2; //walls FIXME
	atlas_t atlas_hull; //hull
	atlas_t atlas_decal; //decal
	
	atlas_t atlas_obj;
	atlas_t atlas_obj_door;
	}

class atlas_t
	{
	ALLEGRO_BITMAP *atlas_bmp;
	ALLEGRO_BITMAP *atlas_mini_bmp;
	ALLEGRO_BITMAP *[] tiles_bmp;
	ALLEGRO_COLOR   [] tiles_minimap_pixels;

	LAYER layer_number;
	const int TILE_SIZE=32; //why is this not g.TILE_D

	@disable this(); 
	this(string path, int w, int h, LAYER layer_type)
		{
		import std.exception;
		import std.string : toStringz;		
		import std.stdio; 
		import std.file;		
	
		layer_number = layer_type;
		
		atlas_bmp = al_load_bitmap(path.toStringz());
		if(atlas_bmp == null)throw new FileException(format("OMGAWD %s", path));

//--------------------------------------
		assert(atlas_mini_bmp == null, "Don't recreate me!");
		atlas_mini_bmp = al_create_bitmap(w*1, h*1); //num tiles * 1 pixel

		ALLEGRO_BITMAP *original_bmp = al_get_target_bitmap();
		al_set_target_bitmap(atlas_mini_bmp);
			{
			al_draw_scaled_bitmap(atlas_bmp,
						// Source
						0,
						0,
						atlas_bmp.w, 
						atlas_bmp.h,
						
						// Destination 
						0,
						0,
						1.0f*g.TILE_D,  //scale it down to one pixel!
						1.0f*g.TILE_D,
						0);
			}
		al_set_target_bitmap(original_bmp);
//--------------------------------------

		for(int j = 0; j < h; j++)
			for(int i = 0; i < w; i++)
				{
				tiles_bmp ~= al_create_sub_bitmap(
					atlas_bmp, 
					i*TILE_SIZE, 
					j*TILE_SIZE, 
					TILE_SIZE, 
					TILE_SIZE);
				tiles_minimap_pixels ~= al_get_pixel(atlas_mini_bmp, i, j);
				}
	
		// fixme ENCAPSULATION LEAK
		// this doesn't exactly work with WALLS
		tiles_minimap_pixels[0] = al_map_rgb(0,0,0); //force 0 to be space/black because it's Bright Pink transparency.
		tiles_minimap_pixels[1] = al_map_rgb(32,32,32); //force 1 to be blackish because it's a TRANSPARENT GLASS
		
		writefln("Number of atlas entries %d", tiles_bmp.length);
		}
		
	ALLEGRO_BITMAP *grab_tile(int index)
		{
		assert(index < tiles_bmp.length); //FIXME
		return tiles_bmp[index];
		}
		
	void draw(viewport_t v, float x, float y)
		{
		// Draw 1D table of all tiles
		// FIXME. Lots of encapsulation and hardcoded WIDTH issues.
		// Use viewport, et al
		int cursor = g.editor.cursor.stored_tile;
		int tiles_in_viewport_width = v.w/TILE_SIZE;
		int tile_offset = cursor - (tiles_in_viewport_width)/2;
		if(tile_offset < 0)tile_offset = 0;
		
		int skipped=0;
		for(int i=tile_offset; i< tiles_bmp.length; i++)
			{
			if((i-tile_offset)*TILE_SIZE > v.w)break;
			
			al_draw_bitmap(
				tiles_bmp[i], 
				v.x + x + i*TILE_SIZE - tile_offset*TILE_SIZE, 
				v.y + y, 
				0);

			if(i == g.editor.cursor.stored_tile)
				{
				if(layer_number == g.editor.cursor.selected_layer)
					{
					ALLEGRO_COLOR c = ALLEGRO_COLOR(1,0,0,.75);
						
					if(layer_number == LAYER.WALL)
						{
						c = ALLEGRO_COLOR(0,1,0,.75);
						}
						
					al_draw_rectangle(	
						v.x + x + (i-tile_offset)    * TILE_SIZE + 0.5f, 
						v.y + y + 0.5f, 
						v.x + x + (i+1-tile_offset) * TILE_SIZE - 1 + 0.5f, 
						v.y + y +     1 * TILE_SIZE - 1 + 0.5f,
						c,
						2.0f
						);
						
					al_draw_line(
						v.x + x + (i-tile_offset)   * TILE_SIZE + 0.5f, 
						v.y + y + 0.5f, 
						v.x + x + (i-tile_offset) * TILE_SIZE - 1 + 0.5f, 
						v.y + y +     1 * TILE_SIZE - 1 + 0.5f + 50,
						c, 10);
					}
//				continue;
				}
			}
		
		if(layer_number == LAYER.FLOOR) //ENCAPSULATION LEAK. Draws different location based on field.
			{
			al_draw_text(g.font, ALLEGRO_COLOR(1,1,1,1), x, y, 0, "FLOORS");

			// Draw the full pixel atlas
			al_draw_scaled_bitmap(atlas_mini_bmp,
							// Source
							0, //FIXME: Encapsulation leak.
							0,
							atlas_mini_bmp.w, 
							atlas_mini_bmp.h,
							
							// Destination 
							v.x + 30  + x,
							v.y + 400 + y,
							1.0f*atlas_mini_bmp.w*4,
							1.0f*atlas_mini_bmp.h*4,
							0);
			}
		
		if(layer_number == LAYER.WALL) //ENCAPSULATION LEAK. Draws different location based on field.
			{
			al_draw_text(g.font, ALLEGRO_COLOR(1,1,1,1), x, y, 0, "WALLS");
		
			// Draw the full pixel atlas
			al_draw_scaled_bitmap(atlas_mini_bmp,
							// Source
							0, //FIXME: Encapsulation leak.
							0,
							atlas_mini_bmp.w, 
							atlas_mini_bmp.h,
							
							// Destination 
							v.x + 300 + x,
							v.y + 400 + y,
							1.0f*atlas_mini_bmp.w*4,
							1.0f*atlas_mini_bmp.h*4,
							0);
			}

		if(layer_number == LAYER.HULL) //ENCAPSULATION LEAK. Draws different location based on field.
			{
			al_draw_text(g.font, ALLEGRO_COLOR(1,1,1,1), x, y, 0, "HULLS");
		
			// Draw the full pixel atlas
			al_draw_scaled_bitmap(atlas_mini_bmp,
							// Source
							0, //FIXME: Encapsulation leak.
							0,
							atlas_mini_bmp.w, 
							atlas_mini_bmp.h,
							
							// Destination 
							v.x + 400 + x,
							v.y + 400 + y,
							1.0f*atlas_mini_bmp.w*4,
							1.0f*atlas_mini_bmp.h*4,
							0);
			}

		if(layer_number == LAYER.DECAL) //ENCAPSULATION LEAK. Draws different location based on field.
			{
			al_draw_text(g.font, ALLEGRO_COLOR(1,1,1,1), x, y, 0, "DECALS");
		
			// Draw the full pixel atlas
			al_draw_scaled_bitmap(atlas_mini_bmp,
							// Source
							0, //FIXME: Encapsulation leak.
							0,
							atlas_mini_bmp.w, 
							atlas_mini_bmp.h,
							
							// Destination 
							v.x + 180 + x,
							v.y + 400 + y,
							1.0f*atlas_mini_bmp.w*4,
							1.0f*atlas_mini_bmp.h*4,
							0);
			}
		}
	}

class viewport_t
	{
	int x,y,w,h;
	float offset_x, offset_y;

	this(int x, int y, int w, int h, float offset_x, float offset_y)
		{
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.offset_x = offset_x;
		this.offset_y = offset_y;
		}
	
	void draw()
		{
		// How do we know what map to inform to draw itself?
		// Deal with later. 

		al_draw_scaled_bitmap(
			g.space_bg_bmp, 
			0,0,
			g.space_bg_bmp.w,
			g.space_bg_bmp.h,
			x-g.space_x - offset_x/25.0, 
			y-g.space_y - offset_y/25.0, 
			g.space_bg_bmp.w/3,
			g.space_bg_bmp.h/3,
			0);

/*
		al_draw_scaled_rotated_bitmap(
			g.space_bg_bmp,
			g.space_bg_bmp.w/2, 
			g.space_bg_bmp.h/2, 
			0-g.space_x - offset_x/25.0, 
			0-g.space_y - offset_y/25.0, 
			0.5, 
			0.5,
			g.space_r, 
			0);
  */ 
   
   		g.world.maps[0].draw(this);
   		//g.world.maps[1].draw(this);
 		
		foreach(o; g.world.objects)
			{
			o.draw(this);
			}
			
		g.world.maps[0].minimap.draw(this);
	
		// LAST
		// ------------------------------------------------------
		g.world.atlas.draw 		(this, 0, g.TILE_D*0);//temp	
		g.world.atlas2.draw		(this, 0, g.TILE_D*1);//temp	
		g.world.atlas_hull.draw	(this, 0, g.TILE_D*2);//temp	
		g.world.atlas_decal.draw(this, 0, g.TILE_D*3);//temp	

		g.editor.draw(this);		
		g.editor.cursor.draw(this); //DRAW MOUSE CURSOR STUFF LAST   palette thingy? 

		// LOGIC 
		// ----------------------------------------------------------------------
		center_viewport_on(g.world.objects[0]); //fixme ish
		}

	void center_viewport_on(object_t o)
		{
		offset_x = g.world.objects[0].x - g.SCREEN_W/2;
		offset_y = g.world.objects[0].y - g.SCREEN_H/2;	
		}
	}
	
enum dir
	{
	down,
	up,
	right,
	left,
	dead
	}

struct cell_location
	{
	int x, y;
	int index; //for 1-D mode
	}



/*
 - Blood splatter on voxels?


	--> WARNING. If we use "voxel tilsets" we're not going to have independant WALL and FLOOR layers are we?! And how do we handle wall destruction, etc?
		- We can destroy an entire tile, but if we make voxel tiles HUGE in dimensions, how are they going to be destroyed in PART?
		
	- If we make voxel tiles out of smaller chunks (3x3x3?), we can, but then we have ANOTHER layer people have to much around with.
	
	--> WHAT RESOLUTION? Even if we decide, we need a tile voxel resolution. 126^3 (6?!?) is the max in MagicaVoxel but there are other suites if necessry. (one, VoxelShop, is actually open source and will eventually support skeletal animation!)
	
		- What we COULD do is start with a higher res version, then down sample it over and over
		and see what "style" we really want.



 - VOXELS idea: 
	- IDEA is, use VOXELS for objects, vehicles, etc. BUT use 2-D sprites for PEOPLE.
		- NEED TO DO ART TESTS to see if that'd even work
	- We have animations.
	
	- How do we handle MULTIPLE FLOORS and/or 3-D? We'd have TONS of new problems to solve if we allow 3-D movement instead of "3-D stuff, but restricted to 2-D plane."
		- One limited idea: Only allow a tile heightmap. So you can have higher sections but, they're just raised platforms, with ladders. As in YOU CAN'T go under a platform. It breaks up the monotony of flatness.
		
		- But we can't allow FREE-FORM 3-D artwork. It needs to be TILE based.
		- And then TILES can have STUFF put in them? But then we'll need walls that support
			- HARDPOINTS
-----------------------------------------------------------------------------------
				- Small, medium, large? [Wall hardpoints, vs ceiling/floor]
				- Each "room" has a mix of them.
				- Things like a power switch, go in a small hardpoint.
				
				- An ENGINEER can ADD a needed hardpoint to an area (single or group of connected walls) by using tools. 
					- CANON reason could be "walls are deployed as pre-fabbed units" so you have to MODIFY the pre-fabs to add pieces you need. This is "space" so it's important to have prefab, replacable chunks.

				- Need to figure out how to integrate them properly with MV (MagicaVoxel). 
					- The simplest of course is an ini file with mnually edited metadata.
					- We could also MAKE a secondary editor so you make the first part, and the [meta editor] will then let you SEE the thing live, while changing things around like the [team palette], or lighting, or marking things as glowing, or whatever. 
			
				- Metadata



		- We could do TEXTURES that are colored a certain way. But not sure how that works. The straight up version is a black-and-white gradient (and maybe TWO of them for alternating colors) (maybe three). And then those selections are modified by run-time paramaters, or, object parameters
			- Remember, we can have each voxel be like a "texture" and a "material" is a combination of a VOXEL and METADATA. So we can have "green room" and "red room" regardless of run-time parameters. So you can explicitly place a green room, or a red room, using the same voxel data, AND, same hardpoints/metadata. (three layers then?)
		
		
		Voxel (physical) 
			-> 
			Internal Metadata Version (hardpoints in one place) 
				-> ???? /External Metadata version (texturing Green vs Red room, same hardpoints, same structure)
		
					
		--> GOOD THING: Our "super view" mode would actually work with voxel items (we already had the idea for that before!)
		
		--> GOOD: Anyone can make static assets very easily and add them.












	MagicaVoxel uses EIGHT-BIT COLOR (256 palette)
		- Good/bad idea: If we SPLIT palettes into certain sections we can EASILY allow
			things like [color cycling], as well as [team colors].
			- We can [station color scheme] (red, blue, yellow, orange) and have those colors
				change FOR MAP so different stations look different for SAME artwork.
				- Change station "paint" lines, vs station "structure" lines.
					- Blue paint for lines, artwork
					- Various greys for structural steel
					- Then, a new station might be TINTED GREEN for all metals! 
					
			- If we DON'T USE LIGHTING inside the palette, then 256 should be easy to split up.
			- OR, FUCK IT, we could HACK THE SOFTWARE and enable 4096/65,536 colors.
				- Wait, is this shit not open source?!
			
	- Who gets a palette? / palette group of colors
		- Station colors
		- Team colors
		- Color cycling (per object), and other EFFECTS like bloom? (station / map conditions may change these too... hmm.)
		-
		
	[NEW ISSUE]
		- Do we allow 3-D/screen rotation? Do we allow "a little rotation" by holding Q or E, just like with Satellite Reign?

	[NEW ISSUE]
		- How... how do you "hide" a 2-D character, inside a 3-D environment? The traditional ways kind of work. You could "fake" being a corpse (!!!), or, hide in a cabinent.
*/




/*
struct texture
	{
	ALLEGRO_BITMAP *b;
	ALLEGRO_COLOR minimap_color;
	}

And we could still move further away from the whole "allegro" dependence part.

*/


// - We COULD support some kind of AUTOMATIC false perspective construction
// for tiles. 

// - We can ALSO support NON-SQUARE movement! Diagnonals corridores that allow DIAGONAL movement! We basically have bitmask for MOVEMENT along with textures that enable "non-standard" movement.
// And in each tile_type_id, we lookup if(movement_mask) then we process it
// instead of simply just running if(blocked).   We may have issues with
// various movement types but usually it's just 0 or 1 and anything that normally
// is set to "block" is then blocked.




// GOOD RULE OF THUMB: Use (texture index) data = 0 with an obvious "missing texture" texture. (SS13 uses this in the floors.dmi/png file)

// Legend of Suffixes and Units:
//------------------------
// _px  -- pixels x,y are often used without _px suffex for bitmap iteration
// _til -- tiles. i,j are often used without _til suffex for array indexes.

// _t 	- "type", typically a class (reference) type, and it means it has functions that are meant to be called like "init()" "setup()" "draw()"
// _i	- interface
// _s	- structure. often omited for unit/primitive types that are meant to be used like integers and floats, etc. like a struct 2dpoint would just 2dpoint. 

// There may be helper functions using UFCS for any of these and don't count toward the _t's init/setup/draw() as they're actually external functions. Not sure if that really matters.

// - I've considered using Units of Measurement. But I haven't done that yet.







// Consider adding these layers to floors/walls/etc



// "Textures" - (Floor texturing layer)
// ----------------------------------------------------------------------------
// We really need to support "textures" so that you can draw a SHAPE, and then
// apply a repeating texture to it. That way we can both create "seamless non-repeating" (not on tile boundry)
// tiles, as well as, reduce the INSANE amount of manual work that developers have to do every time they
// add a new wall or floor type.


// Colorizing layer
// ----------------------------------------------------------------------------
// Not sure best way to do this, but we may just add an RGB pass.
// However, it might be "better" if we could add a mask to this process
// so we only "colorize" the pixels they want (much like oldschool palette swapping). Also, different levels of alpha (if not a bitmask) could mean
// different amounts of colorizing.

// Basically, this is useful for "teams" (red team, blue team), different "species" red golem vs black golem, and even special effects and status effects like redder = heat. bluer = cold or frozen in ice status.

// NOT SURE what we'll do if we need MULTIPLE status effects at once. The simplest method would be to average, or add, the request masking colors so we only have one pass. Don't worry about this till later though.

// DECALS and DAMAGES (floor texturing layer)
// ----------------------------------------------------------------------------
//	- Artists "repeat" their damages over and over in various permutations (if supported at all) because there's no damage overlay. (I think. Dirt works...)
//  - But, in many cases, we could use the same metal "damage" on top of many types of floor tiles (since metal is under the floor tile itself).

// We could also support various "decals"... well... what is a decal? Is it a full-frame? Or a piece?
// we'll need to delinate between bullet holes that can have a UV coordinate, and full-size decals with alpha-masks
// like say, a bar sign.

// We can attach a custom "animation function" / lambda to signs to make them
//		FLICKER alpha on top of normal graphics
//		PULSE	alpha on top of normal graphics.
//	
//
// We might want to support a bunch of default/out-of-the-box functions that support arguments/settings.



// --> ADD REFLECTIONS to GLASS including REFLECTIONS OF SPACE!!!

// Need to re-integrate / modularize the button and event handling code from the last attempt in module_inventory.d
// onscreen console stuff
// logging stuff
// - SQLite support?
// --> Basic networking spectator.
//	- 1) Simply view the world and has own free camera
//	- 2) Send updates when it changes.
//	- 3) Server enforces client camera and only sends updates nearby.





// MODES?
// - Single player / self-hosted/
// - Client
// - Host
// - Level editor??? <---

/*
	Editor Mode
	========================================================================
	
	
	Tile Mode
	------------------------------------------------------------------------	
		- Right-click for dropping second tile? (two palette brush ala MsPaint)
	
	
		+ "Pickup" palette
			- A view that SHOWS the current selected palette
	
		- Replace all X with Y in map. Change all "stone12" with "wood13"
		- Flood fill
		
		- UNDO/redo <------------------
			- What if we could do a special "partial undo" and select a mask and it only APPLIES the undo bits to those spots?
			
		- Cut/copy/paste (What will the "Empty" be set to when cut? Right-click is a background tile? Or, right-click for second tile?)

		- Mouse scroll wheel selects from a list.
		
		- Optional - Support MASKS (see above note). MASKS are like GIMP [selections] and you can move them around and paste over and over a similar pattern!
	
		
		- "Recently used list" (that doesn't change when you pick from it, only when you pick new ones, so the sort order doesn't get confusing/useless).
		
		- Manual hotlist. (Assign keys 1-9)
		
		- Select layers (even if object layer isn't doing anything yet. It's in PIXELS remember, so we'll have to keep the mouse coordinate conversation approproate to mode)
		
		- F5 - Quick-save (with text feedback!)
		- F9 - Quick-load (with text feedback!)		(only problem here is that we're wasting keys nearby.)
				(We could only do CONTROL-F5 and CONTROL-F9. And use them otherwise.)
		
		- F1/2/3/4 - Go to saved location?
		- Control-F1/2/3/4 - Save current location to F key.
	
		- Support rebinding keys.
		
	Other features:
	---------------------------------------------------------------------------
		- Highlight Modes: 
			- Description: Special highlighting modes that help diagnose problems, or setup special things.
			- Object Layer
				- Highlight powered objects (NYI, obviously)
*/

/// Extra code / refresher since I haven't touched anything in forever.












//because why stop at 65,536. Or, why not? I donno. 65,536 seems huge...
	// we could have "mods" dedicated to their own ID (bit) ranges.
	// like, one million for each namespace to prevent collisions.
	// we can also have a website script that supplies GUID upon request.
	// as well as a dedicated range for TESTING values.
	
	// but eight bytes a tile seems kind of large (though RAM is "free")
	// - except for SENDING VALUES OVER NETWORK (sending deltas would be easier? or subnet vs host ip value aware network where we change a million is simply +1). 

// ushort - 0 to 65,535	- 2 bytes
// ulong - 0 to 18,446,744,073,709,551,615 - 8 bytes!!	
// TALK ABOUT A DIFFERENCE!

// DOH, it's 2 to 8 bytes, not 2 to 4. No wonder.

// ushort 	- 2 bytes - 0 to 65,535
// uint		- 4 bytes - 0 to 4,294,967,295
// ulong 	- 8 bytes - 0 to 18,446,744,073,709,551,615	

// If we do that, we should have ALL OBJECTS, FLOORS, etc, as ONE GUID namespace.
// e.g.
// 	with 1 trillion = objects
// 	     2 trillion = floors
// 	     3 trillion = walls
// etc.
/*
	1 byte = 2^8  = 256
	2 bytes = 2^16 = 65,536
	3 bytes = 2^24 = 16,777,216
	4 bytes = 2^32 = 4,294,967,296
		   (= 2^38 = 274,877,906,944)
		   
	5 bytes = 2^40 =          1,099,511,627,776	
	6 bytes = 2^48 =        281,474,976,710,656	
    8 bytes = 2^64 = 18,446,744,073,709,551,615	
*/
// We can still do this with uints!
// 4 bytes for GUID:
// ------------------------------------
// 00000000 00000000 00000000 00000000
// 1 byte = DEVID (0 = test, 1 = me go fuck urself, 10, 100, 1000, 10000?)
// xxxxxxxx yyyyyyyy yyyyyyyy yyyyyyyy
// 		too many bits for this!
// 00000000 00000000 00000000 00000000

// There is ONE problem. If we use GUID with subnet/host masks, we have the 
// a problem! Everyone get THE SAME AMOUNT OF IDs! But a tiny developer with one item
// doesn't need a billion entries!

// We could do IPv4'ish. Where you have subnet mask with host vs whatever.
// It works like this:

/*

	Larger numbered hosts, get fewer "subnet" / value entries.
	
	10000000 00000000 00000000 00000000
	11000000 00000000 00000000 00000000
	11100000 00000000 00000000 00000000
	11110000 00000000 00000000 00000000

	Where 1 = [user/host/modder id], and 0 = [bits for objects]

	And applying it is a simple use of an AND statement and an OR statement
	to "decode" the value into two parts.
	
	We could, like ipv4, reserve certain patterns for special things like 
	wildcards/multicast or whatever. But then processing becomes a bigger deal.
*/

// Seems like original SS13 map is 256x256 tiles (tile: 32x32 pixels)
// each tile is aaa through ZZZ. so it's, (26*2)^3 = 140,608 possible values.
// which is slightly more than (17-bit) = 65536*2 = 131,072

// Though it rarely hits near that. IIRC. It starts with a map of what # = what object.
// And each object can have starting value STATE forced into it. So I think same object + different STATE
// equals different value. (Including orientation). It's not a bad system except for the text encoding.
// a 1.5 MB "BoxStation" compresses down to a mere 100Kb. Talk about a low-entropy / waste of space.

// It's also not a bad idea to refer to objects (see later, older discussion) by a NAME instead of some
// wierd quasi-ipv4 setup. And let it internally have numbers, and have the [HEADER] control the mapping.

// Hell, the idea of having a UNIQUE ID = object + state, has benefits (and cons, like you can't scan 1 
// number to get # of objects, or alter/remove all objects by type. You'd have to keep a lookup table / dictionary[list()]
// for that.) I was htinking of benefits but all a sudden I'm feeling exhausted so I can't think. Time to git commit.

// How do we handle MOUSE BEING HELD with a continious draw

// ALSO, let's treat that as a SINGLE UNDO. So when we START we make a snapshot of the original
// and NO LONGER make snapshots until we END the paint brush process. "paint stroke"

/*
	- Woah. We could merge support for STACK-BASED coordinates, into the bitmap drawing code.
		- If given a stack_pos type, instead of a pos type, it'll resolve it.
		- Necessary/useful?
*/

// TODO -> We could REPLICATE and REUSE a lot of this stuff for adding WALLS
// (and some even for sprites as-is--though I want something better for objects.)
// One point is, "okay, it's easy for you to edit a bunch of sprites at the same time."
// However, the SPRITES have nothing to do with the WALL/FLOOR data. You can
// make an entire ROW of sprites be "empty" but you still can't WALK THROUGH THEM.

// - One more note about "higher res" graphics. In a way, the higher the res, the LOWER
// the view distance which is a GOOD THING because it'll help you focus on DETAILS
// in the room, as well as NOT be able to know what's going on in the next room!



//UFCS rules

string taco(string t){return t;}
string bell(string t){return t;}

void test_ufcs()
	{
	"hello".taco().bell(); 
	// =
	taco(bell("hello")); // C / C++ 's shitty normal syntax.
	
	// ALSO, parethesis with NO arguments is same as NO PARETHENISIS
	
	"hello".taco.bell;
	}




	

class alt_test (int w, int h, T)
	{
	T [w][h] data;
	}
	
void test_alt_test()
	{
	alt_test!(10, 10, int) x;
	}




// NOT SURE YET.
/*template sizerE(MAP_SIZE s)
	{
	const char [] sizerD = AliasSeq(double, double);
	}*/	


	
class why_t ()
	{
	array_t!(64,64) case1;	// works
//	array_t!(mixin(sizer2D!())) case2; // FAILS

	array2_t!(64) case3; // works
	array2_t!(mixin(sizer1D!())) case4; // works

	array3_t!(64) case5; // works
	array3_t!(mixin(sizer2D!())) case6; // WORKS using ONE ARGUMENT method
	}
	
class array_t (int width, int height)
	{
	int [width][height] data;
	}
class array2_t (int width)
	{
	int [width][width] data;
	}
class array3_t (int width, int height=width) //default param... WORKS with one argument
	{
	int [width][height] data;
	}


template sizer2D2()
	{
	const char [] sizer2D2 = "\"[32][32]\"";
	//const char [] sizer2D2 = "\"32,32"";	
	// CAN'T USE COMMAS IN MIXINS  32,64 becomes 64. Because it's a statement.
	}	

	
void test_functionasdg()
	{
	writeln(mixin(sizer2D2!()));	
	auto x = new map2_t!(MAP_SIZE.SHIP);
	auto y = new why_t!();
	
	}











// test idea - different error return type.

// WON'T WORK. Oh well, was worth a shot. It WILL WORK as two lines, but not as one.
struct packed_return(T)
	{
	T value;
	bool error = false;
	alias value this;
	}

packed_return!int test_packed_function(bool should_this_fail)
	{
	packed_return!int data;
	data = 202;
	if(should_this_fail)
		{
		data.error = true;
		}else{
		data.error = false;
		}
	return data;
	}

void test_packed()
	{
	
	//if(int i = 1) works?!?! 


// ONE LINER won't work
//	if(  (packed_return!int ret = test_packed_function(false)).error ) // this won't work then...
	
// Two lines will, but what's the point? Just dump the data or error through a
// reference function argument if you need more data out. Oh well... :(

// That is, I mean: bool error = function(in1, in2, in3, out1, out2);


	//or auto ret = 
	packed_return!int ret = test_packed_function(true);
	if( ret.error )	
		{
		writeln(ret," ",ret.error); 
		}else{
		writeln(ret," ",ret.error);
		}
	// <-- I wonder if a mixin could make this prettier... combine the two
	// visually. ... maybe not. mixin is too long of a word/syntax.
		


	// Apparently, supposedly, we can use a COMMA to do this explicitly
	// https://issues.dlang.org/show_bug.cgi?id=10638
	// for case such as while( line = stdio.readln(), line) {}
	// and better, while( (line = stdio.readln()) is !null) {}
	
	//if(packed_return!int ret2 = test_packed_function(true), ret2)		FAILS
	//if((packed_return!int ret2 = test_packed_function(true)) == 1)	FAILS
//		{
		
	//	}



	// Otherwise, in C++, we could have overloaded the bool operator,
	// so that BOOL cases actually report the ERROR condition.
	// whereas int/float/double/string/object/etc use brings out the value
	// object.
	
	// Likewise, in C++, we can use assignment AS an expression.
	// So this would work in C++.

	}



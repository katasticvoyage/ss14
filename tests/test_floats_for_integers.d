import std.stdio;
import std.conv;

void test_range(T)(T input_type)
	{
	T last_value = 0;
	for(T d = 1.0f; d < d.max; d+=1)
		{
		if(d == last_value)
			{
			writefln("We failed to increment by 1 past (rounded back down to zero) at: %f for type [%s]", d, typeof(d).stringof);
			break;
			}
		last_value = d;
		}
	}

int main()
	{
	double d = 0;
	float  f = 0;
	
	test_range(f); // max is 16777216
	
	writefln("The next one will take a VERY LONG TIME. So consider pressing Control-C to cancel now.");
	
	test_range(d); // 2^53 = 9,007,199,254,740,992 -- this would take a very long time!
	
	return 0;
	}

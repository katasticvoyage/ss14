/*
	Molto Allegro - Library / Wrapper around DAllegro and Allegro
	
	Uses template "magic" to make everything betters.
		
		- Button handling code. (optional)
		- FPS 
		- Easy event loop setup (optional)
			(Capture events{}, react to events{}, draw.{})

		- Basic drop-down console/dialogs


		- al_draw_bitmap_region ???
		
		void al_draw_bitmap_region(ALLEGRO_BITMAP *bitmap,
   float sx, float sy, float sw, float sh, float dx, float dy, int flags)




see

	https://www.allegro.cc/manual/5/al_hold_bitmap_drawing

" While deferred bitmap drawing is enabled, the only functions that can be used are the bitmap drawing functions and font drawing functions. Changing the state such as the blending modes will result in undefined behaviour. One exception to this rule are the transformations. It is possible to set a new transformation while the drawing is held."

see (and other fameworks for ideas)

	https://github.com/MarkOates/allegro_flare
	
*/
version(DigitalMars)
	{
	pragma(lib, "dallegro5_dmd");
	}
version(LDC)
	{
	pragma(lib, "dallegro5_ldc");
	}

version(ALLEGRO_NO_PRAGMA_LIB)
	{
	}else{
	pragma(lib, "allegro");
	pragma(lib, "allegro_primitives");
	pragma(lib, "allegro_image");
	pragma(lib, "allegro_font");
	pragma(lib, "allegro_ttf");
	pragma(lib, "allegro_color");
	}
import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

import std.string;

import core.vararg;
import std.meta;
import std.traits;

import helper;

class bitmap
	{
	ALLEGRO_BITMAP *data;
	alias data this;
	}

class gfx_t
	{
	ALLEGRO_COLOR rgb(float red, float green, float blue)
		{
		return al_map_rgb_f(red,green,blue);
		}

	ALLEGRO_COLOR rgb(ubyte red, ubyte green, ubyte blue)
		{
		return al_map_rgb(red,green,blue);
		}
	ALLEGRO_COLOR rgba(float red, float green, float blue, ubyte alpha)
		{
		return al_map_rgba_f(red,green,blue,alpha);
		}

	ALLEGRO_COLOR rgba(ubyte red, ubyte green, ubyte blue, ubyte alpha)
		{
		return al_map_rgba(red,green,blue, alpha);
		}

	//from old gfx_t class
	void start_frame(){}
	void end_frame(){}
	void draw_frame(){}
	void draw_sprite(){}
	void draw_map(){}

	void initialize()
		{
		}	

	void draw_bitmap(bitmap b, float x, float y)
		{
//		al_draw_bitmap();
		}
		
	//what if we have TEMPLATES to pick which version gets called?
	//	
	//	(how to handle variable arguments?)
	void draw_scaled_bitmap(bitmap b, float x, float y, float scale_factor)
		{
		}

	void draw_2d_scaled_bitmap(bitmap b, float x, float y, float scale_x, float scale_y)
		{
		}

	void set_target(bitmap b)
		{
		}

	void reset_target()
		{
		}

	// is there an EASY way to allow easy customization without 
	// significant slowdown? ala templates.
	
	// Default arguments only partially works... and they HAVE to be on the end.
	void draw_rectangle(float x, float y, float x2, float y2, color c, float width=1.0f)
		{
		}
		
	/*
		Imagine this:
		-------------------------------------------------------
		
		draw_bitmap(
			mybitmap_bmp,
			xy(0,0), 
			); 	//draw normal

		draw_bitmap(
			mybitmap_bmp,
			xy(0,0), 
			scaled(2.0f), //now scale it 2x.
			rotated(100f),
			color(.5,.5,.5, 0.25f)
			);  
		
		- ALSO, this could be COMPILED automatically instead of RUN TIME!!!!
		
		- You pass a bunch of "bitmap_argument" TYPES with their payloads.
		
		-------------------------------------------------------
	*/
	}
	
gfx_t gfx;


// http://liballeg.org/a5docs/trunk/display.html#al_set_new_display_flags
enum SCREEN_MODE
	{
	WINDOWED = 0,
	FULLSCREEN_WINDOW,
	FULLSCREEN,
	};

bool is_maximized;
bool is_resizable;

class gfx3_t
	{
	ALLEGRO_COLOR rgb(float red, float green, float blue)
		{
		return al_map_rgb_f(red,green,blue);
		}

	ALLEGRO_COLOR rgb(ubyte red, ubyte green, ubyte blue)
		{
		return al_map_rgb(red,green,blue);
		}
	ALLEGRO_COLOR rgba(float red, float green, float blue, ubyte alpha)
		{
		return al_map_rgba_f(red,green,blue,alpha);
		}

	ALLEGRO_COLOR rgba(ubyte red, ubyte green, ubyte blue, ubyte alpha)
		{
		return al_map_rgba(red,green,blue, alpha);
		}

	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT_QUEUE *queue;
	ALLEGRO_FONT *default_font;
	
	//TEMPORARY
	this(ALLEGRO_DISPLAY *display, ALLEGRO_EVENT_QUEUE *queue, ALLEGRO_FONT *default_font)
		{
		import std.stdio;
		writeln(" - RUNNING TEMPORARY VERSION that attachs to existing system.");
		
		this.display = display;
		this.queue = queue;
		this.default_font = default_font;
		}
	
	this(int w, int h, SCREEN_MODE mode)  //any dependencies to pass, or return??
		{
		if( al_init() )	
			{
			throw new Exception("Failed to init allegro. wtf.");
			}
			
		display = al_create_display(w, h);
		if(display == null)
			{
			throw new Exception("Failed to create display!");
			}
			
		if (!al_install_keyboard())      assert(0, "al_install_keyboard failed!");
		if (!al_install_mouse())         assert(0, "al_install_mouse failed!");
		if (!al_install_joystick())         assert(0, "al_install_joystick failed!");
		if (!al_init_image_addon())      assert(0, "al_init_image_addon failed!");
		if (!al_init_font_addon())       assert(0, "al_init_font_addon failed!");
		if (!al_init_ttf_addon())        assert(0, "al_init_ttf_addon failed!");
		if (!al_init_primitives_addon()) assert(0, "al_init_primitives_addon failed!");

		al_register_event_source(queue, al_get_display_event_source(display));
		al_register_event_source(queue, al_get_keyboard_event_source());
		al_register_event_source(queue, al_get_mouse_event_source());
		al_register_event_source(queue, al_get_joystick_event_source());

		default_font = al_load_font("./data/DejaVuSans.ttf", 18, 0);

		with(ALLEGRO_BLEND_MODE)
			{
			al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
			}
		}

//0--------------------------------------------
	struct brush_do
		{
		this(bool lock_bitmap)
			{
			bool v = lock_bitmap;
			// LOAD DEFAULT BRUSH
			}
		~this()
			{
			// Needed? We could automatically LOCK and UNLOCK bitmap access!
			}
		}

// ALTERNATE METHOD for center, right_align, etc that don't have arguments.
struct abc_t{}
abc_t right_aligned; //we pass an INSTANCE instead of a TYPE
// HOWEVER, do we have to PROCESS it differently?
// and will the compiler know all the same information?
abc_t left_aligned; //we can also use the same TYPE but different INSTANCE NAMES
// to possibly reinforce some aspect of the API. Like requiring ONE "type" (alignment), 
// but not caring WHICH instance we pass. (left, center, right) and just setting the value.
// al_draw(text("hello"), pos(25,25), left_aligned); // NOTE, no parenthesis on left_aligned!
// Because things like center(), left() kind of looks strange. This would fix that.

/*
	Wait, why do I need 
		al_draw(text(""), ...)
	
	We could also do:
		al_draw_text(...)

	It doesn't look as new-age/"fancy" but is it really any worse?
	
	Also, formatting can be handled by std.format.
	
	Do **WE** need to handle variable arguments / formatting? Or throw all that in std.format?
	

	Original:

		al_draw_text(
			g.font, 
			al_map_rgb(255,255,255), 
			cursor_palette_x, 
			cursor_palette_y + 80, 
			ALLEGRO_ALIGN_CENTRE, format("Tile#%d", stored_tile).toStringz());

		
		al_draw_text(
			font(g.font), 
			color(al_map_rgb(255,255,255)), 
			pos(cursor_palette_x, cursor_palette_y + 80), 
			align_center, 
			format("Tile#%d", stored_tile).toStringz()
			);

		al_draw(
			font(g.font), 
			color(al_map_rgb(255,255,255)), 
			pos(cursor_palette_x, cursor_palette_y + 80), 
			align_center, 
			text( format("Tile#%d", stored_tile).toStringz() )
			);

	Of course one of the BIG savings is we DON'T have to keep RE-setting values
	that are already set before!

	---------------------------------------------------------------------
	
		//set values without drawing? (Or simply call al_draw without text or a bitmap object?)
		al_set() or maybe just
		 al_draw(
			font(g.font), 
			color(al_map_rgb(255,255,255)), 
			align_center, 
			);
			
		al_draw(
			pos(30, 80), 
			text( format("Tile#%d", stored_tile).toStringz() )
			);
			
		al_draw(
			pos(30, 40), 
			text( format("bitaps#%d", slkdga).toStringz() )
			);
			
		One nice thing of al_set is it's CLEAR that we're setting up defaults and you
		don't have to scan or guess if it has text/bitmap in there.
		
		
		Also,

		al_draw(string, ...)
		al_draw(bitmap, ...)
		
		IMPORTANT --> ^^^^^^^^^66
			This should already be enough. 
			We shouldn't need a text() or sprite/bitmap()!!!!
			We'll never pass a string and expect a bitmap, and vice-versa,
			ALSO, it becomes IMPOSSIBLE to specify both. Though, that can be fixed with template checking.
			
			--> Uhh, can't we move the string and bitmap around anyway? They don't HAVE to be the first 
				arguments (if that's better for you) because we can just scan for the only "string" or 
				"bitmap" argument in the list.
				
		The ONLY time this breaks down is if we have some weird function that needs something like
			source_pos AND destination_pos. (We'll need both structs then...)
			and worse, bitmap AND bitmap, which then have THE SAME TYPE unless we box them:
			
			struct source_bitmap{} and
			struct destination_bitmap{}
			
			and then call it like this:
			
			al_draw(source_bitmap(MY_BITMAP), destination_bitmap(MY_BITMAP2), pos(25,25));
			
			But man, that's pretty damn ugly / overly-expressive!
					(alias = sbmp, dbmp, solved.)
			
			But it DOES allow pass-by-name! Holy shit. I JUST REALIZED THAT'S WHAT THIS IS.
			
			It's so damn powerful and expressive!
*/

	brush_do brush_time(bool val) //can this work with auto?
		{
		return brush_do(val);
		}
	
	void test1()
		{
		with(brush_time( true )) //calls startup stuff
			{
			draw_text( pos(25,25) , text("Hi there") );
			} //calls ending stuff
	
		// OMG we could also do
		/*
		
			with( brush( locked () ) )  // "locked()" would be better if it could be "locked" (or just =true or =false)
				{
				al_draw( text("Hi there!"), pos(25,25) ); //both text and graphics calls using al_draw!
				al_draw( bitmap(tile_bmp), pos(25,25) ); //which re-uses structs like pos(25,25) !
				al_draw( bitmap(tile_bmp), pos(25,25), tint(255,128,64,255));
				}
			
			But it doesn't quite line up with say, cutting down bitmap target switching, etc.
			Does it? 
			
			And how do we easily handle subbitmaps (already I think?), or drawing from/to bitmap regions?
			
				WORST CASE, we can always just resort to USING the explicit function if our general case 
				doesn't work. The KEY HERE is making the USUAL CASE super easy and fun. And if we need an 
				EDGE CASE who cares if we have to write it out explicitly ONCE (as opposed to EVERY time.)
		*/
	
		}
//0--------------------------------------------

	void clear_to_color(ALLEGRO_COLOR c)
		{
		al_clear_to_color(c);
		}

	ALLEGRO_DISPLAY* get_current_display()
		{
		return al_get_current_display;
		}

	ALLEGRO_BITMAP* get_target_bitmap()
		{
		return al_get_target_bitmap;
		}
		
	void set_target_bitmap(ALLEGRO_BITMAP *target)
		{
		al_set_target_bitmap(target);
		}

	ALLEGRO_BITMAP *bmp_screen; //SET THIS at STARTUP
	void target_screen()
		{
//		al_set_target_bitmap(bmp_screen); THIS way, or next way?
		al_set_target_bitmap ( al_get_backbuffer( al_get_current_display() ) );
		}

		
	
	// We need to ensure this brush has either a proper DEFAULT, or,
	// REQUIRE THE USER to specify the needed parts!
	// We "could" require a initial brush as part of the with(brush(new ...)) {}
	// construct. 
	struct brush_s
		{
		ALLEGRO_COLOR color;
		float x=0;
		float y=0;
		ALLEGRO_FONT *font;
		int alignment_mode = ALLEGRO_ALIGN_LEFT;
		}
		//is there any problem with having this?
		// what about a "save default" and "restore from default"?
		// or a "with(brush){}" setup that starts from default, gets modified, and leaves data after that.
		// and next "with(brush){}" starts with defaults again.
		
	brush_s brush;
		
	//BOTH text and bitmap attempt
	// --------------------------------------------------------------------
	void draw2(A...)(A a)
		if( anySatisfy!(isa!(pos), typeof(a)) &&		    
		    (
		    anySatisfy!(isa!(text), typeof(a) )
		    || 
		    anySatisfy!(isa!(bmp), typeof(a) )
		    )
		    
			)
		{
		enum hasText = anySatisfy!(isa!(bmp), typeof(a));	
		enum hasBitmap = anySatisfy!(isa!(bmp), typeof(a));	
	
		}

		
	// JUST bitmaps
	// --------------------------------------------------------------------
	/*
	
		Usage
		
		draw3(
			pos(23,23),
			tint(1.0,1.0,1.0),			or col3, col4
			tint4(1.0,1.0,1.0,0.5),
			bmp(bmp_taco),
			rotate(30),
			);
			
		available attributes:
			pos
			pos3 - ?
			tint
			bmp
			rotate - ? angle? 
			flip - ? v/h?
	
	*/
	void draw3(A...)(A a)
		if( anySatisfy!(isa(bmp), typeof(a)) )
		{
		enum hasBitmap 				= anySatisfy!(isa!(ALLEGRO_BITMAP), typeof(a));
		enum hasPosition 			= anySatisfy!(isa!(pos), typeof(a));	
		enum hasColor 				= anySatisfy!(isa!(color), typeof(a));
		enum hasColor4 				= anySatisfy!(isa!(color4), typeof(a));
		enum hasRotation 			= anySatisfy!(isa!(rotate), typeof(a));
		enum hasScaling 			= anySatisfy!(isa!(scale), typeof(a));
		enum hasAnchor 				= anySatisfy!(isa!(anchor), typeof(a));
		enum is_flipped_horizontal	= anySatisfy!(isa!(flip_horizontal), typeof(a));
		enum is_flipped_vertical 	= anySatisfy!(isa!(flip_vertical), typeof(a));
			
		static if(!hasText)
			{
			assert(0, "No bitmap?!?!?");
			}
			
		static if(hasPosition)
			{
			brush.x = a[staticIndexOf!(pos, typeof(a))].x;
			brush.y = a[staticIndexOf!(pos, typeof(a))].y;			
			}
			
		static if(hasColor)
			{
			alias c=a[staticIndexOf!(color, typeof(a))];
			brush.color = al_map_rgb(c.r, c.g, c.b);
			}
			
		static if(hasColor4)
			{
			alias c=a[staticIndexOf!(color4, typeof(a))];
			brush.color = al_map_rgb(c.r, c.g, c.b, c.a);
			}

		int FLIPPING_MODE = 0;
		static if(is_flipped_horizontal && !is_flipped_vertical)
			{
			FLIPPING_MODE = ALLEGRO_FLIP_HORIZONTAL;
			}
		static if(is_flipped_vertical  && !is_flipped_horizontal)
			{
			FLIPPING_MODE = ALLEGRO_FLIP_VERTICAL;
			}
		static if(is_flipped_vertical  && is_flipped_horizontal)
			{
			FLIPPING_MODE = ALLEGRO_FLIP_HORIZONTAL & ALLEGRO_FLIP_VERTICAL;
			}
		
		//--------------------------------------------------------------------
		
		if(!has_rotation && !has_scaling && !hasColor)
			{
			al_draw_bitmap(brush.bmp, brush.x, brush.y, FLIPPING_MODE);
			}
		if(has_rotation && !has_scaling && !hasColor)
			{
			al_draw_rotated_bitmap(brush.bmp, brush.x, brush.y, FLIPPING_MODE);
			}
		if(has_rotation && has_scaling && !hasColor)
			{
			al_draw_scaled_rotated_bitmap(brush.bmp, brush.x, brush.y, FLIPPING_MODE);
			}


		if(!has_rotation && !has_scaling && hasColor)
			{
			al_draw_tinted_bitmap(brush.bmp, brush.x, brush.y, FLIPPING_MODE);
			}
		if(has_rotation && !has_scaling && hasColor)
			{
			al_draw_tinted_rotated_bitmap(brush.bmp, brush.x, brush.y, FLIPPING_MODE);
			}
		if(has_rotation && has_scaling && hasColor)
			{
/*
			al_draw_tinted_scaled_rotated_bitmap(ALLEGRO_BITMAP *bitmap,
   ALLEGRO_COLOR tint,
   float cx, float cy, float dx, float dy, float xscale, float yscale,
   float angle, int flags)	
	*/		
			alias scale_x=a[staticIndexOf!(scale, typeof(a))];
			alias scale_y=a[staticIndexOf!(scale, typeof(a))];
			al_draw_tinted_scaled_rotated_bitmap(brush.bmp, 0, 0, brush.x, brush.y, scale_x, scale_y, FLIPPING_MODE);
			
			}

		}
		
	// Re-imaginging al_draw_text
	// --------------------------------------------------------------------
	void draw_text(A...)(A a)
	// This should FAIL (compile time) BEFORE making the static/asserts (run time)
		if( anySatisfy!(isa!(pos), typeof(a)) &&
		    anySatisfy!(isa!(text), typeof(a)) ) 
		{
		enum hasPosition 			= anySatisfy!(isa!(pos), typeof(a));	
		enum hasFont 				= anySatisfy!(isa!(font), typeof(a));
		enum hasColor 				= anySatisfy!(isa!(color), typeof(a));
		enum hasColor4 				= anySatisfy!(isa!(color4), typeof(a));
		enum isCentered 			= anySatisfy!(isa!(center), typeof(a));
		enum isRightAligned 		= anySatisfy!(isa!(align_right), typeof(a));
		enum hasText 				= anySatisfy!(isa!(text), typeof(a));

		static if(!hasText)
			{
			assert(0, "No text?!?!?");
			}

		static if (!hasPosition) //We can RE-USE the struct for al_draw_bitmap (which is NICE! for remembering.)
			{
			assert(0, "Needs a position argument!"); //Does it? Usually...
			}
			/* POSSIBLE BUG?! Or logic error?
			else{
			brush.x = a[staticIndexOf!(pos, typeof(a))].x;//extra.d(638): Warning: statement is not reachable
			brush.y = a[staticIndexOf!(pos, typeof(a))].y;//extra.d(639): Warning: statement is not reachable
			} 	
			*/
		
		static if (hasPosition) 
			{
			brush.x = a[staticIndexOf!(pos, typeof(a))].x;
			brush.y = a[staticIndexOf!(pos, typeof(a))].y;
			}

		static if(hasColor)
			{
			alias c=a[staticIndexOf!(color, typeof(a))];
			brush.color = al_map_rgb(c.r, c.g, c.b);
			}
		static if(hasColor4)
			{
			alias c=a[staticIndexOf!(color4, typeof(a))];
			brush.color = al_map_rgb(c.r, c.g, c.b, c.a);
			}

		static if(hasFont)
			{
			brush.font = a[staticIndexOf!(font, typeof(a))].font;
			}

		static if (isCentered)
			{
			brush.alignment_mode = ALLEGRO_ALIGN_CENTRE;
			}

		static if (isRightAligned)
			{
			brush.alignment_mode = ALLEGRO_ALIGN_RIGHT;
			}

		al_draw_text(
			brush.font, 
			brush.color, 
			brush.x, 
			brush.y, 
			brush.alignment_mode,
			a[staticIndexOf!(text, typeof(a))].data.toStringz()
			);
		}
	}

void funct2(A...)(ALLEGRO_BITMAP *bit, A a)
	{
	enum hasPosition 			= anySatisfy!(isa!(pos), typeof(a));
	enum hasCenteredPosition 	= anySatisfy!(isa!(center), typeof(a)); 
	enum hasRotate 				= anySatisfy!(isa!(rotate), typeof(a));
	enum hasScale 				= anySatisfy!(isa!(scale), typeof(a));

	// Deal with FLIPPING MODE
	// ------------------------------------------------------------------------
	enum flip_h = anySatisfy!(isa!(flip_horizontal), typeof(a));
	enum flip_v = anySatisfy!(isa!(flip_vertical), typeof(a));

	static if (!flip_h && !flip_v)
		int flipping_mode = 0;
	static if (flip_h && flip_v) //both
		int flipping_mode = ALLEGRO_FLIP_HORIZONTAL & ALLEGRO_FLIP_VERTICAL;
	static if (!flip_h && flip_v) //vertical
		int flipping_mode = ALLEGRO_FLIP_VERTICAL;
	static if (flip_h && !flip_v) //horizontal
		int flipping_mode = ALLEGRO_FLIP_HORIZONTAL;

	// Need a minimum of position.
	// ------------------------------------------------------------------------
    static if (!hasPosition)
		{
		assert(0, "Need to pass a position!");
		// OH SNAP, this will only fail WHEN IT RUNS as opposed to compile time!
		// no wonder they offered to change it!
		}
    
	// Deal with CENTERING
	// ------------------------------------------------------------------------
    static if (hasCenteredPosition)
		{
		float temp_x=a[staticIndexOf!(pos, typeof(a))].x - bit.w/2;
		float temp_y=a[staticIndexOf!(pos, typeof(a))].y - bit.h/2;
		}else{
		float temp_x=a[staticIndexOf!(pos, typeof(a))].x;
		float temp_y=a[staticIndexOf!(pos, typeof(a))].y;
		}
    
    static if (hasPosition && !hasRotate && !hasScale) 
		{
        // Stuff
		pragma(msg, "Class 1");
		writeln("x ", a[staticIndexOf!(pos, typeof(a))].x); //how do we tell the INDEXOF? probably use IndexOf
		writeln("y ", a[staticIndexOf!(pos, typeof(a))].y); //how do we tell the INDEXOF? probably use IndexOf
		
		al_draw_bitmap(bit, 
			temp_x,
			temp_y,
			flipping_mode);
		}

    static if (hasPosition && hasRotate && !hasScale) 
		{
        // Stuff
		pragma(msg, "Class 2");
		writeln("x ", a[staticIndexOf!(pos, typeof(a))].x); //how do we tell the INDEXOF? probably use IndexOf
		writeln("y ", a[staticIndexOf!(pos, typeof(a))].y); //how do we tell the INDEXOF? probably use IndexOf
		
		al_draw_rotated_bitmap(bit, 
			bit.w/2,
			bit.h/2,
			temp_x,
			temp_y,
			a[staticIndexOf!(rotate, typeof(a))].a, 
			flipping_mode);
		}


    static if (hasPosition && !hasRotate && hasScale) 
		{
		pragma(msg, "Class 3 - Scaled");
		al_draw_scaled_rotated_bitmap(
			bit,
			bit.w/2, 
			bit.h/2, 
			temp_x,
			temp_y,
			a[staticIndexOf!(scale, typeof(a))].f,
			a[staticIndexOf!(scale, typeof(a))].f,
			0, 
			flipping_mode);
		}

    static if (hasPosition && hasRotate && hasScale) 
		{
		pragma(msg, "Class 4 - Scaled Rotated");

		al_draw_scaled_rotated_bitmap(
			bit,
			bit.w/2, 
			bit.h/2, 
			temp_x, //a[staticIndexOf!(pos, typeof(a))].x,
			temp_y, //a[staticIndexOf!(pos, typeof(a))].y,
			a[staticIndexOf!(scale, typeof(a))].f,
			a[staticIndexOf!(scale, typeof(a))].f,
			a[staticIndexOf!(rotate, typeof(a))].a, 
			flipping_mode);
		}

	}
    //is(typeof(a) == pos)

void funct(A...)(ALLEGRO_BITMAP *bitmap_bmp, A a)
	{
	bool has_position=false;	
	bool has_rotation=false;	
	bool has_scale=false;
	bool has_color=false;
	bool flip_horizontally=false;	
	bool flip_vertically=false;	

	float x=0;
	float y=0;

	writeln("----------------------------------");
	static foreach(b; a) //OMFG DIP1010 (LDC 1.8.0 supports this!!!!!)
		{
		writeln(b);
		
		static if(is(typeof(b) == pos))
			{
			writeln(" - x - ",b.x); // OMFG. OMFG!!!!!!!!!!!!
			writeln(" - y - ",b.y);
			has_position=true; 
			x = b.x;
			y = b.y;
			}

		static if(is(typeof(b) == rotate))
			{
			writeln(" - a - ",b.a); // OMFG. OMFG!!!!!!!!!!!!
			has_rotation=true;
			}
		static if(is(typeof(b) == scale))
			{
			writeln(" - f - ",b.f); // OMFG. OMFG!!!!!!!!!!!!
			has_scale=true; 
			}

		}
		
	assert(has_position == true, "You need to specify a position!");

//	int flipping_mode = 0;
//	static if(flip_horizontally) {flipping_mode = ALLEGRO_FLIP_HORIZONTAL;}
//	static if(flip_vertically) {flipping_mode = ALLEGRO_FLIP_VERTICAL;}

	static if(has_position)// && !has_rotation && !has_scale && !has_color)
		{
		al_draw_bitmap(bitmap_bmp, 0, 0, flipping_mode);
		}
		
writeln("----------------------------------");
/*	
	static if (a.length)
		{
	writeln(a[0]);


		
		//instead, why not do some kind of static foreach???
		
		
		static if(is(typeof(a[0]) == pos))
			{
			writeln(" - ",a[0].x); // OMFG. OMFG!!!!!!!!!!!!
			writeln(" - ",a[0].y);
			has_position=true; // wait, how does this backpropogate during the recursion?
			}

		static if(is(typeof(a[0]) == rotate))
			{
			writeln(" - ",a[0].a); // OMFG. OMFG!!!!!!!!!!!!
			has_rotation=true;// wait, how does this backpropogate during the recursion?
			}
		static if(is(typeof(a[0]) == scale))
			{
			writeln(" - ",a[0].f); // OMFG. OMFG!!!!!!!!!!!!
			has_scale=true; // wait, how does this backpropogate during the recursion?
			}

		
        static if (a.length > 1)
            funct(a[1 .. $]);
		}*/
	}
	
//----------------------------------------------------------------


void test_inst()
	{
	//funct(g.tile_bmp, pos(100,100), scale(2), rotate(0.0f));
	ALLEGRO_BITMAP *test_bmp; //fixme
	assert(test_bmp != null, "YOU FORGOT TO FIXME.");
	funct2(test_bmp, pos(100,150), scale(1), rotate(0.0f));
	//funct2(g.tile_bmp);
	//funct2(123);
	}


// f2(scale, 2.0, pos, 1.0, 1.0)  <-- not as nice. You could put the wrong number of arguments in.
void f2(A...)(A a)
	{
	static if (a.length)
		{
        writeln(a[0]);
		static if (a.length > 1)f2(a[1 .. $]);
		}
	}

template isa(T){enum isa(U) = is(U == T); }
template isa2(T) { enum isa2(U) = is(U == typeof(T));  }

// Could we use these for "markers" instead of align_left(true), or align_left() just align_left?
//-------------------------------------------------------------------------------
enum align_left2=true;

void test_funct2()
	{
	ALLEGRO_BITMAP *abc = al_create_bitmap(320,240);
	funct4(abc, pos(25,25), align_left2);
	}

void funct4(A...)(ALLEGRO_BITMAP *bit, A a)
	{
	enum hasPosition = anySatisfy!(isa!(pos), typeof(a));
	
	enum has_align_left2 = 
		anySatisfy!(
			isa!(typeof(align_left2)), typeof(a)  //can we clean this up with a better isa! template?
			);

    static if (!hasPosition)
		{
		}
    static if (hasPosition)
		{
		pragma(msg, "hello");
		}
    static if (has_align_left2) //OMGAWD IT WORKS.
		{
		pragma(msg, "hello -- it matches!");
		}
	}

//-------------------------------------------------------------------------------


//classes don't work, but structs do!
// <------------
struct pos { float x, y; }
struct center { float x,y; }
struct scale { float f; }
struct rotate{ float a; }
struct anchor { float x, y; } //anchor point for scaling/rotating/whatever, otherwise 0,0 (or center?)


     
struct flip_horizontal{}
struct flip_vertical{}
struct aspect_ratio{} // ? For doing non-even scaling (2X horizontal, 1.5X vertical)??
struct scale_horizontal{} // ditto?
struct scale_vertical{} // ditto?
struct stretch{} // What if STRETCH or DISTORT means scale independantly?
//struct scale{} //with this being BOTH?

//void al_draw_text(const ALLEGRO_FONT *font,
//   ALLEGRO_COLOR color, float x, float y, int flags,
//   char const *text)

// Automatically convert strings to c-strings!



// - could we have a "brush" version, where if you don't specify a color, 
// it remembers the LAST color???
// If this is in a CLASS we can store the data there.
struct font //NOTE name class issue. HOW TO DEAL? (well, g.font is different than class/struct font...)
	{
	ALLEGRO_FONT *font;
	}
		
struct align_right
	{
	}


// is there any way to ALIAS these together? Like col and col3 = color?
alias col3=color;
alias col=color; // NOT SURE we want aliases.
struct color		//note, the most general case doesn't include a suffix.
	{
	float r,g,b;
	}

struct color4
	{
	float r,g,b,a;
	}

struct pos3 //not used anywhere, but FYI (likewise, pos is 2-D, vs pos3, pos4 etc)
	{
	float x,y,z;
	}

struct text
	{
	string data;
	}

struct bmp
	{
	ALLEGRO_BITMAP b;
	}

// ------------------------------------------------------------------------


/// Penis butt
class gfx2_t
	{
	//Support links
	ALLEGRO_EVENT_QUEUE *queue;

	@disable this(); //and disable copy constructor! whatever one that is.
	//@disable this(this); //i think this is it. 
	// this fails with Error: postblit can only be a member of struct/union, not class gfx2_t

	this(int width, int height, bool is_windowed, ALLEGRO_EVENT_QUEUE *queue) //what else?
		{
		//setup allegroooooo
	

		//queue forces us to setup a queue before hand. It could also be
		// our way to syncronize drawing rates, vsync, reporting frame_rate, etc.
		assert(minimap_temp_bmp == null, "Don't recreate me!");
		minimap_temp_bmp = al_create_bitmap(1,1);
		}

	bool has_called_init=false;  //in a way, why even let anyone MAKE a gfx without setting it up!!!
	ALLEGRO_BITMAP* [] targets;
	ALLEGRO_BITMAP* minimap_temp_bmp;
	
	void push_target_bitmap(ALLEGRO_BITMAP *t)
		{
		targets ~= t;
		al_set_target_bitmap(t);
		}

	void pop_target_bitmap()
		{
		import std.range.primitives;
		targets.popBack();
		al_set_target_bitmap(targets[$]);
		}

	bool has_started_minify=false;

	struct frame_do
		{
		this(int value)
			{
			int v = value;
			//frame/drawing start calls
			}
		~this()
			{
			//frame/drawing end cleanup calls
			}
		}

	frame_do frame(int val) //can this work with auto?
		{
		return frame_do(val);
		}
	
	void test1()
		{
		with(frame(15)) //calls startup stuff
			{
			} //calls ending stuff
		}
		
	void start_minify()
		{
		push_target_bitmap(minimap_temp_bmp);
		has_started_minify=true;
		}

	void end_minify()
		{
		pop_target_bitmap();
		has_started_minify=false;
		//is there any way, to ensure this gets called? 
		// how about we do something instead where you "build" 
		// a scope that auto exits?
		/*
			start_minify!()
				{
				do your stuff
				}//auto closes here.
		*/
		}

	// turns a tile texture into a color (single pixel) for use in minimaps
	ALLEGRO_COLOR minify(ALLEGRO_BITMAP *b)
		{
		assert(has_started_minify == true, "START MINIFY FIRST.");
		
		al_draw_scaled_bitmap(b,
					0,
					0,
					b.w, 
					b.h,
					 
					0,
					0,
					1.0f,  //scale it down to one pixel!
					1.0f,
					0);

		ALLEGRO_COLOR c = al_get_pixel(minimap_temp_bmp, 0, 0);
		return c;	
		}
	}




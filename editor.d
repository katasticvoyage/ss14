import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

import common;
import object_t;
import helper;
import std.string : toStringz;
import molto;

// WHY IS THIS CLASS EMPTY?!? The CURSOR does everything!
// Should we merge the two?! 
class editor_dialog_t
	{
	float x,y,w,h;
	ALLEGRO_COLOR c;
	ALLEGRO_COLOR c2; //border
	int border_radius = 5;
	int border_thickness;
	string window_title;
	
//	string data_to_draw; // MAYBE? But what about graphics, etc?
	// we could have a DELEGATE we pass that tells it what to draw.
	// but the delegate will need to take arguments of where to draw. (x,y and (w,h)?)
	
	void test_function(){}
	
	void delegate(viewport_t v) task2;
	
	void draw_tile_palette(viewport_t v, LAYER layer)
		{
		import std.format;
		import std.stdio;
		int stored_tile=g.editor.cursor.stored_tile; //ENCAPSULATION LEAK
		
		ALLEGRO_BITMAP *bmp;
		if(layer == LAYER.FLOOR)
				{
				 //ENCAPSULATION LEAK
				if( g.editor.cursor.stored_tile >= g.world.atlas.tiles_bmp.length) 
					{g.editor.cursor.stored_tile = cast(int)g.world.atlas.tiles_bmp.length-1;
																stored_tile =  g.editor.cursor.stored_tile;
																 writeln("ASSERT FAIL. fixed tile number. #1");}
					
				bmp = g.world.atlas.tiles_bmp[stored_tile];
				}
		if(layer == LAYER.WALL)
				{
				 //ENCAPSULATION LEAK
				if( g.editor.cursor.stored_tile >= g.world.atlas2.tiles_bmp.length) 
				{g.editor.cursor.stored_tile = cast(int)g.world.atlas2.tiles_bmp.length-1;
										stored_tile =  g.editor.cursor.stored_tile;

					 writeln("ASSERT FAIL. fixed tile number. #2");}

				bmp = g.world.atlas2.tiles_bmp[stored_tile];
				}
		if(layer == LAYER.HULL)
				{
				 //ENCAPSULATION LEAK
				if( g.editor.cursor.stored_tile >= g.world.atlas_hull.tiles_bmp.length) 
					{
					writeln("ASSERT FAIL. fixed tile number. #3");
					writeln("g.editor.cursor.stored_tile: ", g.editor.cursor.stored_tile);
					g.editor.cursor.stored_tile = cast(int)g.world.atlas_hull.tiles_bmp.length-1;
					stored_tile =  g.editor.cursor.stored_tile;
					writeln("cast(int)g.world.atlas_hull.tiles_bmp.length-2: ", cast(int)g.world.atlas_hull.tiles_bmp.length-2);
					}
				bmp = g.world.atlas_hull.tiles_bmp[stored_tile];
				}
		if(layer == LAYER.DECAL)
				{
				 //ENCAPSULATION LEAK
				if( g.editor.cursor.stored_tile >= g.world.atlas_decal.tiles_bmp.length) 
				{g.editor.cursor.stored_tile = cast(int)g.world.atlas_decal.tiles_bmp.length-1; 
															stored_tile =  g.editor.cursor.stored_tile;
			writeln("ASSERT FAIL. fixed tile number. #4");}
				bmp = g.world.atlas_decal.tiles_bmp[stored_tile];
				}

		al_draw_scaled_rotated_bitmap(bmp,
				bmp.w/2, 
				bmp.h/2, 
				x-1 + 0.5f + 100, 
				y-1 + 0.5f + 32 + 100, 
				2.0, 2.0,
				0, 0);

		{
		al_draw_rectangle(
			x-1 + 0.5f + 15, 
			y-1 + 0.5f + 40, 
			x+bmp.w + 0.5f + 15, 
			y+bmp.h + 0.5f + 40, 
			gfx.rgb(255,0,0), 1.0f);
		
		al_draw_bitmap(bmp, x + 15, y + 40, 0);
		}
		
		al_draw_text(
			g.font, 
			gfx.rgb(255,255,255), 
			x + 100, 
			y + 40, 
			ALLEGRO_ALIGN_CENTRE, format("Tile#%d", stored_tile).toStringz());

//		if(layer == LAYER.FLOOR)
			{
			al_draw_text(
				g.font, 
				gfx.rgb(255,128,128), 
				x + 100, 
				y + 60, 
				ALLEGRO_ALIGN_CENTRE, 
				g.world.maps[0].layers[ g.editor.cursor.selected_layer  ].name.toStringz()
				);

			}
		}
	
	this()
		{
		window_title = "Test Dialog";
		x = 300;
		y = 250;
		w = 200;
		h = 200;
		c = COLOR.BLACK;
		c = COLOR.RED;
		}
	
	this(string name, float x, float y, float w, float h, void delegate(viewport_t v) task)
		{
		window_title = name;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		c = COLOR.BLACK;
		c = COLOR.RED;

		task2 = task;
		}
	
	void draw(viewport_t v)
		{
	// Inside filling
	// -------------------------
		al_draw_filled_rounded_rectangle(
			x,
			y, 
			x+w,
			y+h,
			border_radius, border_radius, al_map_rgba(5,5,23,192));
   
    // Border
	// -------------------------
		al_draw_rounded_rectangle(
			x,
			y, 
			x+w,
			y+h,
			border_radius, border_radius, al_map_rgb(255,255,255), border_thickness);
   
	// Dialog Text
	// -------------------------
		al_draw_textf(g.font, al_map_rgb(255,255,255), 
			x + (w / 2), 
			y + 5, ALLEGRO_ALIGN_CENTRE, window_title.toStringz());
		al_draw_line(x, y + 30, x + w, y + 30,
		al_map_rgb(255,255,255), 1);
		
	// -------------------------
		task2(v);
		
		
		draw_tile_palette(v, g.editor.cursor.selected_layer);
// test following the mouse around. (so we can see if elements are bound to the right coordinates)
//		x = g.mouse.x;
//		y = g.mouse.y;
		}
		
	}







//=============================================================================
struct selection_s //needs _s???
	{
	int x, y;
	int w, h; 
	//works with rectangular selections but what if we do other selections?
	
	void draw(viewport_t v)
		{
		v=v;//fuck dscanner
		}
	}

enum BRUSH_MODE
	{
	PIXEL, //single cell
	BRUSH, //click-and-drag functionality (also add option to change BRUSH SIZE?)
	FILL,
	REPLACE, //replace all 'A' with 'B'
	SELECT, //if select, see next enum
	}
	
enum SELECT_MODE
	{
	RECTANGLE,
	FILL,
	REPLACE, //all of a type (like above, but without replacing)
	MOVE, //maybe? drag the selection around.
	} //also, use control/alt? to add/remove? selections ala photoshop/gimp/etc

struct cursor_s
	{
	//TODO
	//----------------------------------------------------------------
	void on_press(){} //for handling click-drag
	void on_relased(){}
	void on_tick(){}
	void adjust_brush_size(int size){}
	//----------------------------------------------------------------
	int x=0, y=0;
	int stored_tile = 0;
	LAYER selected_layer = LAYER.FLOOR; //or does CURSOR know what layer it's on?
	
	void draw(viewport_t v)
		{
		draw_cursor(v);
		}

	int cursor_palette_x = 345;
	int cursor_palette_y = 150;

	void draw_cursor(viewport_t v)
		{
		al_draw_rectangle(
			x*g.TILE_D - v.offset_x, 
			y*g.TILE_D - v.offset_y,
			x*g.TILE_D - v.offset_x + g.TILE_D-1, 
			y*g.TILE_D - v.offset_y + g.TILE_D-1,
			gfx.rgb(255,0,0), 
			2.5); 
		}


//Editor / EFFECTS (move into editor!)
// -------------------------------------------------	
	
	// NEEDS LAYER MODE SELECTOR
	void change_all_tile_to_tile(int from_tile_id, int to_tile_id)
		{
		for(int j = 0; j < g.world.maps[0].height; j++)
		for(int i = 0; i < g.world.maps[0].width; i++)
				{
				if(g.world.maps[0].layers[0].data[i][j] == from_tile_id)
					{
					g.world.maps[0].layers[0].data[i][j] = to_tile_id;
					}
				}
		}



	pos select_tl;
	pos select_br;

	void set_selection_top_left(pos val)
		{
		select_tl = val;
		}

	void set_selection_bottom_right(pos val)
		{
		select_br = val;
		}
		
	void draw_selection()
		{
		//TODO use molto gfx3_t
		al_draw_rectangle(
			select_tl.x, 
			select_tl.y, 
			select_br.x, 
			select_br.y, 
			al_map_rgba_f(1.0,1.0,1.0,1.0f), 
			1.0f);
		}


	void copy_selection()
		{
		if(selected_layer == LAYER.FLOOR)
			{
			}
		}






	
	void flood_fill() //start a floodfill
		{
		if(selected_layer == LAYER.FLOOR)
			{
			int original_tile = g.world.maps[0].layers[0].data[x][y];
			flood_fill_at(x, y, stored_tile, original_tile);
			}
			
		if(selected_layer == LAYER.WALL)
			{			
			int original_tile = g.world.maps[0].layers[1].data[x][y];
			flood_fill_at(x, y, stored_tile, original_tile);			
			}

		if(selected_layer == LAYER.HULL)
			{
			int original_tile = g.world.maps[0].layers[2].data[x][y];
			flood_fill_at(x, y, stored_tile, original_tile);
			}
			
		if(selected_layer == LAYER.DECAL)
			{			
			int original_tile = g.world.maps[0].layers[3].data[x][y];
			flood_fill_at(x, y, stored_tile, original_tile);			
			}

		}

	void flood_fill_at(int x, int y, int new_tile, int original_tile)
		{
		g.world.maps[0].minimap.invalidate();
	//	writefln("%d %d %d %d", x, y, new_tile, original_tile);
		if(new_tile == original_tile) return;

		int map_w=g.world.maps[0].width;
		int map_h=g.world.maps[0].height;

		if(selected_layer == LAYER.FLOOR)
			{
			if(g.world.maps[0].layers[0].data[x][y] == original_tile)
				{
	//			writefln("checking %d %d", x, y); 
				g.world.maps[0].layers[0].data[x][y] = new_tile;
				if(x > 0      )flood_fill_at(x-1, y  , new_tile, original_tile);
				if(x < map_w-1)flood_fill_at(x+1, y  , new_tile, original_tile);
				if(y > 0      )flood_fill_at(x  , y-1, new_tile, original_tile);
				if(y < map_h-1)flood_fill_at(x  , y+1, new_tile, original_tile);
				}else{
				//let nodes die
				}
			}

		if(selected_layer == LAYER.WALL)
			{	
			if(g.world.maps[0].layers[1].data[x][y] == original_tile)
				{
	//			writefln("checking %d %d", x, y); 
				g.world.maps[0].layers[1].data[x][y] = new_tile;
				if(x > 0      )flood_fill_at(x-1, y  , new_tile, original_tile);
				if(x < map_w-1)flood_fill_at(x+1, y  , new_tile, original_tile);
				if(y > 0      )flood_fill_at(x  , y-1, new_tile, original_tile);
				if(y < map_h-1)flood_fill_at(x  , y+1, new_tile, original_tile);
				}else{
				//let nodes die
				}
			}


		if(selected_layer == LAYER.HULL)
			{	
			if(g.world.maps[0].layers[2].data[x][y] == original_tile)
				{
	//			writefln("checking %d %d", x, y); 
				g.world.maps[0].layers[2].data[x][y] = new_tile;
				if(x > 0      )flood_fill_at(x-1, y  , new_tile, original_tile);
				if(x < map_w-1)flood_fill_at(x+1, y  , new_tile, original_tile);
				if(y > 0      )flood_fill_at(x  , y-1, new_tile, original_tile);
				if(y < map_h-1)flood_fill_at(x  , y+1, new_tile, original_tile);
				}else{
				//let nodes die
				}
			}


		if(selected_layer == LAYER.DECAL)
			{	
			if(g.world.maps[0].layers[3].data[x][y] == original_tile)
				{
	//			writefln("checking %d %d", x, y); 
				g.world.maps[0].layers[3].data[x][y] = new_tile;
				if(x > 0      )flood_fill_at(x-1, y  , new_tile, original_tile);
				if(x < map_w-1)flood_fill_at(x+1, y  , new_tile, original_tile);
				if(y > 0      )flood_fill_at(x  , y-1, new_tile, original_tile);
				if(y < map_h-1)flood_fill_at(x  , y+1, new_tile, original_tile);
				}else{
				//let nodes die
				}
			}

	}
	
	
	void set_layer(LAYER l)
		{
		selected_layer = l;
		if(l == LAYER.FLOOR)
			{
//			if(stored_tile > 99)stored_tile = 99;
			}
		if(l == LAYER.WALL)
			{
			if(stored_tile > (10*10)-1)stored_tile = (10*10)-1;
			}
		if(l == LAYER.HULL)
			{
			if(stored_tile > (5*5) - 1)stored_tile = (5*5) - 1;
			}
		if(l == LAYER.DECAL)
			{
			if(stored_tile > (15*15) - 1)stored_tile = (15*15) - 1;
			}
		}
	
	void grab_tile()
		{
		if(selected_layer == LAYER.FLOOR)
			{
			stored_tile = g.world.maps[0].layers[0].data[x][y];
			}
		if(selected_layer == LAYER.WALL)
			{
			stored_tile = g.world.maps[0].layers[1].data[x][y];
			}
		if(selected_layer == LAYER.HULL)
			{
			stored_tile = g.world.maps[0].layers[2].data[x][y];
			}
		if(selected_layer == LAYER.DECAL)
			{
			stored_tile = g.world.maps[0].layers[3].data[x][y];
			}
		}

	void place_tile()
		{
		g.world.maps[0].set(x, y, selected_layer, stored_tile); //Always use setter because it auto invalidates minimap!
		}
	
	void next_tile()
		{
		if(selected_layer == LAYER.FLOOR)
			{
			if( stored_tile < 900-1)stored_tile++;
			}
		if(selected_layer == LAYER.WALL)
			{
			if( stored_tile < 100-1)stored_tile++;
			}
		if(selected_layer == LAYER.HULL)
			{
			if( stored_tile < (15*15)-1)stored_tile++;
			}
		if(selected_layer == LAYER.DECAL)
			{
			if( stored_tile < (15*15)-1)stored_tile++;
			}
		}
	
	void previous_tile()
		{
		if( stored_tile > 0)stored_tile--;
		}
		
	void set_tile_at_cursor(int tile_id)
		{
		g.world.maps[0].set(x, y, selected_layer, tile_id);
		}

// MOVEMENT / SELECTION of cursor itself!		
// -------------------------------------------------	
	void move_to(int _x, int _y)
		{
		x = _x;
		y = _y;
		cap(x, 0, g.world.maps[0].width-1); //FIXME hardcoded
		cap(y, 0, g.world.maps[0].height-1);
		}
		
	void move_rel(int _x_rel, int  _y_rel)
		{
		x += _x_rel;
		y += _y_rel;
		cap(x, 0, g.world.maps[0].width-1); //FIXME hardcoded
		cap(y, 0, g.world.maps[0].height-1);
		}
	}

class editor_t
	{
 	
	int cursor_mode_or_layer; //object layer, pipe layer, floor layer, wall layer, etc.
	cursor_s cursor;
	
	editor_dialog_t cursor_preview_dialog;
	
	this()
		{
		cursor_preview_dialog = new editor_dialog_t("Palette Viewer", 250, 150, 200, 200, 
			delegate void (viewport_t v) { g.dm.draw_text(v, "hello world"); }
			);
			
			// HOW TO DO THIS?
		}
	
	void draw(viewport_t v)
		{
		cursor_preview_dialog.draw(v);
		}
	
//	void set_tile(int x, int y, int tile_type){}
//	int pickup_tile(int x, int y){return 0;}
//	void fill_tiles(int x, int y){}
	}

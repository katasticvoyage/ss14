import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

import common;
import helper;
import object_t;
import molto;

import animation;

import std.conv;
import std.stdio;
import std.string;

struct tile_layer
	{
	string name;
	int [MAX_MAP_W][MAX_MAP_H] data;
	}

class map_t 
	{
	minimap_t minimap;
	string map_name;

	//offsets from galactic center / universe whatever
	float universe_offset_x=0;
	float universe_offset_y=0;
	float universe_offset_z=0;
	float universe_offset_a=0;

	tile_layer [] layers;
	
	int width;
	int height;
	atlas_t atlas;

	bool expand_to(int new_width, int new_height)
		{
		if(new_width <= width){return false;}
		if(new_height <= height){return false;}
		assert(new_width <= MAX_MAP_W);
		assert(new_height <= MAX_MAP_H);
		
		width = new_width;
		height = new_height;
		
		return false;
		}

	void set(int i, int j, LAYER layer_id, int value)
		{
		if(value < 900)layers[layer_id].data[i][j] = value; //NOTE WARN TODO HARDCODED value < 900
		minimap.invalidate();
		}

	@disable this();

	this(atlas_t atlas)
		{
		minimap = new minimap_t(this);
		this.atlas = atlas;
		
		//FIXME: This should have a factory or other helper. (A CONSTRUCTOR LULULUL?!?!?)
		
		auto floor = tile_layer(); //floor (should be)
		auto wall = tile_layer(); //floor (should be)
		auto hull = tile_layer(); //floor (should be)
		auto decal = tile_layer(); //floor (should be)
		
		floor.name = "Floors";
		wall.name = "Walls";
		hull.name = "Hulls";
		decal.name = "Decals";
		
		layers ~= floor;
		layers ~= wall; //wall (should be)
		layers ~= hull; //hull ?
		layers ~= decal; //decal
		}

	bool is_space(float x_px, float y_px)
		{
		int i = to!int(cap_ret(x_px/g.TILE_D, 0, width-1));
		int j = to!int(cap_ret(y_px/g.TILE_D, 0, height-1));
		//writefln("%d,%d", width, height);
		//writefln("%f,%f = %d,%d", x_px, y_px, i, j);
		assert(width > 0);
		assert(height > 0);
		assert(i >= 0);
		assert(j >= 0);
		assert(i < g.world.maps[0].width);
		assert(j < g.world.maps[0].height);

		if(layers[0].data[i][j] == 0)
			{
			return true;
			}else{
			return false;
			}
		}
	
	bool would_collide(float x_px, float y_px)
		{
		int i = to!int(cap_ret(x_px/g.TILE_D, 0, width-1));
		int j = to!int(cap_ret(y_px/g.TILE_D, 0, height-1));

		//writefln("[%f %f][%d %d] = %d", x_px, y_px, i, j, data[i][j]);
		if(layers[1].data[i][j] == 0)// || data[i][j] <= 700)
			{
			return false;
			}else{
			return true;
			}
		}

	float light_x = 20;
	float light_y = 20;
	float light_swell = 25;

	void draw(viewport_t v)
		{
		assert(width > 0);
		assert(height > 0);
		
		//screen caps
		// FIXME, add UNIVERSE OFFSET to FRUSTRUM CULLING
		int start_x =                0 + to!int(v.offset_x/g.TILE_D);
		int start_y =                0 + to!int(v.offset_y/g.TILE_D);
		int end_x 	= ((v.w)/g.TILE_D)+1 + to!int(v.offset_x/g.TILE_D); //should be v.w-v.x?
		int end_y 	= ((v.h)/g.TILE_D)+1 + to!int(v.offset_y/g.TILE_D); //should be v.h-v.y?
		
		//array boundary caps
		if(start_x < 0)start_x = 0;
		if(start_y < 0)start_y = 0;
		if(start_x > width-1)start_x = width-1;
		if(start_y > height-1)start_y = height-1;
		if(end_x < 0)end_x = 0;
		if(end_y < 0)end_y = 0;
		if(end_x > width-1)end_x = width-1;
		if(end_y > height-1)end_y = height-1;
		//writefln("%d, %d", start_x, start_y);
		//writefln("%d, %d", end_x, end_y);
		//writefln("%d-%d %d-%d -- v[%d][%d]", start_x, start_y, end_x, end_y, v.w/g.TILE_D, v.h/g.TILE_D);	
		//writefln("WIDTH %d, HEIGHT %d", width, height);
		//assert(layers.length > 0);
//		writeln("LENGTH ", layers.length);
//		writeln(" - ", layers[3]);
//		writeln(" - ", layers[3].data[0][0]);

// HULL:
// COPY PASTA------------------------------------------------------
		if(g.draw_hull != false)
		{
		for(int i = start_x; i <= end_x; i++) //note <= because this is start/end coordinates, not, "width" which would be -1.
			for(int j = start_y; j <= end_y; j++)
				{
		//		writefln("%d,%d", i, j);
			//	assert(i>=0);
			//	assert(j>=0);
				if(layers[2].data[i][j] == 0)
					{
					// empty/space
					}else{
					al_draw_bitmap(
						g.world.atlas_hull.grab_tile(layers[2].data[i][j]), //could this be atlas[index] instead of atlas.grab_tile? 
						i*g.TILE_D - v.offset_x + v.x/g.TILE_D + universe_offset_x, 
						j*g.TILE_D - v.offset_y + v.y/g.TILE_D  + universe_offset_y, 
						0);
					}
				}
		}

// Floors
//-------------------------------------------------------------------
		for(int i = start_x; i <= end_x; i++) //note <= because this is start/end coordinates, not, "width" which would be -1.
			for(int j = start_y; j <= end_y; j++)
				{
		//		writefln("%d,%d", i, j);
			//	assert(i>=0);
			//	assert(j>=0);
				if(layers[0].data[i][j] == 0) //this is hardcoded to a LAYER. FIXME: encapsulation leak
					{
					// empty/space
					}else{
						
					al_draw_bitmap(
						atlas.grab_tile(layers[0].data[i][j]), //could this be atlas[index] instead of atlas.grab_tile? 
						i*g.TILE_D - v.offset_x + v.x/g.TILE_D + universe_offset_x, 
						j*g.TILE_D - v.offset_y + v.y/g.TILE_D + universe_offset_y, 
						0);
						
/*
					al_draw_tinted_bitmap(
						atlas.grab_tile(layers[0].data[i][j]),
						al_map_rgba_f(1, 0.5, 0.5, 0.5),
						i*g.TILE_D - v.offset_x + v.x + universe_offset_x, 
						j*g.TILE_D - v.offset_y + v.y/g.TILE_D + universe_offset_y, 
						0);*/
					}
				}


// Decals:
// COPY PASTA------------------------------------------------------
		if(g.draw_decals != false)
		{
		for(int i = start_x; i <= end_x; i++) //note <= because this is start/end coordinates, not, "width" which would be -1.
			for(int j = start_y; j <= end_y; j++)
				{
		//		writefln("%d,%d", i, j);
			//	assert(i>=0);
			//	assert(j>=0);
				if(layers[3].data[i][j] == 0)
					{
					// empty/space
					}else{
					al_draw_bitmap(
						g.world.atlas_decal.grab_tile(layers[3].data[i][j]), //could this be atlas[index] instead of atlas.grab_tile? 
						i*g.TILE_D - v.offset_x + v.x/g.TILE_D + universe_offset_x, 
						j*g.TILE_D - v.offset_y + v.y/g.TILE_D + universe_offset_y, 
						0);
					}
				}
		}

// WALLS
// fixme genericify
// COPY PASTA------------------------------------------------------
		if(g.draw_walls != false)
		{
		for(int i = start_x; i <= end_x; i++) //note <= because this is start/end coordinates, not, "width" which would be -1.
			for(int j = start_y; j <= end_y; j++)
				{
		//		writefln("%d,%d", i, j);
			//	assert(i>=0);
			//	assert(j>=0);
				if(layers[1].data[i][j] == 0)
					{
					// empty/space
					}else{
					al_draw_bitmap(
						g.world.atlas2.grab_tile(layers[1].data[i][j]), //could this be atlas[index] instead of atlas.grab_tile? 
						i*g.TILE_D - v.offset_x + v.x/g.TILE_D + universe_offset_x, 
						j*g.TILE_D - v.offset_y + v.y/g.TILE_D + universe_offset_y, 
						0);
					}
				}
		}


// LIGHTING SETUP
// -------------------------------------------------------------	
//	const int ALLEGRO_ADD = 0;
//	const int ALLEGRO_DEST_MINUS_SRC = 2;

	const int ALLEGRO_ZERO = 0;
	const int ALLEGRO_ONE = 1;
	const int ALLEGRO_ALPHA = 2;
	const int ALLEGRO_INVERSE_ALPHA = 3;
	
	al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_DEST_MINUS_SRC, ALLEGRO_ONE, ALLEGRO_ALPHA);

// LIGHTING PASS (per lightbulb)? Maybe?
// -------------------------------------------------------------	

	// BUG BUG BUG BUG. v.x is v.x. but v.y is v.y/g.TILE_D? NO NO NO NO NO
	// BUG BUG BUG BUG. v.x is v.x. but v.y is v.y/g.TILE_D? NO NO NO NO NO
	// BUG BUG BUG BUG. v.x is v.x. but v.y is v.y/g.TILE_D? NO NO NO NO NO
	// BUG BUG BUG BUG. v.x is v.x. but v.y is v.y/g.TILE_D? NO NO NO NO NO
	// BUG BUG BUG BUG. v.x is v.x. but v.y is v.y/g.TILE_D? NO NO NO NO NO
	// BUG BUG BUG BUG. v.x is v.x. but v.y is v.y/g.TILE_D? NO NO NO NO NO
	// BUG BUG BUG BUG. v.x is v.x. but v.y is v.y/g.TILE_D? NO NO NO NO NO
	// FIX EVERYWHERE.<------------
	
	/*
		NOTE: Current blending mode is ADDITIVE SHADOW. <---
	*/
	static if(false)
		{
		float val = 0.5f;
		float x1 = 640;
		float y1 = 480; 
		float x2 = 640 + 320;
		float y2 = 480 + 240;
		al_draw_filled_rectangle(x1, y1, x2, y2, al_map_rgba_f(val,val,val, 1.0f));
		}

// LIGHTING PASS (per tile)
// -------------------------------------------------------------	
	if(g.draw_walls != false) //shouldn't we have a DO_LIGHTING flag?
		{
		for(int i = start_x; i <= end_x; i++) //note <= because this is start/end coordinates, not, "width" which would be -1.
			for(int j = start_y; j <= end_y; j++)
				{
				// Draw a white (or less brightness) square, with the right blending, per-tile
				// to be a "light"
				
				// - Light doesn't have to be only a tile (could be a circle!)
				// - light could also be a gradient bitmap.
				// Not sure if this is the best or worst way to do this
				// Do multiple lights add together? I think this is actually a "screen burn" effect.
				
				float x1 = i*g.TILE_D - v.offset_x + v.x/g.TILE_D + universe_offset_x;
				float y1 = j*g.TILE_D - v.offset_y + v.y/g.TILE_D + universe_offset_y; 
				float x2 = (i+1)*g.TILE_D - v.offset_x + v.x/g.TILE_D + universe_offset_x;
				float y2 = (j+1)*g.TILE_D - v.offset_y + v.y/g.TILE_D + universe_offset_y;

				import std.math;
//				light_x += 0.01f;
//				light_y += 0.01f;
				float A = to!float(i)-light_x;
				float B = to!float(j)-light_y;
				float d = 
					sqrt(
						A*A + B*B
						);
				
				light_swell += .00002f;
				
				float val = (d/light_swell); //higher divisor means faster fall off / shorter radius
//					al_draw_filled_rectangle(x1, y1, x2, y2, al_map_rgba_f(val,val,val, 0.5f));
				if(val > 1.0f)val=1.0f;
				if(val < 0)val=0f;

				al_draw_filled_rectangle(x1, y1, x2, y2, al_map_rgba_f(val,val,val, 1.0));
				}
		}

		// DEFAULT blender
		al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_ADD, ALLEGRO_ONE, ALLEGRO_INVERSE_ALPHA);

		}// end of whatever draw()
	
	void tick()
		{
		}

	//WARN: Appending immutable strings = slow?
	string serialize_bitmap_layer(int [MAX_MAP_W][MAX_MAP_H] data) //why is this need hardcoded??? 
		{
		string output = "";

		for(int j = 0; j < height; j++)
			{
			for(int i = 0; i < width; i++)
				{
				output ~= format("%d", data[i][j]);
				if(i != width-1)
					{
					output ~= " ";
					}else{
					output ~= "\n";
					}
				}
			}
	
		return output;
		}

	void create_empty_map(string path, int width, int height)
		{
		import std.stdio;
		import std.file;
		const int LAYERS = 4;
		immutable string[LAYERS] layer_names = ["FLOOR", "WALL", "GRID", "Object list"];
		immutable bool[LAYERS] 		is_bitmask_layer = [true, true, true, false];
		
		writefln("Creating new empty map at [%s]", path);
		File file = File(path, "w");
		file.writefln("width=%d", width);
		file.writefln("height=%d", height);
		file.writefln("layers=%d", LAYERS); //layers
		for(int k = 0; k < LAYERS; k++)
			{
			file.writefln("layer=%1d name=\"%s\"", k, layer_names[k]);
			if(is_bitmask_layer[k])	
				{
				//FIXME cleanup
				file.write( 
					serialize_bitmap_layer(
						g.world.maps[0].layers[k].data
						) 
						);
				/*
				if(k == 0)file.write( serialize_bitmap_layer(floor_layer) );
				if(k == 1)file.write( serialize_bitmap_layer(wall_layer) );
				if(k == 2)file.write( serialize_bitmap_layer(hull_layer) );*/
				}else{

				// This is the opposite of an empty map, lol.
				file.writefln("Number of Objects=%d", g.world.objects.length);

				foreach(int i, o; g.world.objects)
					{
					file.writefln(
						serialize_object_to_file(o, i)
						);
					}
				}	
			}		
		}
		
		string serialize_object_to_file(object_t o, int index)
				{
				return format("id:%06d x:%.2f y:%.2f m:%d x_vel:%.2f y_vel:%.2f Name: %s", index, o.x, o.y, o.m, o.x_vel, o.y_vel, o.pretty_name);
				}

	void save_map2(string path)
		{
		import std.stdio;
		import std.file;
		import std.json;
		writeln("Saving Map Format v2");
		writeln("-------------------------------------");		
			{
			File file4 = File("./data/maps/map_output_v2.json", "w");
			
			// we load the old file format to keep the schema
			// and then we change/fill the relevant data fields
			
			map_in_json_format.object["map_name"] = "taco";
			map_in_json_format.object["map_offset_x"] = 1;
			map_in_json_format.object["map_offset_y"] = 2;
			map_in_json_format.object["map_offset_a"] = 3;
			map_in_json_format.object["tile_layers"] = 3;
			map_in_json_format.object["object_layers"] = 1;
			map_in_json_format.object["layer_names"] = JSONValue( ["floors", "walls", "hull", "decal"] );
			
			map_in_json_format.object["layer0"] = JSONValue( layers[0].data ); //omg std.json is amazing
			map_in_json_format.object["layer1"] = JSONValue( layers[1].data );
			map_in_json_format.object["layer2"] = JSONValue( layers[2].data );
			map_in_json_format.object["layer3"] = JSONValue( layers[3].data );
			map_in_json_format.object["width"] = 100;
			map_in_json_format.object["height"] = 100;
			

// TODO <------------ OBJECTS inside an array/named object?
//			map_in_json_format.object["objects"] //PASS-THROUGH at the moment!


			file4.write(map_in_json_format.toJSON(true));
			file4.close();

			//string str = std.file.readText("./data/maps/map3.json"); //INPUT
			//JSONValue j = parseJSON(str);
			//File file3_output = File("./data/maps/map_output.json", "w");
			//file3_output.write(j.toJSON(true));
			//writeln(j.toJSON(true));			
			
			/*
				TODO SECTION:
				
				 - JSON OUTPUT of current live map data.
				 
				 Currently, we're just opening, and re-saving our JSON file!
					(and USING that file for live data in load_map2) but we're
					not saving that live data properly yet.
			*/
			
			
			}

		}

	import std.json;
	JSONValue map_in_json_format;

	void load_map2(string path)
		{
		import std.stdio;
		import std.file;
		import std.json;		
		
		// JSON snippet: See also http://dpaste.dzfl.pl/bcb14d6a 
		string str = std.file.readText("./data/maps/map_output_v2.json");
		
		JSONValue t = parseJSON(str);
		map_in_json_format = t;
		
		writeln("--------------------------------");
		writeln("--------------------------------");

		width = to!int(t.object["width"].integer);
		height = to!int(t.object["height"].integer);
		writeln(width, " by ", height);
	
		foreach(int j, r; t.object["layer0"].array)
			{
			foreach(int i, val; r.array)
				{
				layers[0].data[j][i] = to!int(val.integer); //"integer" outs long. lulbbq.
				}
			}
		foreach(int j, r; t.object["layer1"].array)
			{
			foreach(int i, val; r.array)
				{
				layers[1].data[j][i] = to!int(val.integer); //"integer" outs long. lulbbq.
				}
			}
		foreach(int j, r; t.object["layer2"].array)
			{
			foreach(int i, val; r.array)
				{
				layers[2].data[j][i] = to!int(val.integer); //"integer" outs long. lulbbq.
				}
			}
		foreach(int j, r; t.object["layer3"].array)
			{
			foreach(int i, val; r.array)
				{
				layers[3].data[j][i] = to!int(val.integer); //"integer" outs long. lulbbq.
				}
			}

		writeln("OBJECTS:");
		writeln("--------------------------------");
		writeln("--------------------------------");
		writeln("type: ", t.object["objects"].type);
		foreach(string key, o; t.object["objects"].object)
			{
			writeln("key: ", key, " = ", o);
			
			object_t obj = new object_t();		
			obj.pretty_name = o.object["name"].str;
			obj.type = o.object["type"].str;
			obj.x = o.object["x"].integer;
			obj.y = o.object["y"].integer;
			obj.m = 0; //map

			if(obj.type == "MONKEY")obj.anim = new animation_t(true); //fixme
			if(obj.type == "DOOR")obj.anim = new animation_t(false); //fixme
			g.world.objects ~= obj;
			writefln("Adding object. New length of array [%d]", g.world.objects.length);
			}
		
		writeln("--------------------------------");
		writeln("--------------------------------");
		
		minimap.on_load();
		}
	}

class minimap_t
	{
	ALLEGRO_BITMAP *minimap_bmp;
	ALLEGRO_BITMAP *mini_floors_bmp;
	map_t m;
	int x=25;
	int y=150;
	bool is_dirty=true;
	float scale_factor=2.0;
	
	void onClick(float rel_x, float rel_y) //relative to dialog position
		{
		g.world.objects[0].x = x + rel_x*g.TILE_D/scale_factor;
		g.world.objects[0].y = y + rel_y*g.TILE_D/scale_factor;
		}
	
	void set_scale(float val)
		{
		scale_factor = val;
		}
	
	this(map_t map)
		{
		m = map;
//		on_load();
		}

	void on_load()
		{
	//	assert(minimap_bmp == null, "Don't call me twice!");
		minimap_bmp = al_create_bitmap(m.width, m.height);
//		assert(minimap_bmp != null);
		}
	
	void invalidate()
		{
		is_dirty = true;
		}
		
	// TODO NEEDS LAYER SUPPORT
	void redraw_cache()
		{
		ALLEGRO_COLOR c = ALLEGRO_COLOR(0,0,0,0);
		ALLEGRO_COLOR c2 = ALLEGRO_COLOR(0,0,0,0);
		ALLEGRO_COLOR c3 = ALLEGRO_COLOR(0,0,0,0);
		ALLEGRO_COLOR c4 = ALLEGRO_COLOR(0,0,0,0);
		ALLEGRO_COLOR result;
		ALLEGRO_BITMAP *original_bmp = al_get_target_bitmap();
		al_set_target_bitmap(minimap_bmp);
		al_lock_bitmap(minimap_bmp, al_get_bitmap_format(minimap_bmp), ALLEGRO_LOCK_WRITEONLY); //this function signature is stupid.
		for(int i = 0; i < m.width; i++)
			for(int j = 0; j < m.height; j++)
				{
				int value = m.layers[0].data[i][j];
				int value2 = m.layers[1].data[i][j];
				int value_hull = m.layers[2].data[i][j];
				int value_decals = m.layers[3].data[i][j];
				int number_of_layers=0;
				
				//what if we simply don't count zeros? Hmm. But then black spots won't show up.
				
				if(g.draw_floors)
					{
					number_of_layers++;
					c = g.world.atlas.tiles_minimap_pixels[value]; 
					}
				if(g.draw_walls)
					{
					number_of_layers++;
					c2 = g.world.atlas2.tiles_minimap_pixels[value2];
					}
				if(g.draw_hull)
					{
					number_of_layers++;
					c3 = g.world.atlas_hull.tiles_minimap_pixels[value_hull]; 
					}
				if(g.draw_decals)
					{
					number_of_layers++;
					c4 = g.world.atlas_decal.tiles_minimap_pixels[value_decals]; 
					}
					
				if(number_of_layers > 0)
					{
					number_of_layers--;
					result.r = (c.r+c2.r+c3.r+c4.r)/number_of_layers; 
					result.g = (c.g+c2.g+c3.g+c4.g)/number_of_layers; 
					result.b = (c.b+c2.b+c3.b+c4.b)/number_of_layers; 
					result.a = 1; 
					}else{
					result = ALLEGRO_COLOR(0,0,0,1);
					}
				
				al_draw_pixel(i, j, result);
				}
			
		al_unlock_bitmap(minimap_bmp);
	   	al_set_target_bitmap(original_bmp);
	}
	
	void draw(viewport_t v)
		{
		if(is_dirty)
			{
			redraw_cache();
			is_dirty = false;
			}
					
		al_draw_tinted_scaled_bitmap(minimap_bmp,
			ALLEGRO_COLOR(1,1,1,0.9),
			0, 
			0, 
			minimap_bmp.w, 
			minimap_bmp.h,
			x + v.x, 
			y + v.y, 
			minimap_bmp.w*scale_factor,
			minimap_bmp.h*scale_factor, 
			0);
		
		//needed? should be cached??
		foreach(o; g.world.objects)
			{
			al_draw_filled_rectangle(
				(( o.x/g.TILE_D)    * scale_factor) + v.x + x, 
				(( o.y/g.TILE_D)    * scale_factor) + v.y + y,
				(((o.x/g.TILE_D)+1) * scale_factor) + v.x + x - 1, 
				(((o.y/g.TILE_D)+1) * scale_factor) + v.y + y - 1,
				ALLEGRO_COLOR(1,1,1,1));
			}
		}
	}


/**
	Scans "super" ini and places it into an associated array.
	
		- Feature: are we going to use tabs to denote indentation? (ala NOT an ini?)
		- Feature: we also allow 2-D data which isn't an ini
*/

import std.stdio;
import std.file;
import std.string;
import std.conv;

class ini_parser 
	{
	string[string] data;
	
		//how do we handle indention?
		// sectionname.subsection 
		// [section]				ini file
		// [section.subsection] 	ini file
		// we cal also support implied, through intention
		// [section]
		//		[subsection]
		// 
		// But it's pretty funny to do all this work just so someone doesn't write a couple extra letters. But it'll reduce user error.

	int getIntegerValue(string s)
		{	
		auto i = indexOf(s, "=", 0);
		string string_val = s[i+1..$].stripLeft().stripRight();
		int val = to!int(string_val);
		return val;
		}

	string getKey(string s)
		{	
		auto i = indexOf(s, "=", 0);
		if(i == -1)return "";
		string string_key = s[0..i].stripLeft().stripRight();
//		writefln("Returning key [%s]", string_key);
		return string_key;
		}

	string getStringValue(string s)
		{	
		auto i = indexOf(s, "=", 0);
		if(i == -1)return "";
		string string_val = s[i+1..$].stripLeft().stripRight();
//		writefln("Returning string value [%s]", string_val);
		return string_val;
		}
	
	string getSectionString(string s)
		{
		auto i = indexOf(s, "[", 0);
		if(i == -1)return "";
		auto i2 = indexOf(s, "]", 0);
		string string_val = s[i+1..i2].stripLeft().stripRight();
//		writefln("Returning section [%s]", string_val);
		return string_val;
		}

	string getComments(string s)
		{
		import std.regex;
		auto r = regex(r".*?(#.*)");
		auto x = matchFirst(s, r);
//		writefln("Grabbing comments = [%s]", x[0]);
		return x[0]; 
		}

//from http://rosettacode.org/wiki/Strip_comments_from_a_string#D
	string remove1LineComment(in string s, in string pat=";#") 
		{
		import std.regex;
		const re = "([^" ~ pat ~ "]*)([" ~ pat ~ `])[^\n\r]*([\n\r]|$)`;
		return s.replace(regex(re, "gm"), "$1$3");	
		}
		
	string removeComments(string s)
		{
		return remove1LineComment(s, "#");
		/*import std.regex;
		auto r = regex(r"^(.*?)#");
		auto x = matchFirst(s, r);
		writefln("Removing comments, leftover string = [%s]", x[0]);
		foreach(a; x)
			{
			writefln("  - [%s]", a);
			}
		return x[0];*/
		}
		
	enum mode //token FSM
		{
		confused,
		section,
		subsection,
		subsubsection,
		}
		
	bool hasAnything(string s)
		{
		s = s.strip(); // just empty whitespace?
		if( s.length == 0 )
			{
			return false;
			}else{
			s = removeComments(s); //any comments in there?
			if(s.length == 0)
				{
				return false;
				}
			}
		return true;
		}
		
	int countIndents(string s)
		{
		int number_of_tabs = 0;
		s = s.replace(" ", "");
		for(int i = 0 ; i < s.length; i++)
			{
			if(s[i] == '\t')
				{
				number_of_tabs++;
				} // stop counting tabs AFTER we reach a non-tab!
				else{
				break;
				}
			}
		return number_of_tabs;
		}
		
	this(string path)
		{
		import std.file;
		
		File file = File(path, "r");
		
		int current_mode=mode.confused;
		string section = "";
		string subsection = ""; 
		string subsubsection = ""; 
		string key_value;
		string key;
		string value;
		string full_key_path=""; //section.subsection.key
		
		int indent_level = 0;
		int last_indent_level = 0;
		string last_section="";
		string last_subsection="";
		string last_subsubsection="";
		/*
			FIXME: SERIOUS ISSUE. "section" is not section! It's the top-level name of the modu8le or ini file. "Monkey" is not the section! It's the object! Graphics is the section!
			
			ALSO, we can't count indent_levels if we DIDN'T GET A VALUE. (empty but tabbed)
		*/
		foreach(line; file.byLineCopy)
			{
			//cache values
			last_indent_level = indent_level;
			last_section = section;
			last_subsection = subsection;
			
			// if tabs, but NO SECTION OR KEY/VALUE to be found, skip setting our "indent" tracking!
			// --------------------
			// if(???)
			if(!hasAnything(line))
				{
				continue; //skip whitespace-only lines, or lines with only comments!  
				}
			// --------------------
			indent_level = countIndents(line);

/*
How is this supposed to work? We don't SET the new values till later!!
-------------------------------------------------------------------------
*/
			if(indent_level == 0 && last_indent_level > 0) //3,2,1 to 0
				{
				writefln("reverting subsection [%s] to [%s]", subsection, "root");//last_subsection);
			//	subsection = last_subsection; //reset
				subsection = ""; //reset <--- this works because we HARDCODE it.
				}

			if(indent_level == 1 && last_indent_level > 1) //3,2 to 1
				{
				writefln("reverting subsection [%s] to [%s]", subsubsection, "root");
				subsubsection = ""; 
				}
			
			// THIS IS NOT USED. This would be SUB-SUB-section!
			if(indent_level == 1 && last_indent_level == 2) //2 to 1
				{
				writefln("reverting sub-subsection [%s] to [%s]", subsection, last_subsection);
				subsection = last_subsection; //reset
				}
/*
-------------------------------------------------------------------------
*/
			writefln("indent level %d", indent_level);
			string line2 = removeComments( line.stripLeft() ).stripRight();
			for(int i=0; i<indent_level;i++)
				{
				writef("\t");
				}
			writefln("line2=[%s]", line2);
	
			// NOTE, isn't handling subsections yet. (Why not just freakin' write them like proper INI?   Section.Subsection.Blah.  And we just have section="section.subsection.blah". Maybe. Hmm. We still don't have easy tree access but do we care?)
			if(line2.length > 0)
				{
				if(line2[0] == '[')
					{
//					if(section == "")
					if(indent_level == 0)
						{
						current_mode = mode.section; //does this even matter? maybe for later scans.
						section = getSectionString(line2.idup.toLower()); //idup allows char[] -> string ... somehow
						}
					if(indent_level == 1)
						{
						current_mode = mode.subsection;
						subsection = getSectionString(line2.idup.toLower());
						}
					if(indent_level == 2)
						{
						current_mode = mode.subsubsection;
						subsubsection = getSectionString(line2.idup.toLower());
						}
					}else{
					key = getKey(line2.idup.toLower());
					value = getStringValue(line2.idup);
					
					//NOT SURE if we really NEED to prune these but sure:
					
					if(subsubsection != "")
						{
						full_key_path = 
							section.toLower() ~ "." ~ 
							subsection.toLower() ~ "." ~ 
							subsubsection.toLower() ~ "." ~ 
							key.toLower();
						}else if(subsection != "")
						{
						full_key_path = 
							section.toLower() ~ "." ~ 
							subsection.toLower() ~ "." ~ 
							key.toLower();
						}else if(section != "")
						{
						full_key_path = 
							section.toLower() ~ "." ~ 
							key.toLower();
						}else{
						full_key_path = 
							key.toLower();
						}

					data[full_key_path] = value; // we don't lowercase VALUES.
					}
				}
			}
		
		foreach(k, v; data)
			{
			writeln(k, " = ", v);
			}

		get_tileset();
		}
		
	string get_tileset()
		{
		writefln("---------------------------------");
		writefln("monkey.name = %s", data["monkey.name"]);
		string name = data["monkey.name"];
		writefln("monkey.graphics.file = %s", data["monkey.graphics.file"]);
		//writefln("Monkey.Graphics.size  = %s", data["Monkey.Graphics.size"]); //how do we split these? Or, just do TWO LINES, width=32 height=32
		
		writefln("monkey.graphics.width = %s", data["monkey.graphics.width"]); 
		writefln("monkey.graphics.height = %s", data["monkey.graphics.height"]); 
		
		writefln("monkey.graphics.up = %s", data["monkey.graphics.up"]);
		writefln("monkey.graphics.down = %s", data["monkey.graphics.down"]);
		writefln("monkey.graphics.left = %s", data["monkey.graphics.left"]);
		writefln("monkey.graphics.right = %s", data["monkey.graphics.right"]);
		writefln("monkey.graphics.dead = %s", data["monkey.graphics.dead"]);
		
		return "";
		}
	}	


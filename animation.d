import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;

import common;

class animation_t
	{
	dir current_dir;
	cell_location [5] cells;
	ALLEGRO_BITMAP* [5] sprites; //sub bitmaps
	ALLEGRO_BITMAP* sprite_sheet;
	
	@disable this();
	
	this(bool is_monkey) //HACK!!!!!!!!!!!!!!!!!!!!!!!!!!!
		{
		if(is_monkey){
		sprite_sheet = al_load_bitmap("./data/tiles/monkey.png");
		}else{
		sprite_sheet = al_load_bitmap("./data/tiles/public.png");
		} //door
		
		assert(sprite_sheet != null);
		with(g){
		const int TILE_SIZE=32; //TEMP. FIXME. dur.
			
		sprites[0] = al_create_sub_bitmap(
					sprite_sheet, 
					0*TILE_SIZE, 
					0*TILE_SIZE, 
					TILE_SIZE, 
					TILE_SIZE);
		sprites[1] = al_create_sub_bitmap(
					sprite_sheet, 
					1*TILE_SIZE, 
					0*TILE_SIZE, 
					TILE_SIZE, 
					TILE_SIZE);
		sprites[2] = al_create_sub_bitmap(
					sprite_sheet, 
					2*TILE_SIZE, 
					0*TILE_SIZE, 
					TILE_SIZE, 
					TILE_SIZE);
		sprites[3] = al_create_sub_bitmap(
					sprite_sheet, 
					0*TILE_SIZE, 
					1*TILE_SIZE, 
					TILE_SIZE, 
					TILE_SIZE);
		sprites[4] = al_create_sub_bitmap(
					sprite_sheet, 
					1*TILE_SIZE, 
					1*TILE_SIZE, 
					TILE_SIZE, 
					TILE_SIZE);
		}
	
		with(dir)
			{
			//based on monkey.png
			cells[down].x = 1;
			cells[down].y = 1;
			cells[down].index = 0;

			cells[up].x = 2;
			cells[up].y = 1;
			cells[up].index = 1;

			cells[right].x = 3;
			cells[right].y = 1;
			cells[right].index = 2;

			cells[left].x = 1;
			cells[left].y = 2;
			cells[left].index = 3;

			cells[dead].x = 3;
			cells[dead].y = 3;
			cells[dead].index = 4;
			}
		}
	
	void set_direction(dir d)
		{
//		writefln("CHANGING DIR TO %s", d);
		current_dir = d;
		}
		
	ALLEGRO_BITMAP* get_directed_cell()
		{
//		writefln("Using [dir]=%s", current_dir);
		return sprites[current_dir];
		}
	}


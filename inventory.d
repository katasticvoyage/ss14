import std.stdio;




//sidenote: Is there a way for us to support punching AND kicking? Would it be useful? Would it be FUN?

enum DAMAGE_TYPE //wrong file? duplicate?
	{
	DT_RADIATION, //<-- don't forget this one
	DT_FIRE,
	DT_BLUNT,
	DT_PIERCING,
	DT_ACID,
	DT_PLASMA, // ??
	}

enum BULK_TYPE
	{
	BULK_NOTHING, //needed?
	BULK_TINY, //needed?
	BULK_SMALL,
	BULK_MEDIUM,
	BULK_LARGE,
	BULK_HUGE,
	BULK_MASSIVE, // only holds 1 item, ever. ala forklift lifting a computer/container/etc.
	}

/* this won't compile...
  immutable int[SLOT_TYPE] slot_sizes = [
  SLOT_TYPE::BULK_TINY: 4,
  SLOT_TYPE::BULK_SMALL: 10,
  SLOT_TYPE::BULK_MEDIUM: 2000
];
*/ 

class script_t
	{
	string data;
	}
	
class object_t 
	{	
	object_traits_ct	object_traits;
		
	//composition... I think?
	inventory_traits_ct inventory_traits; //if has an inventory
	accessory_traits_ct accessory_traits; 
	// - if it has accessories (which are usually "hidden" inventories in a kind of way.)
	
	
	// if it's a container (or do we just have if traits != null)
	//	bool 			is_container;
	//	container_t 	container; //ref type
	// Events
	// -----------------------------------------------------------------
	void on_thrown(object_t by){}    //start of throw
	void on_object_collision(float force, object_t with){} //after being thrown and hits something
	void on_wall_collision(float force, object_t with){} //after being thrown and hits something
		// is hitting a FLOOR a different event from hitting a WALL or PERSON?
		// perhaps in on_collision you can look up the collision type.
		
	void on_damaged(int damage_type, object_t by){} // burn, radition, fire, etc. handle in switch?
	void on_crushed(float force, object_t by){} //stepped on, or containing bag is deformed.
		// do we want a "is inside" difference? We've got a bool.
		
	void on_pushed(object_t by){}
	void on_bumped(){} //same as pushed?
	
	
	void on_shoved(object_t by){} //specifically someone getting pushed over using PUSH threat level? 
	
	// ala banana
	void on_tripped(object_t by){} // I got tripped by this object?
	void on_tripping(object_t who){} // i tripped this guy?  
	
	
	void on_wet_by(int liquid_type){} // feet are wetting based on floor flooding height. Then pants. Then torso/hands/etc. Up to head. Each of the body's items are then called with this. ???
	
	
	void on_flooded(int liquid_type){}// with liquid type. 
	void on_pressure_update(){} // ONLY UPDATED with async physics ticks. For testing whether an object EXPLODES or IMPLODES when it's too high or too low pressure. We might also be able to SYSTEMIZE this for many objects instead of using an ovverriding script.
	
	// void on_sucked_into_space(){} ??
	// void on_teleported(){} ??   We DON'T have teleporters in this universe, right? (support for SS13 mod?)

	// User inputs
	// ------------------------------------------------------------------------

	// WHEN OUTSIDE INVENTORY 
	// -----------------------------------
	//void on_clicked(object_t by){}
	// (can these be shared the same as when inside inventory?)
	//	- ala, a grenade timer clicked, while the grenade is on table. (obviously no hotkeyse)
	
	//see also on_pushed(), etc.
	
	// WHEN INSIDE INVENTORY
	// -----------------------------------
	void on_hotkey_1(){}
	void on_hotkey_2(){}
	void on_hotkey_3(){}
	void on_hotkey_4(){}
	
	void on_left_click(){}
	void on_left_double_click(){} // Turns on SuperView with double-click?
	void on_right_click(){}

	void on_click_at(float x, float y, int map){} // Item is selected and you click somewhere, like using a gun on the world.
	void on_control_click_at(float x, float y, int map){}
	void on_alt_click_at(float x, float y, int map){}
	void on_shift_click_at(float x, float y, int map){}
	//do we support right click at? (non-BYOND)
	
	// different use-case then the above??
		void on_used_by(object_t o){} // dragging something onto it? / also INSERTING an item into it from outside? not sure...
		void on_combine_with(object_t o){}  // or what about combining using two hands
	
	// Actions
	// -----------------------------------------------------------------
	void throw_at(float x, float y){}
		

	}
	
struct object_traits_ct
	{	
	// could we use STATIC data for these non-instance data?
	// or a separate structure...
	BULK_TYPE 	bulk_type;
	bool 		can_be_thrown; // Can all be? Below a certain bulk classification?
	
	// INSTANCE data
	bool is_inside;
	}
	
// TRAITS FOR OBJECTS
//ct for composition type? should we use S for STRUCT? Should we use a class?
struct inventory_traits_ct 
	{
	object_t [] inventory;
	slot_t [] 	slots; //slot types. Question: How do we match which object to which slot?
	int	[]		object_to_slot_mapping; // <- This way. For each object in the inventory, it has an associated map to which slot. (Careful to keep these consistent and not break!)
	
		// ISSUE: How do we deal with SUB-DIVIDING SLOTS and/or COLLECTIONS?
		// A [LARGE] slot holding 4 [MEDIUM] items.
		//  - We could have 4 objects all with the same SLOT number.
		// 		- Which means we have to SCAN for that case instead of simply mapping 1-to-1.
		//		- But it should work otherwise?
	
// Physical stuff
	bool 		is_organized;	// Keeps clutter from falling out or randomly 
	bool 		is_sealed; 		// So stuff won't fall out. if spilled, spills inside. (a treasure chest is unorganized but sealed.)
	
// Actions
	void shuffle() {} //randomize contents because of being bumping / tossed / whatever

	bool 	has_hidden_inventory; // Hidden inventories are for SuperView. You can place accessories like silencers on a weapon and it goes into the inventory.
		
	bool 	spill_from_force;
	int 	spill_force_threshold;
	bool 	spill_when_tossed; // is there a different between FORCE-based shuffling, and ORIENTATION-based shuffling? 
	// That is, a milk jug tray can take some force but turn it UPSIDE DOWN (by throwing it) and they fall out. 
	
	bool	is_sturdy; //allows internal items to be crushed. (think a bag)
		float crush_force_threshold; //minimum force to start crushing in.
		// do we support permanate crushing deformation? Could this provide any real purpose? An egg tray that's crushed (by what??), which crushes the eggs too.
	
	bool	leaks_liquid_in;
	bool	leaks_fire_in;
	bool	leaks_acid_in;
	bool 	leaks_electricity_in;
	bool 	leaks_temperature_in;
		float 	temperature_leak_resistance; //could we just use 0 for the bool true/false? And have floats for all of them?

	bool	can_be_locked; // for common locking types using the ID card system? And we can support overrides for scripted special locking systems?
	bool	is_locked;
	}

struct accessory_slot_t //not sure how different from slot_t
	{
	//deals with what can fit into an accessory slot / hardport.
	}
	
struct accessory_traits_ct
	{
	object_t [] inventory;
	slot_t [] 	slots; //slot types. Question: How do we match which object to which slot?
	int	[]		object_to_slot_mapping; // <- This way. For each object in the inventory, it has 
	
	// QUESTION: Is there EVER a case where you'd want something that can use SuperView AND also have a storage on it? The answer is MAYBE. What about some sort of cool briefcase with a SuperView SECURITY CODE / COMBINATION but then reveals the contents. In THIS case, we might as well just add a separate inventory for hidden objects. OR, allow ANY inventory item to be hidden. But then it's going to "use" up slots and that's all confusing.

	// Perhaps call it an ACCESSORY INVENTORY.
	// Note, we WILL have to have some sort of special HARDPOINT like slot system.
	// Each HARDPOINT accepts ONLY objects that fulfill that HARDPOINT type.
	// So a battery fits into a battery slot. A silencer does not.
	// HOW we determine what GROUP TYPES and/or SETS of items fit a SLOT is a big ???
	// right now.
	}

class superview_control_t
	{
	float x, y, z;
	float x_rot, y_rot, z_rot;
	
	// All 6 DOF. 
	// Q: Do we allow DOF axies to be ROTATED above? Or have the axies ALWAYS pointing to/from player? The reason is, if you rotated something, you may still want it to have to/from/left/right SCREEN rotations. However, what if you had something mounted AT 45 DEGREES and wanted the player to turn the SuperView and then interact with it? I think the COSTS outweigh the BENEFITS (until someone makes a damn good justification for the need.) So we should just keep the axies static.
	
	bool DOF_allow_x; //left/right
	bool DOF_allow_y; //up/down
	bool DOF_allow_z; //in-out of screen
	bool DOF_allow_x_rot;
	bool DOF_allow_y_rot;
	bool DOF_allow_z_rot; //rotating around axis pointing into screen. Ala a knob.	
	
	// - script function to call on touch   
	// - hotkey_1,2,3,4 associated with directly calling it using the hotkey quick buttons.
	// Wait, do hotkeys merely hit buttons? That could be a super useful encapsulation.
	// - If an item HAS a super-view, all actions have to go THROUGH a superview handler.
	// And default_action() / use_item() / whatever() default LMB action simply hit a
	// superview button/whatever. So, when you're IN superview, you can also use the same
	// code.
	
	// Example:
	// - Grenade is in hand.
	// - Clicking on grenade primes it.
	// - However, you could also go into SuperView and pull the priming pin.
	//
	// - Clicking on the grenade in your hand is simply a shortcut to
	//		SuperView->PullPin(1.0f);
	//		where X is 0 to 1.0F (0-100%) of how far the pin is pulled out.
	//
	// - Likewise, click, double-click, point_at, etc could also be hotkey'd through SuperView.
	//	- However, throw_at(x,y) is not likely one.
	//		throw_at(x,y) ends up moving the object, which eventually has 
	//		a on_collision(force:132.2F); event. Neither of which need SuperView. 
	}
	
// push in buttons (AND flip switches? or different class. Different position handler.)
class superview_button_t : superview_control_t
	{	
	bool is_toggle;
	bool is_onoff;
	}
	
// turn knobs.
class superview_knob_t : superview_control_t
	{
	}

// 2-D touchpad input (ala music keyboard interface)
class superview_touchpad_t: superview_control_t
	{
	float value_x; // 0 to 1.0
	float value_y; // 0 to 1.0
	}

// A cover, rotating on one axis, like a flip-up cover hiding the inside buttons of a cellphone/PDA/etc.
// Needed? We could simply use a "button" and not have it connected to any real action.
class superview_cover_t : superview_control_t
	{	
	}

//text display  (LED display too? ala gun ammo display)
class superview_text_display_t : superview_control_t
	{
	// font_t font;
	}

// WOAH, COOL IDEA.
// SuperView Picture-In-Picture / MiniView. Have a small box
// of an item's superview on your normal screen that you can look at.
// So you can have your gun's ammo showing.
// Limit to only ONE P-in-P, so you have to choose what to best automate?
//  - Could make it interactive so you can have a bomb's LED lights showing
//  	as well as let you flick a switch.
struct superview_ct
	{
	// Instance Data
	float x_rot, y_rot, z_rot;
	float x_offset, y_offset, z_offset;
	string title;
	
	// Type Data
	script_t code;
	
	// This is a whole, complete, interactive system. So this could be a bitch
	// to make interactive, requiring a full script for it.
	
	// 2-D views might be easier. But are they? Or is the interactivity the problem?
	}
		
class slot_t
	{
	BULK_TYPE 	bulk_size;
	//bool is_filled; //needed? simpler than testing the object array each time?
	
//	object_t [] objects; //a large slot would simply contain 4 mediums, or 4x4=16 smalls, etc.
	// However, how we DISPLAY those slots may be more difficult to parse. 

	// Actions
	// ------------------------------------------------
	bool is_filled(){if(objects.length != 0) return true; else return false;}
	bool add_object(object_t x){return false; /*if slot is filled*/}
	
	object_t get_object(){return new object_t;} //how to handle multiple objects? (what happens if you call this when there are more than 1 objects in the slot?)
	object_t [] get_objects()
		{
		object_t [] x;
		return x;
		} //for case with multiple?
	
	// Do SLOTS shuffle their insides around? Or just containers?
	
	// Does is it REALLY better for a slot to contain multiple objects?
	// or is it better to have a 1-to-1 slot-to-object interface,
	// and find some way to make "more slots" for smaller objects?
	}
// is a body a container too? (hopefuly for simplicity?)

class container_traits_ct
	{
	}

int main()
	{
		
	return 0;
	}

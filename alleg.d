module alleg;

// Wait, this should all be in GRAPHICS.d

// EXCEPT, maybe, [graphics.d] defines the INTERFACE
// and [alleg.d] provives the IMPLEMENTATION. So we can support SDL or whatever
// if for some reason Allegro stops being supported, or, we need to target
// a different system that Allegro 5 doesn't support. We can also support
// future versions of Allegro (6, etc) if necessary. But that's a pretty rare
// chance that we'll need to.

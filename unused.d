class dialog2_t(T)
	{
	float x,y,w,h;
	void draw_text(){}
	void draw_sprite(){}
	
	T data;
	
	void onPress(float x_rel, float y_rel) //relative to our x,y which is 0,0 
		{
		}

	void onRelease(float x_rel, float y_rel)
		{
		}

	void draw()
		{
		}
	
	void delegate (T)onDraw;
	
	this(void delegate(T) f) 
		{
		onDraw = f;
		}
	}
	
class thing_with_data
	{
	float abc=1, def=2;
	}

class other_thing_with_data
	{
	float abc=3, def=4;
	}

void test_function2()
	{
	auto t  = new thing_with_data; 
	auto t2 = new other_thing_with_data; 
		
	auto dh = new dialog_handler_t;
//	auto dialog = new dialog2_t!thing_with_data(delegate void(t){ writefln("TACO TEST = %2f", data.abc);});

//	dialog.onDraw(t);

	test_dialog();
	assert(0, "END OF PROGRAM");

//	auto dialog2 = new dialog2_t!other_thing_with_data(t2);
	}
	
//-------------------------------------------
class test_window
	{
	float x;
	float y;
	
	void draw_text(string text)
		{
		writeln(text);
		// al_draw_text(g.font, al_map_rgb(255,255,255), x, y, 0, text.toStringz() );
		// void al_draw_text(const ALLEGRO_FONT *font,ALLEGRO_COLOR color, float x, float y, int flags,  char const *text);
		}

	this( void delegate(test_window) onDraw  )
		{	
		this.onDraw = onDraw;
		}

	void run() //called every frame
		{
		onDraw(this);
		}

	 void delegate (test_window) onDraw;
	}

// TESTING ACCESS TO the OWNING function
// Could we turn this into some sort for re-usable TEMPLATE?
void test_dialog() //KAT test delegates accessing class members
	{
	auto t = new test_window( (test_window ptr)
		{ 
		with(ptr)
			{ 
			draw_text(format("Hello, world. [x,y]=[%f,%f]", x, y)); 
			} 
		});
		
	t.run();
	}

// TESTING ACCESS to anything
// ----------------------------------------------------------
struct data_to_access_t
	{
	int tacos;
	}

struct data_to_access2_t
	{
	struct beans
		{
		int x;
		};
		
	beans b;
	}

class abc(T)
	{
	int x;
	void delegate(T) run_delegate;
		
	T data;
		
	this(T t, void delegate(T) d)
		{
		data = t;
		run_delegate = d;
		}
	
	void execute()
		{
		run_delegate(data);
		}
	}

void test_dialog_anything()
	{
//	auto test = () => {writeln("");}; //wait, how can this one have a curly?? but not below!?
	data_to_access_t  d; 
	data_to_access2_t d2; 
	d.tacos = 4;
	d2.b.x  = 5;

//WORKS!
//can only be ONE statement/expression/whatever
	auto x = new abc!data_to_access_t ( d, (d) => writefln("test  %d", d.tacos)  );
//add curly, remove equals  
	auto y = new abc!data_to_access_t ( d, (d){writefln("test  %d", d.tacos);}  );
//verbrose syntax
	auto z = new abc!data_to_access2_t(d2, delegate void (d2){writefln("test2 %d", d2.b.x);}  );
	
	x.execute();
	y.execute();
	z.execute();
	//assert(0, "nope.");
	}

class undo_manager
	{
	undo_action [] undo_stack;
	undo_action [] redo_stack;
	void push(map_t map, int layer, int x, int y, int w, int h){}
	void pop(){} //doesn't APPLY (ala throw away)
	void apply_and_pop(){} //applies!
	
	void undo()
		{
		//move most recent to the REDO stack
		}
	
	void redo()
		{
		import std.range : popBack;
		redo_stack ~= undo_stack[$];
		undo_stack.popBack();
		
		}
	
	void user_has_performed_action() //CLEAR THE REDO STACK.
		{
//		import std.container.array;
		redo_stack = [];
		// NOW - add new action
		}
		
	//how do we handle UNDO/REDO until we MAKE a change, and then break
	// further changes on the stack?
	//https://www.quora.com/How-is-undo-and-redo-functionality-typically-implemented
	}

// doesn't work for objectss.
class undo_action
	{
	int x, y, w, h; //bounding box of what we want to undo
	int layer;
	int [][] data;
//	int map; //not yet
	map_t map;

	this(map_t map, int layer, int x, int y, int w, int h)
		{
		this.map = map;
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
		this.layer = layer;
		
		acquire_snapshot();
		}

	void acquire_snapshot()
		{
		for(int i = 0; i < w; i++)
			for(int j = 0; j < h; j++)
				{
				data[i][j] = map.layers[layer].data[x + i][y + j];
				}	
		}

	void redo()
		{
		//todo
		}
		
	void undo()
		{
		for(int i = 0; i < w; i++)
			for(int j = 0; j < h; j++)
				{
				map.layers[layer].data[x + i][y + j] = data[i][j];
				}	
		}

	}



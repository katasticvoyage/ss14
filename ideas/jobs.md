# SS14 - Ideas for Jobs#

Okay, so the idea here, is that jobs should have a few key charactoristics:

 * Unique gameplay
 * Unique mini-game that requires **arcade skill** to play
 * Easy-to-learn / intuitive basics to perform Space Station essentials and feel included
    * Some professions will have higher TIER versions (like researcher vs Research Director)
    * **BASICS** (which are also essentials!) should also have some kind of social REWARD. So people thank people for providing essential services. 
    * **ADVANCED** research grooms the staff into similar skills that will become useful in obtaining...
    * **EXPERT** research, which has significant RISKS and even danger to the station. 
    	* HOWEVER, stations that produce crazier setups ALSO HAVE HIGHER REWARDS. So the Captian has a **VESTED INTEREST** in allowing research *that he thinks is possible*. Should a rogue researcher go around him, they may end up in prison. Should the captain err too far on the side of RISK, his station may end up in ruins! "*No guts, no glory!*"
	* **MASTER RESEARCH** involves **GAME MODES**. Possibly. Each round, given a certain crew of proven researchers, will officially allow one (or a few) departments to **pursue groundbreaking master research**. The rest of the crew should support this effort. However, **terrorists** may try to stop it. And **rogue researchers** may try their own experiental research without anyone knowing.
 * Harder professions should require more background knowledge
 * Research of some kind to allow **player progression**
 * Research of harder levels should employ **significant risk** to both the player and the station
 * Pursuing this research (and/or other income/fame/etc) should constantly pit the player against **doing his duty** verses **taking the easier route which results in higher personal gain**
  
 Think of the **Mad Scientist** as a perfect example, and then find a correllory for every other profession.
 
For example, the *Botanist.*

* Planting seeds, using a shovel, finding water, watering the plants, etc should all be pretty straight forward.
* Advanced/mid-level research includes becoming better at raising more difficult plants, and diagnosing the problems that may ail them. These more difficult plants end up as **drugs for medical departments**, research departments, and so on.
* Expert research involves experimenting with **new life forms**! Lifeforms that may rebell and murder everyone.

##Section 2
###Section 3
####Section 4







### What is this repository for? ###

* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet

https://guides.github.com/features/mastering-markdown/

http://www.unexpected-vortices.com/sw/rippledoc/quick-markdown-example.html

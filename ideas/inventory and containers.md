# Notes about Inventory and Containers


/*
NEW TERMS
===============================================================================

	SUB-DIVIDING SLOTS and COLLECTIONS.
	---------------------------------------------------------------------------
		- sub-dividing a slot is when say, a LARGE (bulk type) slot is being used to hold 4 medium items. 
		- collections are ARBITRARY NUMBER of "stacks" (ala SS13) of items. They become a SINGLE object in the system, with a variable indicating how many. 1 object, collection of 32 nails, fits inside a SMALL bulk type.
		
		---> QUESTION: What if you have a LARGE and try to fill it with THREE MEDIUMS... and then a small? That'd be four items. Which might be fine. But what if you add ANOTHER SMALL?
			- We can make "larger sizes lossy" so they can only hold effectively their size AND ONE SIZE SMALLER x 4. >>>ANY smaller sizes can still only be 4x.<<<
			- Alternatively: We can make LARGE simply be = 4x MEDIUM, = 16 SMALL = 64 TINY, etc.
				- But how do we deal with the INSANE possible amount of combinations?
				- How do we deal with the data structure, AND, the visual slot system?
				--> We could do "LARGE = 4x MEDIUM = 16x SMALL" but you can ONLY use stacks at that point. So they have to be THE SAME ITEM TYPE for that slot. One large slot could have anywhere between 1, and 16, small batteries. But that slot with one battery could NOT include other object types regardless of size. This doesn't make a HUGE amount of logical sense but it simplifies what a slot does internally and the player's understanding of the system.
					--> A slot can have ONE type of object in it, ever. If you go SMALLER, then it ends up having a NUMBER associated with it, just an impromptu COLLECTION.
						- One large slot has 1 medium battery.
						- Add one more battery and it shows a "2".
						- Try to add a medium-bulk axe, it doesn't let you. Even though there's room for another medium, it HAS to be the same size.
						- This kind of system exists in other games, I'm pretty sure, so it's not impossible / super counter-intuitive for players. 

		- I'm not sure if there are any other alternatives. I'll have to take some time away and let myself think about it without tunnel vision.
*/

/*
	NEW IDEAS
	=======================================================


	Inventory:
	-------------------------------------------------------

- No One Lives Forever (2, at least) has a NEAT FEATURE. It takes TIME to search an inventory. Obvious items show up quickly, but if you hold [SEARCH] on a body, it'll show more hidden / rarer / smaller / harder-to-find items.

---> SUPPORT HIDING ITEMS.
	- One functionality, by hiding items UNDER say, your coat. (How and where could this work? Hiding something under your shirt vs hiding something under another item already in your hands is a completely different mechanism.)
	- Adding hidden compartments to existing items?
	- Hiding items using SuperView. 
		- Open up something, and stick something smaller inside.
		- For example, a Canteen with a top on it, but just enough room to fit something tiny in it.


- GREAT FREAKIN' IDEA. "WORKSPACES."
	- Workspaces are tables, etc, specially made for crafting.
	- You can "craft" anywhere you want, but on this workspaces you have something special. What? You have ADDITIONAL SLOTS. 
	- Your hands are two slots.
	- When physically next to a workspace, click it, and you GAIN SLOTS.
	- These slots aren't "active" slots, they can't be selected like a hand.
		- HOWEVER, could we add ACTIVE SLOTS using robotics or something?
		- Would there be a use?
	- So, say you disassemble something:
		- Screw driver is in one hand.
		- You take a screw out, it's in your other hand.
		- You take another screw out, thanks to [FEATURE] collections, you have multiple screws in one hand.

		
		- That works fine when you're working on something outside your body. But wait, now you're working on something ON your body.
		- You hold a TV in one hand. A screwdriver in the other. You unscrew a bolt.
			- Where does the bolt go? On the FLOOR in ss13.
		- Screws are out, you click the TV with an [OPEN HAND]. Except you need to toss the screw driver on the floor or put it away. You open the cover.
		- Now you need to cut a wire, so you open your backpack/belt and bring out your wire cutters, use them on the TV, and then put them away again so you can use [OPEN HAND] to shut the cover.
		- Pretty tedious though it can be learned effectively. 
		- Well, we keep the same setup but ADD [WORKSPACES].
		- Now, when you get the screws, they fall into the workspace slot.
		- Take more screws? They're added to the workspace slot which becomes a collection of screws.
		- Then you place your screwdriver onto the workspace. (No need for a belt/backpack!)
		- Then you open the cover with your now open hand. 
		- You grab your wirecutter from whereever, cut the wire.
		- Place the wirecutter in the workspace tray, and close the cover.
		- Grab the screws from the tray, put them in the TV in your other hand.
		- Grab the screwdriver from the tray, screw the screws in.
		- Put the screwdriver in your belt. 
		- Pickup the wirecutters, put them in your belt.
		- All the while keeping the TV in your other hand and never dropping it.
		
		- LIKEWISE, we could also add ACTIVE SLOTS.
			- What's an active slot? I'm not sure. There's two distinctions I can think of. Here's the three slot types I can think of so far:
				
				1) A [IMMUTABLE SLOT]. A lightbulb sits in its lightbulb box. You can't modify the lightbulb no matter what without removing it first.

				2) A [NORMAL? SLOT] where you can work on something. (You CAN modify a lightbulb if it's been placed into a WORKSPACE, your HANDS, and possibly other slots on your body. Like putting a SILENCER on a weapon already attached to your back/belt. Maybe.) Notice that it's a slot where you can ACT UPON IT. It's not an ACTION slot, it's an "acted upon" slot.
				
				3) An [ACTION SLOT] is a slot that you can use tools to act UPON another slot (including another action slot). Your hands are action slots. They can act on stuff inside another hand, but also act on normal slots, and pickup from immutable slots. (A normal slot can't be "selected" to pickup from another slot. Your hands can.)
					- So the question is, other than [HANDS] is there any benefit to having:
						- More than 2 hands? (A cyborg with three hands?)
						- [Wearable objects] that GRANT additional hands?
						- [Workstations] that GRANT additional hands?

- SuperView 2000 (or some number to not be same as Pipboy)
	- A 2-D (or rotatable VOXEL view!) that can be interacted with.
	- Other objects can be EMBEDDED into a hidden / accessory inventory. A silencer on a gun. An ammo clip or battery on a gun.
	- It's *FUN* to explore items in SS14. Each item is like a puzzle in Myst. There's benefits to experimenting with objects. Even the Microwave may have special functions you're not aware of normally. What happens if you enter a negative time into the Microwave? (etc) Are there any hidden buttons on the BACKSIDE of the Microwave?
	- Do we support SuperView for STATIONARY objects, or only HELD objects? Because it doesn't make sense that you can ROTATE (to see the underside) of a FRIDGERATOR as if it's in your hand. HOWEVER, we COULD support a FIXED ROTATION SUPERVIEW for objects that support it. For a fridge, if you face the front, it'll show you the front. If you pull the fridge out, and go to the backside, you could see the BACK of the SuperView.
	
- Slot Action Keys. (1,2,3,4)
	- When you select a weapon (or any item really), certain right-click 
		functions are assigned keyboard controls for fast access. Each item
		has their own custom, hardcoded keys. So there's a BENEFIT to becoming familiar
		with an item instead of using a slower right-click menu.
	- Items, where possible, in the SS13 tradition, have a BAD KEY TO NOT PRESS.
		- A frustration key. Like pressing 3, while holding a beaker, spills it
		while in 99% of cases is NOT what you wanted to do. Panic = Critical Failure for the player (but preferably LIMITED damage to other players so everything
		doesn't become some sort of IED to easily hurt other players).




FIGHTING (wrong file but whatever)
-------------------------------------------------------	
	- When fighting a short yellow marker arrow will appear. 
		- Up arrow for attacking your head. 
		- Lower left/right for legs.
		- etc
		
	- You then have a very short time to BLOCK/parry that attack.
	- YOU can block an attack by someone attacking a third party. 
		- So that means three cops, next to a single attacker, can easily parry him at
			3x the potential.
	- You only know to block if you KNOW it's coming. So if someone without warning just attacks, you won't be able to quickly hit the defense button. Basically YOU have to constantly be on guard to deflect an attack.
	
	--> MAYBE, it's harder to block an attack by having to counter the DIRECTION it's
	coming from. So if the enemy is UP-LEFT from you, it's a different block then
	if it comes from DOWN-LEFT. So that means you constantly have to be aware of your
	enemy's POSITION as well (just like in real life.)
		- Maybe blocking is a two step process / key input. Like press the DIRECTION it's coming from, then the body part he's attacking.
		- So a person from UP-LEFT attacking your RIGHT leg/foot would require you to
		press UP-LEFT then DOWN-RIGHT really quickly.
		- So there's a FOCUS element where your brain is constantly calculating where the attack is coming from (which physically STRESSES and wears down your physical, gamer, body/mind.) preventing most people from being at their prime at any given moment (allowing sneak attacks/backstabs), AND, it allows people who are fatigued or OVERWHELMED by attackers, to make mistakes. Just like in real life. Fighting two or more people would be super stressful in real-life.
			
			
			
*/

/*
	- There may be a BENEFIT to having a few SPECIALIZED versions of the generic object_t class.	
			
	We then put these specialized versions into their own arrays so they can be run and drawn faster without tons of branching. Kind of like a particle system but of a few, discrete levels of complexity. 
		
		(Careful about draw order assumptions! Multiple arrays mean sorting to fix
		Z-ordering goes to crap! However, all objects are drawn together'ish so 
		specialized versions shouldn't be used elsewhere... I think. Like, a specialized
		object would never be used for a WALL. A wall object (or specialized wall) object
		is drawn for a wall.)

	For example,
	
	- simple_object_t

		- Cannot have many of the more complex features, such as an INVENTORY.
		- Not sure where the line is yet. 
		- Do we FORCE all objects to work this way (making our job easier? but potentially limiting scripting options...)
			- That is, if ALL BULLETS are required to be simple_object_t, then NO bullets
				could ever have an inventory. In 99.9% this wouldn't matter, right?
				- But what if they NEED it? How do they get around it?
					- Can they spawn a NON simple_object_t bullet? Will it script/logic right?
					- Are there any other options? Or are they just SOL?

	- other_object_types???
		- maybe a few levels of compleixty such as:
			object_t, 
			thin_object_t, (or simple_object_t)
			thinner_object_, (simpler_)
			thinnest_object_t (simplest_)
*/











// Lets find a way to UNITTEST this, you know, completely separately from the rest of the codebase. We can test and refine it
// almost as a separate interactive PROGRAM with a console input and it tells you.

// How do we handle CONTAINER OBJECTS? 
// - belt holds items... but is ALSO an item. 

// Other questions:
// - Can a SAME SIZE bulk ever CONTAIN the same size bulk? Can a medium belt hold... mediums?
// - Does a belt CHANGE in bulk when it's FULL vs EMPTY? Can an EMPTY BELT be put in your pocket but a FULL BELT not? 
//		- Or does it "inflate" to the size of the largest bulk item? An empty belt = small, a belt + medium = medium.



// how does the HIERARCHY work? 
/*

	container -> slots -> objects
	[human body][1].gun

	but what about:

	[human body][1].gun
	[human body][2].belt

	[human body][2].belt[1].grenade
	[human body][2].belt[2].clip

	[human body][2].belt[2].clip[1].bullets_stack_of(20)	

	*(so we don't have 20 bullet objects instead of simply decrementing a counter. reducing the time-density of object allocations.)

	Now the class heirachy looks fine... but wait, how the hell does that fit into:

	countainer -> slots -> objects?


	[human body][2].belt[2]		.clip[1]		.bullets_stack_of(20)	
	[object][slot] .object[slot].object[slot]	.object???

	WE COULD get this to work, simply, if each object CAN (through composition) have a container_t
	and each container_t can reference additional objects? 

	Or, object_t contains
		{
		object_t [] inventory; // <--
		inventory_traits_t inventory_traits;
		}

	So the question is, can we somehow RE-USE the existing functionality of the normal "all objects can contain multiple other objects" into some sort of easily GUI drawable and manipulatable hierarchy.

		-> We could SCAN an object hierarchy upon calling "open_inventory()" and buid the tree.
*/




What if we have a QUALITATIVE heirarchy.

***

For example:

	
	4 TINY 		go into SMALL slot
	4 SMALL 	go into MEDIUM slot 
	4 MEDIUM 	go into LARGE slot
	4? LARGE 	go into HUGE slot
	1 HUGE 		go into MASSIVE slot <-- NOTE, massive slots are HOOK/DRAG/LIFT SLOTS. ONLY ONE ITEM per slot. Ever.
	
 - e.g. Picking up an [Alien Queen] into a [Forklift] or [Medical Containment Machine]. 

HUGE goes in NOTHING? (or rarely only HUGE slots, in rare cases like [Heavy Machinery]/[Mechs]?)
MASSIVE goes in NOTHING.

And we can adjust as necessary and makes it "somewhat" easier to balance by adjusting the **number of slots**, or globally by adjusting how many smaller items fit into a slot of a certain size.


    				- Size 	- Size of containment
    Small pockets 	-
    Large pockets 	-		
    Toolchest		- Large	- 4 medium
    Closet			- BULK	- 


### How do we DISPLAY these sub-hierarchies? (in cases OTHER than "collections")

***

One possible design.
	
    - Each slot shows up in a bar. For slots with more than one item, it shows a higher up hierarchial bar.
	- However, what if nearby slots are BOTH over-filled?

Another possible design. (Good idea, emulates the difficulty of SEARCHING)

	- Items in slots are shared. So if you have a slot, you CLICK the slot and see what's inside.
	- When you have TWO items in one slot, the GRAPHIC displayed is a SPLIT LINE
			with both graphics cut-in-half. This makes the graphics a little harder to
			decipher.
	- Having FOUR items in one slot is split four-way and even harder to decipher.
	- However, you CAN "kind of" eye-ball your inventory slot and have a rough idea
			of what's inside that pocket.
		
	--> Now, how do we handle collections? 3 shotgun shells and one piece of candy?
		- With just 4 shotgun shells we can show the SHOTGUN shell and the number 4.
		- With 3+1 (1 candy), we could show "3" over the shotgun sprite and use the normal two-split.
		- With 2+2, we could show 2, 2 and the normal two-split.
		- HOWEVER these are gonna be TIGHT inventory slots in terms of pixel area.
		
		--> Now, how do we handle MORE than four items?
			- DEFINITELY do some kind of "click it to look inside."
			- Imagine. Large slot = 4x medium = 4x(4x small) = 16 small items, right?
			- If we have 16 items of ONE time (shotgun shells) sure.
					[SEE CONTAINERS section]
						
						
			- Now, back to the MORE THAN FOUR ITEMS. We've got a large slot with small items.
				- HOW does this work other than a collection of up to 16 shotgun shells?
					- 16 different items CANNOT split graphically into 16 subtiles.
					- We could GRAPHICALLY draw all 16 items "randomly" on top of each other and offset slightly to look like a "pile."
						- Clicking the pile shows it.
						- >>>OH MY GOSH GREAT FEATURE<<< PILES JOSTLE AROUND as you move.
							- So without a proper container, your vials/grenades/ammo will shift around in your pocket.
							- 

## ONE MORE QUESTION. Do we need BAGS to be able to fill slots with bulk?

***


    - pocket can hold an array of junk.

	- A CHEST can hold an array of junk but... how are you going to find it? Everything gets RANDOMLY SORTED when it moves around.

	- A [something] will let the same area, hold MORE stuff without falling out,
		like cargo pants/shorts/jacket full of pockets. You may have the same "area" or "weight"
		you can carry (technically less with the clothing) but it actually lets you 
		carry much more.
		
	- Do we want to support Jagged Alliace 2 v1.13's advanced inventory system of snaps/hooks/etc? (not likely)



## CONTAINERS

***


 ----> FEATURE: Certain CONTAINERS can LIST how many objects are in them.
	
 - WITHOUT the feature, you can only "guess" without explicitly looking over a certain collection size. (4-5?)
		- (Either never remember, or, implement a "memory" feature where your character begins to forget collections over time and more damage / commotion causes you to forget faster.)
			---> This FEATURE could also be used for other systems possibly.


- CONTAINERS features:
	- Showing number of contents. (per above)
	- Providing ORDERING/preventing mixing [See next below]
	- Preventing SPILLING for spillable items 
		- [held in hand, it's fine unless thrown or dropped]
		- [place open-top beaker in your pocket and it'll likely spill!]

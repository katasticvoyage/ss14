

/*

	- What happens when the mouse clicks into TWO or more dialogs taking the same place? We need Z-ordering. 
		- We could just sort (by Z) every time we add/remove a dialog, and use the first result (that satisfies the x/y) in searches.
		- We could leave the context menu at the top always and re-use it with different text/etc so we don't have to resort every time we right-click. Maybe?


	- What happens if we drag start and end on the same slot? Terminate the potential event.
	- What happens if we drag and only have ONE valid slot? (The start or end is nothing.) Terminate the potential event.
	
	We need an API that runs clicks and once the potential "click event" is complete inside the handler, 
	such as a drag, or detecting a double-click, THEN the event is generated to the dialog.

	And, higher-level than that, we translate drag-click, etc into
	
	void drag_and_drop_click(slot_t from, slot_t to);
	
	Or something to that effect.
	w
	(What did I mean when I wrote that ten minutes ago?)
*/



/*
	
	object_t	- object type,  type=class (something with plenty of methods. as opposed to somethign that can be used numerically like a fixed point number, or a vector.)
	other_s		- object struct, s = struct
	
	object_t obj_r;    - r = reference. As in, "I do not own this object. I just know about it."
	- Note, the difference here is COMPOSITION. There's a difference between "knowing about" and object, and using composition (a ref/ptr) to include another class that "IS apart of myself."
		- A car HAS-A engine. The engine should be destroyed/initialized when the car object is destroyed/initialized.
		- A person may KNOW about "the last guy that punched me" with a ref/pointer, but he doesn't OWN that other person. The other person isn't destroyed when this guy is. 

	In this specific case:

	object_t		- object INSTANCE of a type ("taco") "I am a taco, located at x/y.")
	object_type_t	- object TYPE (not C++/D type). (ala a "taco" type of ingame object.) "A taco is a food, eadible by people, with meat, etc."



*/

pragma(lib, "dallegro5");

version(ALLEGRO_NO_PRAGMA_LIB)
{}
else
{
	pragma(lib, "allegro");
	pragma(lib, "allegro_primitives");
	pragma(lib, "allegro_image");
	pragma(lib, "allegro_font");
	pragma(lib, "allegro_ttf");
	pragma(lib, "allegro_color");
}

import std.stdio;
import std.conv;
import std.string;
import std.format; //String.Format like C#?! Nope. Damn, like printf.

import allegro5.allegro;
import allegro5.allegro_primitives;
import allegro5.allegro_image;
import allegro5.allegro_font;
import allegro5.allegro_ttf;
import allegro5.allegro_color;


const int SCREEN_W = 1300;
const int SCREEN_H = 700;



import std.meta; //for aliasSeq!
// https://dlang.org/phobos/std_meta.html
import std.container : SList; //singlely linked list

struct globals_t{
	ALLEGRO_TIMER *double_click_timer;
	int viewport_offset_x = 0;
	int viewport_offset_y = 0;
	keys_t keys;
	};

struct keys_t
	{
	bool pressed_down=0;
	bool pressed_up=0;
	bool pressed_left=0;
	bool pressed_right=0;
	}

globals_t g;
ALLEGRO_FONT* font; //FIXME move into globals

enum BULK_TYPE
	{
	NONE = 0, // 0 - cannot be placed in a slot or picked up, ever.
	TINY,	// 
	SMALL,  // 4x TINY
	MEDIUM, // 4x SMALL
	LARGE, 	// 4x MEDIUM
	HUGE // 1 ANYTHING fits in 1 HUGE. ***
	} 
//NOTE, this may be ADJUSTED/AMMENDED for balancing.
	
// *** Does this make SENSE? Or should there be a larger/different version? Like "GRAPPLE"
// GRAPPLE being picking up a single object.
//
// Because what size is a PERSON? 
// - If a person is HUGE, then a forklift can pick up one person per "forklift grabber"
// 		- But here's the problem. Put a PERSON inside a cryo tube and 
//			now you can't even fit a TINY cherry in there? That doesn't make sense.

// So, should we have HUGE = 1 anything, or, simplify it to 4x or whatever.
// But then we also have a NEW QUALIFIER called "can_subdivide". Which is almost always
// yes, except in the case of a forklift storing a single object.

// But wait, one more question! While probably a better system. We haven't actually 
// dealt with the example issue. A cryo tube that takes a HUGE person can't take a 
// cherry on top of that. In NORMAL Space Station 13, it's a tube, so you can fit a person
// AND some random extra crap like a suit, clothes, beaker, etc. I don't think you can take
// TWO people however due to scripting.

// ANOTHER PROBLEM. If a HUGE person fits in a CRYO tube. What happens if they "drop" 
// their stuff. Technically, now, you've got a HUGE person in a object PLUS the size of
// the other items he dropped! That is, by DROPPING an item, we've somehow INCREASED the
// total volume of items inside a container.

// So the question is: Is this a FUNDAMENTAL PROBLEM, or can we leave it as a side-effect 
// of simplified rules and leave it as a idiosyncrasy.

// idiosyncrasy - a mode of behavior or way of thought peculiar to an individual.
// peculiarity - an odd or unusual feature or habit.

// ANOTHER QUESTION we really need to 'decide' on is: what do tiny/small/medium/large/huge
// each refer to? That is, what size is a PERSON? Huge? Then don't we need something
// bigger than a person for hulk/alien/super monsters as well as vehicles? A forklift
// probably shouldn't be able to pickup another forklift.

/*
	If human = huge:
		
		large = large chest, locker, ...
		medium = tacklebox
		small =
		tiny = cherry (4x cherries?)

	WHAT IF WE SUPPORT STACKS SEPARATELY. This will allow MUCH BETTER granularity 
	for BALANCING. 
		
		- A [STACK] of cherries could be TINY, and be up to 4x/6x/8x.
		- What if STACKS can expand to larger slots? 
			- (This could be annoying to people moving 64x coins from a medium slot, and ending up taking a 4 small slots with 16x each.)
			- 4 cherries in TINY, 16 cherries in SMALL, etc. 4x rule.
			- HOWEVER, we could support custom amounts for each BULK_TYPE. (4, 10, 20, 50)
			- We could also support custom STARTING amount for stack sizes...
				- Like 4 cherries in TINY, 16 in small, 64 in medium.
				- 10 shotgun shells in small, 40 in medium, (note: still 4x, and we're just setting the starting value and starting bulk_type)
					- However, shouldn't you still have ONE shotgun shell in TINY? 
						- Depends on what TINY is... the point is you should be able to have at ONE item in the smallest bulk type, right?
							- Or is that not a requirement?
*/

	// ANOTHER QUESTION: If you can CARRY a person, do they turn into a slot?
	// Because you've got two hands. You CAN'T carry two people. So in a way, your
	// individual hand slots are special because they may be combined into one super-slot
	// than can pick up a person.


 // THIS NO LONGER COMPILES. Did it ever??
// https://dlang.org/spec/hash-map.html    It should work, looking like this...static initialization of AA

// COMPILER PROBLEM!!! The EXAMPLE CODE doesn't compile!!
// Is my LDC2 out of date or broken? Would DMD show different?
/*
immutable int[string] BULK_TYPE_CONVERSION =
	[
	"TINY_IN_SMALL": 4,
	
	"TINY_IN_MEDIUM": 4*4,
	"SMALL_IN_MEDIUM": 4,
	
	"SMALL_IN_TINY": 4*4*4,
	"SMALL_IN_LARGE": 4*4,
	"MEDIUM_IN_LARGE": 4,
	
	"TINY_IN_HUGE": 1, //enjoy picking up a single cherry with a forklift.
	"SMALL_IN_HUGE": 1,
	"MEDIUM_IN_HUGE": 1,
	"LARGE_IN_HUGE": 1,
	];*/
// "error, is not a constant." <---
// are any of these strings mentioned elsewhere? or is a template modifying it?

//woah, you can do int [5-1] array_of_four; Can you do that in C/C++/etc?


class world_t
	{
	map_t [] maps;
	
	void draw(int offset_x, int offset_y, int map_number)
		{
		maps[map_number].draw(offset_x, offset_y);
		}
	
	this()
		{
		map_t t = new map_t;
		t.load_stuff();
		maps ~= t;
		}
	}

world_t world;
	
class map_t
	{
	int width=25;
	int height=25;
	int [][] data;
	ALLEGRO_BITMAP *[] tiles;

	void load_stuff()
		{
		ALLEGRO_BITMAP *t = al_load_bitmap("./data/tiles/tile01.png");
		tiles ~= t;
		}
	
	void draw(int offset_x, int offset_y)
		{
		int min_x = offset_x/64;
		int max_x = (SCREEN_W + offset_x)/64;
		if(min_x < 0)min_x = 0;
		if(max_x >= width)max_x = width-1;
		
		int min_y = offset_y/64;
		int max_y = (SCREEN_H + offset_y)/64;
		if(min_y < 0)min_y = 0;
		if(max_y >= height)max_y = height-1;
		
		int total_tiles_drawn = 0;
			
		for(int j=min_y; (j-1) < max_y; j++)
			for(int i=min_x; (i-1) < max_x; i++)
			{ //we go +1 so we overdraw by at least one tile so it doesn't show missing pieces at the right and bottom edges.
			
			al_draw_bitmap(
				tiles[0], 
				i*64 - offset_x, 
				j*64 - offset_y, 
				0);
				
			total_tiles_drawn++;
			}
					
		al_draw_textf(
			font, 
			ALLEGRO_COLOR(1,1,1,1),
			250, 10, 
			ALLEGRO_ALIGN_LEFT, 
			"Drawing x[%d,%d] y[%d,%d] #Tiles=%d", min_x, max_x, min_y, max_y, total_tiles_drawn);		

		}
	}


struct mouse_t
	{
	bool is_being_held = false;
	int x, y;
	int starting_x, starting_y;
	//void click_at(int x, int y){}

	long click_event_timestamp = 0;

	void button_down(CLICK_TYPE button_type)
		{
		if(button_type == CLICK_TYPE.CLICK_RIGHT)
			{
			context_menu1.check_collision(mouse.x, mouse.y);
			}
			
		// DOUBLE CLICK DETECTION
		// THIS IS NOT WORKING YET. It currently detects "every other" click. 
		if(button_type == CLICK_TYPE.CLICK_LEFT)
			{
			if(click_event_timestamp != 0)
				{
				long click_event_timestamp_end = al_get_timer_count(g.double_click_timer);
				long delta = click_event_timestamp_end - click_event_timestamp;
				
				writefln("Delta is %d", delta);
				click_event_timestamp = 0;
				//https://www.allegro.cc/manual/5/al_get_timer_speed
				writeln("END MOUSE double click check.");
				}else{
				writeln("BEGIN MOUSE double click time.");
				click_event_timestamp= al_get_timer_count(g.double_click_timer);
				}
			
			
			mouse.capture_starting_position();
			mouse.is_being_held = true;
			}
		}

	void button_up(CLICK_TYPE button_type)
		{
		is_being_held = false;

		if(x == starting_x)
			{
			if(y == starting_y)
				{
				clicked_without_drag(button_type);
				}
			}else{	
			clicked_with_drag();
			}
		}
	
	void capture_movement_state(int _x, int _y)
		{
		x = _x;
		y = _y;
		//could also capture button status or whatever in a similar function.	
		}
	
	void capture_starting_position()
		{
		starting_x = x;
		starting_y = y;
		}
	
	void clicked_with_drag()
		{
		writefln("Mouse click with drag at %d, %d", x, y);
		dialog_handler.mouse_collision_drag();
		}

	void clicked_without_drag(CLICK_TYPE button_type)
		{
		writefln("Mouse clicked without moving at %d, %d", x, y);
		dialog_handler.mouse_collision(button_type);
		}
	}
	
mouse_t mouse;

class inventory_t
	// DO ME.
	{		
	}

class graphics_t
	{
	void draw_slot(int x, int y)
		{
		}
	}

graphics_t gfx;


enum CLICK_TYPE
	{
	CLICK_LEFT = 1,  //Allegro = 1
	CLICK_MIDDLE,  // ??
	CLICK_RIGHT = 2, //Allegro = 2
	CLICK_4,  // ??
	CLICK_5  // ??
	};
	
class context_menu_t
	{
	context_item_t items;	
	int x, y, w, h;
	
	bool is_shown = false;
	bool is_mouse_over = false;
	
	this()
		{
		x = 100;
		y = 50;
		w = 100;
		h = 200;
		}
		
	void move(int _x, int _y)
		{
		x = _x;
		y = _y;

		bounce_boundaries();
		}
		
	void bounce_boundaries()
		{
		if(x + w > SCREEN_W)
			{
			x = SCREEN_W - w - 1 - 16;
			}
			
		if(y + h > SCREEN_H)
			{
			y = SCREEN_H - h - 1 - 16;
			}
		}

	void open()
		{
		is_shown = true;
		}
		
	void close()
		{
		is_shown = false;
		}

	void check_collision(int mouse_x, int mouse_y)
		{
		if(check_rectangle_collision(x, y, w, h, mouse_x, mouse_y))
			{
			is_mouse_over = true;
			}else{
			is_mouse_over = false;
			}
		}
		
	void draw()
		{
		if(!is_shown)return;
		
		if(is_mouse_over)
			{
			al_draw_filled_rounded_rectangle(x, y, x+w, y+h, 10, 10, al_map_rgb(128,128,255));
			}else{
			al_draw_filled_rounded_rectangle(x, y, x+w, y+h, 10, 10, al_map_rgb(0,0,128));
			}

		al_draw_rounded_rectangle(x, y, x+w, y+h, 10, 10, al_map_rgb(255,255,255), 2);
		}
		
	void add_line(){}
	void add_text(){}
	void add_icon(){}
	void clear_items(){}
		
	void draw_line(){}
	void draw_text(){}
	void draw_icon(){}
	}
	
//helper function
bool check_rectangle_collision(U)(int x, int y, int w, int h, U target_x, U target_y)
	{
	if(target_x > x && target_x < x + w && target_y > y && target_y < y + h)
		{
		return true;
		}else{
		return false;
		}
	}

class context_item_t
	{
	int x, y;
	int w, h;
	
	bool check_collision(int mouse_x, int mouse_y)
		{
		return check_rectangle_collision(x, y, w, h, mouse_x, mouse_y);
		}
	}
context_menu_t context_menu1;


class dialog_console_t
	{
	public:
	int x, y, w, h;
	int line_height=16;
	
	string [] buffer;
	
	void add_line(){}
	void clear(){}
	void scroll(int position){}
	
	void draw()
		{
		foreach(int i, line; buffer)
			{
			al_draw_textf(font, ALLEGRO_COLOR(1,1,1,1), x, y + line_height*i, 0, "%s", toStringz(line) );
			}
		}
	}


class dialog_handler_t
	{
	public:
	dialog_slot_t [] slots;
	
	this()
		{			
		ALLEGRO_BITMAP *bg_slot = al_load_bitmap("./data/inventory_internal_slot.png"); //fix me later. ENCAPSULATION VIOLATION.	

		ALLEGRO_BITMAP *bg_hands = al_load_bitmap("./data/inventory_hands.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_head = al_load_bitmap("./data/inventory_head.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_face = al_load_bitmap("./data/inventory_face.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_back = al_load_bitmap("./data/inventory_back.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_armor = al_load_bitmap("./data/inventory_armor.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_eyes = al_load_bitmap("./data/inventory_eyes.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_ears = al_load_bitmap("./data/inventory_ears.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_shoes = al_load_bitmap("./data/inventory_shoes.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_overalls = al_load_bitmap("./data/inventory_overalls.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_overalls_slot = al_load_bitmap("./data/inventory_overalls_slot.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_card = al_load_bitmap("./data/inventory_card.png"); //fix me later. ENCAPSULATION VIOLATION.	
		ALLEGRO_BITMAP *bg_belt = al_load_bitmap("./data/inventory_belt.png"); //fix me later. ENCAPSULATION VIOLATION.	
		
		

		slots ~= new dialog_slot_t(10     ,   300, bg_slot);
		slots ~= new dialog_slot_t(10 + 74,   300, bg_slot);
		slots ~= new dialog_slot_t(10 + 74*2, 300, bg_slot);
		slots ~= new dialog_slot_t(10 + 74*3, 300, bg_slot);
		slots ~= new dialog_slot_t(10 + 74*4, 300, bg_slot);
		slots ~= new dialog_slot_t(10 + 74*5, 300, bg_slot);
		slots ~= new dialog_slot_t(10 + 74*6, 300, bg_slot);

		slots ~= new dialog_slot_t(60 + 74  , 0       , bg_head); //top most
		slots ~= new dialog_slot_t(60       , 0 + 74  , bg_eyes); //top left
		slots ~= new dialog_slot_t(60 + 74  , 0 + 74  , bg_face); //top
		slots ~= new dialog_slot_t(60 + 74*2 , 0 + 74  , bg_ears); //top right
		slots ~= new dialog_slot_t(60       , 0 + 74*2, bg_overalls); //left
		slots ~= new dialog_slot_t(60 + 74  , 0 + 74*2, bg_armor); //middle
		slots ~= new dialog_slot_t(60 + 74*2, 0 + 74*2, bg_hands);  //right
		slots ~= new dialog_slot_t(60 + 74  , 0 + 74*3, bg_shoes); //bottom
		slots ~= new dialog_slot_t(60 + 0   , 0 + 74*3, bg_back); //bottom left
		slots ~= new dialog_slot_t(60 + 74*2, 0 + 74*3, bg_overalls_slot); //bottom right
		slots ~= new dialog_slot_t(60 + 74*3, 0 + 74*3, bg_card); //bottom right
		slots ~= new dialog_slot_t(60 + 74*4, 0 + 74*3, bg_belt); //bottom right right
		}

	void draw()
		{
		foreach (t; slots)
			{
			t.draw();
			}
		}
		
	void check_collisions(int mouse_x, int mouse_y)
		{
	// foreach
	//	check_rectangle_collision
		}

	void mouse_collision(CLICK_TYPE button_type)
		{
		context_menu1.is_shown = false; // FIX ENCAPSULATION VIOLATION!

		if(button_type == CLICK_TYPE.CLICK_LEFT)
			{
			context_menu1.close();
			}
		if(button_type == CLICK_TYPE.CLICK_RIGHT)
			{
			//open context menu 
			// only if we find something that CAN be context matched???
			//context_menu1.x = mouse.x;	
			//context_menu1.y = mouse.y;	
			//context_menu1.is_shown = true;
			context_menu1.open();
			context_menu1.move(mouse.x, mouse.y);
//			context_menu1.check_collision(mouse.x, mouse.y);
				
			return;
			}
			
		foreach (t; slots) //reset state before beginning (to remove previous drag to/from)
			{
			t.is_colliding = false;
			t.is_recieving = false;
			}

		foreach (t; slots)
			{
			int x = t.x;
			int y = t.y;
			
			if(mouse.x > x)
			if(mouse.x < x + 64)
			if(mouse.y > y )
			if(mouse.y < y + 64)
					{
					writefln(" - Found collision.");
					t.is_colliding = true;
					continue;
					}
			t.is_colliding = false;
			}
		}

	void mouse_collision_drag()
		{
		foreach (t; slots) //reset state before beginning (to remove previous drag to/from)
			{
			t.is_colliding = false;
			t.is_recieving = false;
			}
			
		bool found_start = false;
		bool found_end = false;
		dialog_slot_t starting_slot;
		dialog_slot_t ending_slot;
			
		foreach (t; slots)
			{
			int x = t.x;
			int y = t.y;
			
			if(check_rectangle_collision(x, y, 64, 64, mouse.x, mouse.y))
				{
				found_start = true;
				starting_slot = t;
				writefln(" - Found collision with start.");
				continue;
				}

			if(check_rectangle_collision(x, y, 64, 64, mouse.starting_x, mouse.starting_y))
				{
				found_end = true;
				ending_slot = t;
				writefln(" - Found collision with end.");
				continue;
				}
			}
			
		if(found_start && found_end)
			{
			if(starting_slot != ending_slot) //can't drag-n-drop into itself
				{
	//			starting_slot.has_object = true;
	//			ending_slot.has_object = false;
				
				if(starting_slot.object is null)
					{
					starting_slot.object = ending_slot.object;
					ending_slot.object = null;
					writeln(" - SWAPPING OBJECT WITH EMPTY SLOT.");
					}else{
					object_t temp = starting_slot.object;
					starting_slot.object = ending_slot.object;
					ending_slot.object = temp;
					writeln(" - SWAPPING TWO OBJECTS.");
					}

				// NEED TO CLEAR ALL OTHER SLOTS ???
 					
				starting_slot.is_colliding = true;
				ending_slot.is_recieving = true;
				writeln("recieving and colliding (drag-n-drop)");
				}
			}
		}	
	}

dialog_handler_t dialog_handler;


class object_type_t
	{
	public:
	string name;
	BULK_TYPE size;
	}
	
object_type_t taco;
object_type_t shoes;
object_type_t helmet;
object_type_t suit;
object_type_t armor;



class object_t
	{
	object_type_t type_r;
		
	ALLEGRO_COLOR color;
	ALLEGRO_BITMAP *sprite = null;
	
	this(ALLEGRO_COLOR _color)
		{
		color = _color;
		}

	this(ALLEGRO_BITMAP *bitmap)
		{
		sprite = bitmap;
		}
		
	this()
		{
		color = al_map_rgb(255,255,255);
		}
		
			
	void draw(int x, int y)
		{
		if(sprite == null)
			{
			al_draw_filled_rounded_rectangle(x - 16, y - 16, x + 16, y + 16, 10, 10, color);
			}else{
			al_draw_bitmap(sprite, x - 32, y - 32, 0);
			}
		
		}
	}

class slot_t
	{
	BULK_TYPE size;
	object_t obj_r;
	
//	bool has_object = false;

//TWO failure modes? Is FULL. vs is valid BULK TYPE?
// - Is there room
// - Is it going into a LARGER bulk type? (no difference)
// - Is it going into a STACK?
// maybe I'm over thinking this? what's the issue? 
 
	bool is_occupied()
		{
		if(obj_r !is null)return true; else return false;
		}
		
	bool is_equal_or_lesser_bulk_type(object_t o)
		{
		if(size == o.type_r.size)
			{
			return true;
			}
			
		if(size == BULK_TYPE.HUGE)
			{
			return true; //huge can take anything.
			}

		if(size == BULK_TYPE.LARGE)
			{
			if(o.type_r.size == BULK_TYPE.LARGE)return true;
			if(o.type_r.size == BULK_TYPE.MEDIUM)return true;
			if(o.type_r.size == BULK_TYPE.SMALL)return true;
			if(o.type_r.size == BULK_TYPE.TINY)return true;
			}

		if(size == BULK_TYPE.MEDIUM)
			{
			if(o.type_r.size == BULK_TYPE.MEDIUM)return true;
			if(o.type_r.size == BULK_TYPE.SMALL)return true;
			if(o.type_r.size == BULK_TYPE.TINY)return true;
			}

		if(size == BULK_TYPE.SMALL)
			{
			if(o.type_r.size == BULK_TYPE.SMALL)return true;
			if(o.type_r.size == BULK_TYPE.TINY)return true;
			}

		if(size == BULK_TYPE.TINY)
			{
			if(o.type_r.size == BULK_TYPE.TINY)return true;
			}

		// There may be a more elegant way to do this. However, HUGE and NONE are 
		// special cases. And we may change this around. So we need to be careful
		// not to tie ourselves down to a structure that may change. Otherwise,
		// an ~1 page long function is not the end of the world.

		return false; // If the object's BULK_TYPE is NONE (never put in slot), 
		// or, larger than our slot.
		}
 
	bool check_for_room_to_insert(object_t o)
		{
		if(is_occupied)return false;
		if(!is_equal_or_lesser_bulk_type(o))
			{
			return false;
			}
			
		return true;
		}
	
	
	void insert(){}
	void remove(){}
	void draw(){}
	}

class dialog_slot_t
	{
	int x;
	int y;
	
	// MOUSE RELATED status
	bool is_colliding = false; //
	bool is_recieving = false; //being dragged into
	
	// SLOT related status
	bool is_active = false;  //not used yet
	
	//bool has_object = false;
	object_t object;
	
	ALLEGRO_BITMAP *background_bmp;
	
	this(int _x, int _y)
		{
		x = _x;
		y = _y;
		}

	this(int _x, int _y, ALLEGRO_BITMAP *bitmap)
		{
		x = _x;
		y = _y;
		background_bmp = bitmap;
		}
	
	void insert(object_t o)
		{
		object = o;
		//has_object = true;
		}
	void remove()
		{
		object = null; // WARNING, this function DELETES the object. (Normally rare in SS13). It doesn't "drop" it or whatever.
		//has_object = false;
		}
	
	void draw()
		{
		if(background_bmp !is null)
			{
			al_draw_bitmap(background_bmp, x, y, 0);
			}
			
//		if(background_bmp is null)
			{
			if(is_colliding == false)
				{
	//			al_draw_filled_rounded_rectangle(x, y, x + 64 - 1, y + 64 -1, 10, 10, al_map_rgba(0,0,0, 128));
				}else{
				al_draw_filled_rounded_rectangle(x, y, x + 64 - 1, y + 64 -1, 10, 10, al_map_rgba(32,64,128, 128));
				}	
			}

		if(is_recieving == true)al_draw_filled_rounded_rectangle(x, y, x + 64 - 1, y + 64 -1, 10, 10, al_map_rgba(192,64,32,128));
		
//		al_draw_rounded_rectangle(x, y, x + 64 - 1, y + 64 -1, 10, 10, al_map_rgb(255,255,255), 2);
		
//		if(has_object)
		if(object !is null)
			{
	//		al_draw_rounded_rectangle(x + 16, y + 16, x + 64 - 1 - 16, y + 64 -1 - 16, 10, 10, al_map_rgb(255,0,0), 2);
			object.draw(x + 32, y + 32);
			}
		}

	}
	
//dialog_slot_t [] slots; 

int setup()
	{
		
	ALLEGRO_BITMAP *item_shoes1 = al_load_bitmap("./data/items/shoes_1.png"); //fix me later. ENCAPSULATION VIOLATION.	
	ALLEGRO_BITMAP *item_shoes2 = al_load_bitmap("./data/items/shoes_2.png"); //fix me later. ENCAPSULATION VIOLATION.
	ALLEGRO_BITMAP *item_helmet1 = al_load_bitmap("./data/items/helmet_1.png"); //fix me later. ENCAPSULATION VIOLATION.
	ALLEGRO_BITMAP *item_armor1 = al_load_bitmap("./data/items/armor_1.png"); //fix me later. ENCAPSULATION VIOLATION.
	ALLEGRO_BITMAP *item_suit1 = al_load_bitmap("./data/items/suit_1.png"); //fix me later. ENCAPSULATION VIOLATION.

//	object_t o1 = new object_t(al_map_rgb(0,0,255));
//	object_t o2 = new object_t(al_map_rgb(0,255,255));
//	object_t o3 = new object_t(al_map_rgb(255,0,255));
	object_t o1 = new object_t(item_shoes1);
	object_t o2 = new object_t(item_shoes2);
	object_t o3 = new object_t(item_helmet1);
	object_t o4 = new object_t(item_armor1);
	object_t o5 = new object_t(item_suit1);
		
	context_menu1 = new context_menu_t();
	gfx = new graphics_t(); 
	dialog_handler = new dialog_handler_t();
	
	dialog_handler.slots[0].insert(o1);
	dialog_handler.slots[3].insert(o2);
	dialog_handler.slots[4].insert(o3);
	dialog_handler.slots[5].insert(o4);
	dialog_handler.slots[6].insert(o5);
	
	
	setup_world();
	
	return 0;
	}
	
void setup_world()
	{
	world = new world_t();
	}

void draw()
	{
	world.draw(g.viewport_offset_x,g.viewport_offset_y,0); //last digit is map number
	dialog_handler.draw();
	context_menu1.draw();
	}

void process_inputs()
	{
	dialog_handler.check_collisions(mouse.x, mouse.y);
	context_menu1.check_collision(mouse.x, mouse.y); //only used for MOUSE OVER
	
	// As long as these keys are held down, we apply them.
	
	const int SPEED = 2;
	
	if(g.keys.pressed_down)g.viewport_offset_y+=SPEED;
	if(g.keys.pressed_up)g.viewport_offset_y-=SPEED;
	if(g.keys.pressed_right)g.viewport_offset_x+=SPEED;
	if(g.keys.pressed_left)g.viewport_offset_x-=SPEED;
	
	g.viewport_offset_x += 1;
	g.viewport_offset_y += 1;
	}

void logic()
	{
	process_inputs();
	}
	
//=============================================================================
int main(char[][] args)
{

	return al_run_allegro(
	{
		if (!al_init())
		{
			auto ver = al_get_allegro_version();
			auto major = ver >> 24;
			auto minor = (ver >> 16) & 255;
			auto revision = (ver >> 8) & 255;
			auto release = ver & 255;

			writefln("The system Allegro version (%s.%s.%s.%s) does not match the version of this binding (%s.%s.%s.%s)",
				major, minor, revision, release,
				ALLEGRO_VERSION, ALLEGRO_SUB_VERSION, ALLEGRO_WIP_VERSION, ALLEGRO_RELEASE_NUMBER);
		
			return 1;
		}
		
		//ALLEGRO_CONFIG* cfg = al_load_config_file("test.ini");
		ALLEGRO_DISPLAY* display = al_create_display(SCREEN_W, SCREEN_H);
		ALLEGRO_EVENT_QUEUE* queue = al_create_event_queue();

		if (!al_install_keyboard())      assert(0, "al_install_keyboard failed!");
		if (!al_install_mouse())         assert(0, "al_install_mouse failed!");
		if (!al_init_image_addon())      assert(0, "al_init_image_addon failed!");
		if (!al_init_font_addon())       assert(0, "al_init_font_addon failed!");
		if (!al_init_ttf_addon())        assert(0, "al_init_ttf_addon failed!");
		if (!al_init_primitives_addon()) assert(0, "al_init_primitives_addon failed!");

		al_register_event_source(queue, al_get_display_event_source(display));
		al_register_event_source(queue, al_get_keyboard_event_source());
		al_register_event_source(queue, al_get_mouse_event_source());

		ALLEGRO_BITMAP* bmp = al_load_bitmap("./data/mysha.pcx");
		font = al_load_font("./data/DejaVuSans.ttf", 18, 0);

		with(ALLEGRO_BLEND_MODE)
		{
			al_set_blender(ALLEGRO_BLEND_OPERATIONS.ALLEGRO_ADD, ALLEGRO_ALPHA, ALLEGRO_INVERSE_ALPHA);
		}

		auto color1 = al_color_hsl(0, 0, 0);
		auto color2 = al_map_rgba_f(0.5, 0.25, 0.125, 1);
		writefln("%s, %s, %s, %s", color1.r, color1.g, color2.b, color2.a);
		

		g.double_click_timer = al_create_timer(1 / 60.0);
		al_start_timer(g.double_click_timer); 
		//this one just has to run to be a delta, it doesn't matter if we start it only when the game starts or right now.


		if( setup() ) return -1; //<---- NOTE our setup routine is here.


		bool exit = false;
		while(!exit)
		{
			ALLEGRO_EVENT event;
			while(al_get_next_event(queue, &event))
			{
				switch(event.type)
				{
					case ALLEGRO_EVENT_DISPLAY_CLOSE:
					{
						exit = true;
						break;
					}


					case ALLEGRO_EVENT_KEY_DOWN:
					{
						switch(event.keyboard.keycode)
						{
							case ALLEGRO_KEY_UP:
							{
								g.keys.pressed_up = true;
								break;
							}
							case ALLEGRO_KEY_DOWN:
							{
								g.keys.pressed_down = true;
								break;
							}
							case ALLEGRO_KEY_LEFT:
							{
								g.keys.pressed_left = true;
								break;
							}
							case ALLEGRO_KEY_RIGHT:
							{
								g.keys.pressed_right = true;
								break;
							}
							
							case ALLEGRO_KEY_ESCAPE:
							{
								exit = true;
								break;
							}
							default:
						}
						break;
					}
					
					case ALLEGRO_EVENT_KEY_UP:
					{
						switch(event.keyboard.keycode)
						{
							case ALLEGRO_KEY_UP:
							{
								g.keys.pressed_up = false;
							break;
							}
							case ALLEGRO_KEY_DOWN:
							{
								g.keys.pressed_down = false;
							break;
							}
							case ALLEGRO_KEY_LEFT:
							{
								g.keys.pressed_left = false;
							break;
							}
							case ALLEGRO_KEY_RIGHT:
							{
								g.keys.pressed_right = false;
							break;
							}
							default:
						}
						
					}
					break;
					
					case ALLEGRO_EVENT_MOUSE_AXES:
					 	{
					   	mouse.capture_movement_state(event.mouse.x, event.mouse.y);
						break;
						}

					case ALLEGRO_EVENT_MOUSE_BUTTON_DOWN:
					{
						writefln("MOUSE BUTTON %d pressed", event.mouse.button);
						if(event.mouse.button == 1)
							{
							mouse.button_down(CLICK_TYPE.CLICK_LEFT);
							}
						if(event.mouse.button == 2)
							{
							mouse.button_down(CLICK_TYPE.CLICK_RIGHT);
							}
						break;
					}

					case ALLEGRO_EVENT_MOUSE_BUTTON_UP: //ON MOUSE RELEASE
					{
						if(event.mouse.button == 1)
							{
							mouse.button_up(CLICK_TYPE.CLICK_LEFT);
							}
						if(event.mouse.button == 2)
							{
							mouse.button_up(CLICK_TYPE.CLICK_RIGHT);
							}
						break;
					}
					default:
				}
			}

//			al_clear_to_color(ALLEGRO_COLOR(0.5, 0.25, 0.125, 1));
			al_clear_to_color(ALLEGRO_COLOR(0, 0, 0, 1));
			al_draw_bitmap(bmp, 50, 50, 0);
			al_draw_triangle(20, 20, 300, 30, 200, 200, ALLEGRO_COLOR(1, 1, 1, 1), 4);
			al_draw_text(font, ALLEGRO_COLOR(1, 1, 1, 1), 70, 40, ALLEGRO_ALIGN_CENTRE, "Hello!");
			
			
			logic();
			
			draw();
			
			al_wait_for_vsync(); // MAYBE. Reduces laptop CPU usage from ~100% to ~25%.
			al_flip_display(); //NOTE, we're drawing EVEN if there are no updates!
		}

		return 0;
	});
}
